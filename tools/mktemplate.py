#!/usr/bin/env python3

import sys
import re

if len(sys.argv) < 3:
	print('usage: mktemplate.py template_file "key_value=pairs,separated_by=commas" > output_file')
	print()
	print('given a pair string of "key=value", this replaces for example:')
	print('    > Hello {{key}}!')
	print('with:')
	print('    > Hello value!')
	print('within template_file.')
	exit(0)

file_name = sys.argv[1]
stuff = sys.argv[2]

stuff = stuff.split(",")
stuff = [x.strip() for x in stuff]
stuff = [[sides.strip() for sides in x.split("=")] for x in stuff]
stuff_dict = {}
for entries in stuff:
	stuff_dict[entries[0]] = entries[1]

def replace_placeholder(re):
	return stuff_dict.get(re.group(1))

with open(file_name, "r") as file:
	line = file.readline()
	while line:
		line = re.sub(r'\{{2}(.+?)\}{2}', replace_placeholder, line)
		print(line, end="")
		line = file.readline()