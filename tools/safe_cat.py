#!/usr/bin/env python3

# `cat` except it takes into account NTFS "symlinks" rendered
# as Interix links: https://en.wikipedia.org/wiki/Interix

import sys
import os

sys.argv.pop(0)

for arg in sys.argv:
	try:
		with open(arg, "rb") as file:
			if file.read(8) == b"IntxLNK\x01":
				link_path = file.read().decode("utf-16")
				real_path = (
					os.path.join(os.path.dirname(arg), link_path)
				)
				
				with open(real_path, "rb") as linked:
					print(linked.read().decode("utf-8"))
			else:
				file.seek(0)
				print(file.read().decode("utf-8"))
	except:
		pass