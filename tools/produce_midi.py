#!/usr/bin/python

'''
MIDI "producer". Takes in midicsv output and tries
to clean it up a bit. Removes most redundant CC messages.
Also converts the MIDI to 96 ppqn since a lot of tooling seems
to work better with it than 480 ppqn files.

These redundant messages are kept:
	- Program changes after a CC 0 set
		Affects switching to a GS patch.
	- Pitch bends after a CC 6 with RPN 0000 set
		Affects pitch bend range updating.
'''

import csv
import sys
import configparser

file_name = sys.argv[1].strip()
metafile_name = sys.argv[2]

# CSV entry list.
command_bin = []

# emulated MIDI state.
# {
#	 <channel_num>: {
#		 'controls': {
#			 1: 0,
#			 2: 4
#		 },
#		 ...
#	 }
# }
emulated_states = {
}

using_gs = False
tempo = 0

for i in range(16):
	emulated_states[i] = {
		'controls': {},  # all the CC values
		
		'patch': None,  # current patch number
		'bend': None,  # pitch bend value; this is bend range * bend message
		'bankswitch': False,  # set if we're currently switching banks
		'set_rpn': False, # set if we're currrently setting RPN

		'bend_range': 1,  # set by modifying RPN 0000
	}

sys.stdin.reconfigure(encoding='latin-1')

with sys.stdin as csvfile:
	reader = csv.reader(csvfile, delimiter=',')
	old_tick = -1
	og_timebase = None
	for row in reader:
		track = int(row[0])
		tick = int(row[1])
		if og_timebase is not None:
			tick = round(tick/og_timebase*96)
		event = row[2].lower().strip()
		data = [x.strip() for x in row[3:]]

		pre_events = []  # entries to place before the current message
		post_events = []  # entries to place after the current message

		should_insert = True

		if event == 'control_c':
			channel, cc, value = [int(x) for x in data]

			# record the CC if it doesn't exist
			if emulated_states[channel]['controls'].get(cc, None) is None:
				if cc == 6:  # data CC is a special case, do not record
					if emulated_states[channel]['controls'][100] == 0 and \
							emulated_states[channel]['controls'][101] == 0:
						# however, do record our actual bend range
						emulated_states[channel]['bend_range'] = value
				else:
					emulated_states[channel]['controls'][cc] = value
					if cc == 0:
						using_gs = True  # we're explicitly using GS features
						emulated_states[channel]['bankswitch'] = True
			else:  # check if this CC doesn't change the value already there
				if cc == 6:
					if emulated_states[channel]['set_rpn']:
						# grant redundancy if we have set an RPN
						should_insert = True
						emulated_states[channel]['set_rpn'] = False

						if emulated_states[channel]['controls'][100] == 0 and \
								emulated_states[channel]['controls'][101] == 0:
							emulated_states[channel]['bend_range'] = value
				else:
					if emulated_states[channel]['controls'][cc] == value:
						should_insert = False
					else:
						emulated_states[channel]['controls'][cc] = value

						# check if we're switching GS bank
						if cc == 0:
							emulated_states[channel]['bankswitch'] = True

						# check if we're switching RPN / NRPN
						if cc in [100, 101, 98, 99]:
							emulated_states[channel]['set_rpn'] = True

			# if we're bankswitching, always add a program change event
			if emulated_states[channel]['patch']:
				if emulated_states[channel]['bankswitch']:
					emulated_states[channel]['bankswitch'] = False
					post_events.append([
						track, tick, 'program_c', channel, emulated_states[channel]['patch']
					])

		elif event == 'header':
			og_timebase = int(data[2])
			data[2] = '96'
		
		elif event == 'program_c':
			channel, patch = [int(x) for x in data]

			# record program change if it doesn't exist
			if emulated_states[channel]['patch'] is None:
				emulated_states[channel]['patch'] = patch
			else:  # check if we're not switching to a new patch
				if emulated_states[channel]['patch'] == patch:
					should_insert = False
				else:
					emulated_states[channel]['patch'] = patch

		elif event == 'pitch_bend_c':
			channel, bend = [int(x) for x in data]

			# pitch bend settings are like program change banks
			calculated_bend = bend * emulated_states[channel]['bend_range']

			# record bend if it doesn't exist
			if emulated_states[channel]['bend'] is None:
				emulated_states[channel]['bend'] = calculated_bend
			else:  # check if we're setting redundant bend
				if emulated_states[channel]['bend'] == calculated_bend:
					should_insert = False
				else:
					emulated_states[channel]['bend'] = calculated_bend

		elif event == 'tempo':
			new_tempo = int(data[0])
			if tempo == new_tempo:
				should_insert = False
			else:
				tempo = new_tempo
			
		elif event in ['key_signature', 'copyright_t', 'cue_point_t']:
			should_insert = False  # this should be added in post
		
		elif event == 'title_t':
			if track == 1:
				should_insert = False

		elif event == 'marker_t':
			if data[0].startswith("\"Sequenced by"):
				should_insert = False
		
		elif event == 'start_track':
			# add my metadata straight away
			if track == 1:
				post_events = [
					['****', 'YYYY', 'ZZZZ'],  # placeholder for standardized meta info
#					['@@@@', 'ZZZZ', 'QQQQ'],  # placeholder for GM reset
					['XXXX', '!!!!', '????']  # placeholder for GS events
				]
		
		elif event == 'end_track':  # ensure track ends right after the final event
			if tick != old_tick:
				tick = old_tick
		
		old_tick = tick

		if should_insert:
			csv_string = "%d, %d, %s, %s" % (
				track, tick, event, ', '.join(data)
			)

			for ev in pre_events:
				command_bin.append(', '.join([str(x) for x in ev]))

			command_bin.append(csv_string)

			for ev in post_events:
				command_bin.append(', '.join([str(x) for x in ev]))

meta_place = command_bin.index('****, YYYY, ZZZZ')
gs_sysex_place = command_bin.index('XXXX, !!!!, ????')
#gm_sysex_place = command_bin.index('@@@@, ZZZZ, QQQQ')

#command_bin[gm_sysex_place] = \
#	'1, 0, system_exclusive, 4, 126, 127, 9, 1'

# insert a GS sysex
if using_gs:
	command_bin[gs_sysex_place] = \
		'1, 0, system_exclusive, 10, 65, 16, 66, 18, 64, 0, 127, 0, 65, 247'
	# master volume
	command_bin.insert(gs_sysex_place,
					   '1, 0, system_exclusive, 5, 126, 127, 9, 1, 247'
					   )
else:
	command_bin.pop(gs_sysex_place)

# lookup the appropriate info

meta = configparser.ConfigParser()
meta.read(metafile_name)

try:
	title = meta[file_name]['t']
	copyright_txt = meta[file_name]['c']
	date = meta[file_name]['d']
	
	command_bin[meta_place] = '1, 0, marker_t, "Sequenced by Zumi %s"' % (date)
	command_bin.insert(meta_place, '1, 0, copyright_t, "%s"' % (copyright_txt))
	command_bin.insert(meta_place, '1, 0, title_t, "%s"' % (title))
except:
	command_bin.pop(meta_place)

sys.stdout.reconfigure(encoding='latin-1')  # some windows programs can't handle utf-8

sys.stdout.write(
	'\n'.join(command_bin)
)
