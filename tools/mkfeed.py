#!/usr/bin/env python3

import html
from datetime import datetime, timedelta, timezone
import re
import json

# force using local MDiocre (for devving)
import os
import sys
inpath = os.path.realpath(__file__ + '/../')
if os.path.isdir(f'{inpath}/mdiocre'):
	sys.path.insert(0, f'{inpath}/mdiocre')
else:
	raise Exception('"mdiocre" directory must be in the same directory as this script')
from mdiocre.core import MDiocre
from mdiocre.wizard import Wizard

# git version number
import subprocess
GIT_VERSION = subprocess.check_output(["git", "log", "-1", "--date=short", "--format=%h (%ad)"]).decode('ascii').strip()
VERSION = GIT_VERSION or '0.2 (2021-09-28)'

# date funcs
def rfc822date(date):
	return (datetime.strftime(date, '%a, %d %b %Y %H:%M:%S %z').strip(), date)

def rfc822date_from_string(time_str):
	# timestamps
	TRY_REGEX = {
		'ISO8601'   : '(\d+)-(\d{2})-(\d{2})T(\d{2}):(\d{2})(:(\d{2}))?(([\+-]\d{2}):(\d{2}))?',
		'Clear'     : '(\d{1,2}) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) (\d+) (\d{2}):(\d{2})(:(\d{2}))?( ([\+-]\d{2})(\d{2}))?'
	}
	for tmatchname, tregex in TRY_REGEX.items():
		match = re.match(tregex, time_str)
		if match is not None:
			#print(f'Matching {tmatchname}')
			group = match.groups()
			if tmatchname == 'Clear':
				if group[7] is not None: #timezone
					if group[5] is not None: #second
						return rfc822date(datetime.strptime(time_str, '%d %b %Y %H:%M:%S %z'))
					else:
						return rfc822date(datetime.strptime(time_str, '%d %b %Y %H:%M %z'))
				else:
					if group[5] is not None: #second
						return rfc822date(datetime.strptime(time_str, '%d %b %Y %H:%M:%S'))
					else:
						return rfc822date(datetime.strptime(time_str, '%d %b %Y %H:%M'))
			elif tmatchname == 'ISO8601':
				if group[7] is not None: #timezone
					tz_s = (int(group[8]) * 3600) + (int(group[9] * 60))
					tz = timezone(timedelta(seconds=tz_s))
					if group[5] is not None: #second
						date =  datetime.strptime(time_str[:-6], '%Y-%m-%dT%H:%M:%S')
					else:
						date = datetime.strptime(time_str[:-6], '%Y-%m-%dT%H:%M')
					return rfc822date(date.replace(tzinfo=tz))
				else:
					if group[5] is not None: #second
						return rfc822date(datetime.strptime(time_str, '%Y-%m-%dT%H:%M:%S'))
					else:
						return rfc822date(datetime.strptime(time_str, '%Y-%m-%dT%H:%M'))
	return None

# utility funcs
def sanitize(tag):
	pass

def escape(string):
	return html.escape(string)

# simple function
def build_tree(tag, content, attrs={}):
	encoded_content = ""
	encoded_attrs = ""
	
	if isinstance(content, str):
		encoded_content = escape(content)
	
	elif isinstance(content, dict):
		for k, e in content.items():
			encoded_content += build_tree(k, e)
	
	elif isinstance(content, list):
		for k in content:
			if isinstance(k, dict):
				encoded_content += build_tree(
					k.get("tag"),
					k.get("content"),
					attrs=k.get("attrs", {})
				)

	for k, e in attrs.items():
		encoded_attrs += " %s=\"%s\"" % (k, e)
	
	if encoded_content:
		return "\n<%s%s>%s</%s>" % (tag, encoded_attrs, encoded_content, tag)
	else:
		return "\n<%s%s/>" % (tag, encoded_attrs)

def make_items(baseUrl, relativeDir, sourceDir, pubVar, numEntries=-1):
	itemsList = []
	
	# setup MDiocre
	m = MDiocre()
	
	to_html_link = lambda x: '.'.join(x.split('.')[:-1])+'.html'
	
	# make file list
	files = \
		[i for i in os.listdir(sourceDir) \
		if ( \
			i.lower().endswith('.md') or\
			i.lower().endswith('.rst') or\
			i.lower().endswith('.zimtxt') \
		)]
	
	w = Wizard()
	w.register_converters()
	# hack in zimtext
	w.converters["zimtxt"] = w.converters["zim"]

	# insert items from files
	for f in files:
		name, ext = os.path.splitext(f.lower())
		href = '/'.join([relativeDir, to_html_link(f)])
		
		# parse file
		m.switch_parser(w.converters[ext[1:]])
		with open(os.path.join(sourceDir,f), 'r') as content:
			_vars = m.process(content.read())

		# get titles etc.
		title = _vars.get('title') or name
		pubDate = _vars.get(pubVar)
		content = re.sub(
				  r'<p><time.+/time></p>'+
				  r'|'+
				  r'<p><time.+/time>\)</span></p>',
				  '',
				  _vars.get('content'), count=1)
		
		itemsList.append({
			"tag": "item",
			"content": {
				"title": title,
				"link": '/'.join([baseUrl,href]),
				"description": content,
				"pubDate": rfc822date_from_string(pubDate)
			}
		})
	# sort by descending
	itemsList.sort(key=lambda x: x['content']['pubDate'][1], reverse=True)
	# grab only latest 10
	if numEntries > 0:
		itemsList = itemsList[:numEntries]
	# clean up items list
	for i in range(len(itemsList)):
		displayDate, realDate = itemsList[i]['content']['pubDate']
		itemsList[i]['content']['pubDate'] = displayDate
	return itemsList

def make_rss(jsonFile):
	# load data
	rssData = json.load(open(jsonFile,'r'))
	
	# init channel
	channelData = [
				{"tag":"title",       "content":rssData["meta"]["title"]},
				{"tag":"link",        "content":rssData["meta"]["link"]},
				{"tag":"description", "content":rssData["meta"]["description"]},
				{"tag":"language",    "content":rssData["meta"]["language"]},
				{"tag":"image",       "content":{
					"url":   rssData["meta"]["image"]["url"],
					"title": rssData["meta"]["image"]["title"],
					"link":  rssData["meta"]["image"]["link"]
				}},
				{"tag":"generator", "content":"mkfeed.py %s" % VERSION},
				{"tag":"lastBuildDate", "content":rfc822date(datetime.now(timezone.utc))[0]},
				{"tag":"atom:link", "attrs": {
					"href": rssData["meta"]["link"],
					"rel": "self",
					"type": "application/rss+xml"
				}}
	]
	# doctype
	print("<?xml version=\"1.0\" encoding=\"utf-8\"?>", end="")
	
	baseUrl = rssData["baseurl"]
	pubVar = rssData["pub_var"]
	numEntries = rssData["num_entries"]
	
	# make listing
	itemList = []
	for sourceDir, relativeDir in rssData["dirs"].items():
		itemList += make_items(baseUrl, relativeDir, sourceDir, pubVar, numEntries)
	
	channelData += itemList
	
	# generate RSS feed
	print(
		build_tree(
		"rss", {
			"channel": channelData
		},
		attrs={"version": "2.0", "xmlns:atom": "http://www.w3.org/2005/Atom"})
	)

if __name__ == '__main__':
	feed = make_rss(sys.argv[1])
