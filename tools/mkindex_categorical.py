#!/usr/bin/python

import json
import re
from datetime import datetime, timedelta, timezone

import pprint

# force using local MDiocre (for devving)
import os
import sys
inpath = os.path.realpath(__file__ + '/../')
if os.path.isdir(f'{inpath}/mdiocre'):
	sys.path.insert(0, f'{inpath}/mdiocre')
else:
	raise Exception('"mdiocre" directory must be in the same directory as this script')
from mdiocre.core import MDiocre
from mdiocre.wizard import Wizard


INDEX_PREAMBLE = '''
## Bloge Index

**Warning**: I curse!

View by [chronological order](index-chrono.html)

<div id="blog-index-categorical">
'''

INDEX_AFTER = '''
</div>

<!--: title = "Bloge Index" -->
'''


def determineDate(time_str):
	# timestamps
	TRY_REGEX = {
		'ISO8601'   : '(\d+)-(\d{2})-(\d{2})T(\d{2}):(\d{2})(:(\d{2}))?(([\+-]\d{2}):(\d{2}))?',
		'Clear'     : '(\d{1,2}) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) (\d+) (\d{2}):(\d{2})(:(\d{2}))?( ([\+-]\d{2})(\d{2}))?'
	}
	for tmatchname, tregex in TRY_REGEX.items():
		match = re.match(tregex, time_str)
		if match is not None:
			#print(f'Matching {tmatchname}')
			group = match.groups()
			if tmatchname == 'Clear':
				if group[7] is not None: #timezone
					if group[5] is not None: #second
						date = datetime.strptime(time_str, '%d %b %Y %H:%M:%S %z')
					else:
						date = datetime.strptime(time_str, '%d %b %Y %H:%M %z')
				else:
					if group[5] is not None: #second
						date = datetime.strptime(time_str, '%d %b %Y %H:%M:%S')
					else:
						date = datetime.strptime(time_str, '%d %b %Y %H:%M')
				return date
			elif tmatchname == 'ISO8601':
				if group[7] is not None: #timezone
					tz_s = (int(group[8]) * 3600) + (int(group[9] * 60))
					tz = timezone(timedelta(seconds=tz_s))
					if group[5] is not None: #second
						date = datetime.strptime(time_str[:-6], '%Y-%m-%dT%H:%M:%S')
					else:
						date = datetime.strptime(time_str[:-6], '%Y-%m-%dT%H:%M')
					date = date.replace(tzinfo=tz)
				else:
					if group[5] is not None: #second
						date = datetime.strptime(time_str, '%Y-%m-%dT%H:%M:%S')
					else:
						date = datetime.strptime(time_str, '%Y-%m-%dT%H:%M')
				return date
	return None

CATEGORIES = {}

def makeSortedEntries(_dir, datevar, fromLatest=True):
	# setup MDiocre
	m = MDiocre()
	
	files = \
	[i for i in os.listdir(_dir) \
	if (i.lower().endswith('.md') or i.lower().endswith('.rst')
	or i.lower().endswith('.zimtxt'))]
	
	# init converters
	w = Wizard()
	w.register_converters()
	# hack in zimtext
	w.converters["zimtxt"] = w.converters["zim"]

	for f in files:
		name, ext = os.path.splitext(f.lower())
		href = to_html_link(f)
		
		# parse file
		m.switch_parser(w.converters[ext[1:]])
		with open(os.path.join(_dir,f), 'r') as content:
			_vars = m.process(content.read())
		
		title = _vars.get('title') or name
		
		category = _vars.get('category')
		if category not in CATEGORIES:
			CATEGORIES[category] = []
		
		pubdate = _vars.get(datevar)
		
		date_ = determineDate(pubdate)
		formatted_date = datetime.strftime(date_, '%Y-%m-%d').strip()
		formatted_date_iso = datetime.strftime(date_, '%Y-%m-%dT%H:%M:%S').strip()
		
		CATEGORIES[category].append(
			(formatted_date, title, href, date_)
		)
	dt = lambda x: x[3]
	od = lambda x: ORDERING[x]
	
	for i in CATEGORIES:
		CATEGORIES[i].sort(key=dt, reverse=fromLatest)
	
	#pp = pprint.PrettyPrinter()
	#pp.pprint(CATEGORIES)
	#exit(0)
	
# main process
# --------------------------------
if __name__ == '__main__':
	to_html_link = lambda x: '.'.join(x.split('.')[:-1])+'.html'
	
	# sys.argv[1] = entry sources
	# sys.argv[2] = template to use
	makeSortedEntries(sys.argv[1], 'longdate')
	
	content = INDEX_PREAMBLE
	
	ordering = [
		"rants",
		"musings",
		"tools",
		"personal",
		"behind-the-scenes",
		"site-updates"
		""
	]
	
	for key in ordering:
		content += '<div>'
		
		entries = CATEGORIES[key]
		
		if key.strip() == "":
			heading = "Unsorted"
		else:
			heading = key.replace("-"," ").title()
		
		content += f'<h3 id="{"-".join(heading.lower().strip().split(" "))}">{heading}</h3>\n<ul>'
		for date_, title_, href_, actualdate_ in entries:
			content += f'<li><a href="{href_}">{title_.strip()}</a></li>\n'
		content += '</ul>\n'
		
		content += "</div>"
	
	content += INDEX_AFTER

	#print(content)
	
	m = MDiocre()
	w = Wizard()
	w.register_converters()
	m.switch_parser(w.converters['md'])
	
	with open(sys.argv[2], 'r') as template:
		print(m.render(template.read(), m.process(content)))
