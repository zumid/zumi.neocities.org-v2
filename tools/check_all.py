#!usr/bin/python

import os
from bs4 import BeautifulSoup
from check import get_plaintext_size, get_media_count, get_form_count, get_anchor_count
import sys

DIRECTORY = sys.argv[1]

total = 0
xfiles = 0

for root, folder, files in os.walk(DIRECTORY):
	def htfilter(f):
		return os.path.splitext(f.lower())[1] == '.html'
	
	files = list(filter(htfilter, files))
	
	if len(files) > 0:
		print('in {}...'.format(root))
	
	for f in files:
		print('\t{}:'.format(f))
		
		file_name = os.path.sep.join([root,f])
		# perform check
		with open(file_name,'r') as html_file:
			markup = html_file.read()
		
		soup = BeautifulSoup(markup, 'html.parser')
		
		# unminify html, slight modification to the rules
		prettied = soup.prettify()

		overhead = 2048
		plaintext_size = get_plaintext_size(soup) * 1.5
		media_size = get_media_count(soup) * 256
		form_size = get_form_count(soup) * 128
		anchor_size = get_anchor_count(soup) * 128

		ideal_size = overhead + plaintext_size + media_size + form_size + anchor_size
		actual_size = len(prettied)
		
		#print('\t\tIdeal size:')
		#print('\t\t-----------')
		#print('\t\toverhead : {}'.format(overhead))
		#print('\t\ttext     : {}'.format(plaintext_size))
		#print('\t\tmedia    : {}'.format(media_size))
		#print('\t\tform     : {}'.format(form_size))
		#print('\t\tanchor   : {}'.format(anchor_size))
		#print()
		if actual_size > ideal_size:
			print('\t\tactual size \033[1m{:0.0f}\033[0m \033[31;1;4mMORE THAN\033[0m ideal \033[1m{:0.0f}\033[0m ({:0.2f}x)'.format(actual_size, ideal_size, actual_size/ideal_size))
			xfiles += 1
			
		else:
			print('\t\tactual size \033[1m{}\033[0m l.e.q. ideal \033[1m{:0.0f}\033[0m'.format(actual_size, ideal_size))
		#print()
		total += 1

print(' RESULTS ')
print('=========')
print('{} (\033[32;1m{:0.2f}%\033[0m) files optimized / {} (\033[31;1m{:0.2f}%\033[0m) files unoptimized'.format((total-xfiles),(total - xfiles)/total*100, xfiles,xfiles/total*100))
print('out of {} files total'.format(total))
