#!/usr/bin/env python3

from datetime import datetime
import sys
import os

from mdiocre import MDiocre, VariableManager
from mdiocre.parsers.html import HtmlParser

# define function to turn ISO 8601 dates to readable dates
def toReadableDate(iso_date):
	date_and_time = datetime.fromisoformat(iso_date)
	return date_and_time.strftime("%d %b %Y, %H:%M:%S %Z")

# so here's a simple blogging tool

if len(sys.argv) < 3:
	print("mk_dev.py [/src_folder] [template_file.html] > [output.html]")
	print()
	print("src folder must be filled with .md (markdown) files with a date variable in it")
	print("example file contents:")
	print()
	print("<!--:date=\"2021-11-06T16:30+07:00\"-->")
	print()
	print("## section title")
	print("contents")
	print()
	print("### subsection title")
	print("other content")
	exit()

# grab vars
src_folder = sys.argv[1]
template_file = sys.argv[2]

# the html snippet to use for the individual posts
POST_COMPONENT = '''
	<section class="card" id="<!--: post id -->">
		<div class="-post">
		<!--: content -->
		</div>
		<footer>
			<time datetime="<!--: date -->"><!--: readable date --></time>
			<a href="#top">(to top)</a>
			<a href="#<!--: post id -->" class="-linkself">(to this section)</a>
		</footer>
	</section>
'''

# prepare html post list
post_list = []

for folder_name, subfolders, files in os.walk(src_folder):
	# make post out of every file in a directory
	for file in files:
		if not file.endswith(".md"): continue

		# open a file in the folder
		with open(os.path.join(folder_name, file), "r") as file_:
			# extract its contents and process it to the
			# post component using MDiocre
			section_contents = MDiocre().process(file_.read())

			# create a custom variable to store the date
			section_contents.assign(
				'readable date = "%s"' %
				toReadableDate(section_contents.variables.get('date', '1970-01-01T00:00:00+00:00'))
			)

			# custom variable id is just the file name
			id = os.path.splitext(file)[0]
			section_contents.assign(
				'post id = "%s"' % id
			)

			# append the resulting HTML to our post list (first element)
			# second element is date so we can sort it later
			# third element is the id for use as an anchor link
			post_list.append(
				{
					"page": MDiocre().render(POST_COMPONENT,section_contents),
					"date": section_contents.get('date'),
					"id": id
				}
			)

# sort by date
post_list.sort(key=lambda x: x["date"], reverse=True)

# make our table of contents
toc = "<ul>"
for post in post_list:
	toc += '<li><a href="#%s">%s</a></li>' % (post["id"], post["id"])
toc += "</ul>"

# insert it into our template HTML
with open(template_file, "r") as template:
	md = MDiocre()
	md.switch_parser(HtmlParser)
	container = md.process("\n".join(
			[post["page"] for post in post_list]
		)
	)
	container.assign(
		'generated timestamp = "%s"' %
		toReadableDate(section_contents.get("mdiocre-gen-timestamp"))
	)
	container.assign(
		"table of contents = '%s'" %
		toc
	)
	print(
		md.render(template.read(), container)
	)
