PYTHON := /usr/bin/env python3

MKFEED      := tools/mkfeed.py
MKINDEX     := tools/mkindex.py
MKINDEX_CAT := tools/mkindex_categorical.py

CAT := $(PYTHON) tools/safe_cat.py

CHECKALL    := tools/check_all.py

MDIOCRE       := mdiocre
MDIOCRE_OPTS  :=

BUILD_DIR := built
SITE_DIR  := $(BUILD_DIR)/site
THEME_DIR  := $(SITE_DIR)/css
BLOG_DIR  := $(BUILD_DIR)/site/bloge
TEMPLATE_DIR  := $(BUILD_DIR)/template

PORT ?= 8000

#
MINIBLOG_SRC := src/miniblog
MINIBLOG_DIR := $(BUILD_DIR)/site/stuff/miniblog
MKMINIBLOG := tools/mkminiblog.py
MKTEMPLATE := tools/mktemplate.py
#
MIDI_SRC := src/my_midi
MIDI_DIR := $(BUILD_DIR)/site/stuff/midi/mine
MIDICSV := midicsv
CSVMIDI := csvmidi
CSVMIDI_OPTS :=
MCOPT := tools/produce_midi.py
#

all: css .site-generated blog-index miniblog rss midi typescript

.PHONY: css blog-index miniblog rss midi typescript

check:
	$(PYTHON) $(CHECKALL) $(SITE_DIR)

clean-all: clean
	rm -rf $(BUILD_DIR)

clean:
	rm -rf $(SITE_DIR)
	rm -f .site-generated
	rm -fv $(MIDI_SRC)/*.csv

start: all
	$(PYTHON) -m http.server --directory $(SITE_DIR) $(PORT)

TEMPLATES := \
	$(TEMPLATE_DIR)/main.html \
	$(TEMPLATE_DIR)/blog.html \
	$(TEMPLATE_DIR)/blog_post.html \
	$(TEMPLATE_DIR)/subsections.html \
	$(TEMPLATE_DIR)/not_found.html \
	$(TEMPLATE_DIR)/barebones.html \
	$(TEMPLATE_DIR)/_themed_subsection.html \
	$(TEMPLATE_DIR)/mdiocre.html

THEMES := \
	$(THEME_DIR)/screen.css \
	$(THEME_DIR)/honeycomb.css \
	$(THEME_DIR)/ics.css \
	$(THEME_DIR)/geocities.css \
	$(THEME_DIR)/material.css \
	$(THEME_DIR)/material_v3.css \
	$(THEME_DIR)/print.css \
	$(THEME_DIR)/fluent.css \
	$(THEME_DIR)/weblog09.css \
	$(THEME_DIR)/memestrap.css \
	$(THEME_DIR)/blank.css \
	$(THEME_DIR)/naked.css \
	$(THEME_DIR)/current_year.css \

MIDIS := \
	$(patsubst $(MIDI_SRC)/%, $(MIDI_DIR)/%, $(shell find $(MIDI_SRC) -name '*.mid')) 

# Needed assets and templates
DEPENDENTS := \
	$(TEMPLATES) \
	$(shell find src/static/ | sed 's/ /\\ /g')

COMMIT_NO := $(shell git rev-parse --short=8 HEAD)
EXTRA_VARIABLES := 

# Build website itself
.site-generated: $(DEPENDENTS)
	mkdir -p $(SITE_DIR)
	$(MDIOCRE) $(MDIOCRE_OPTS) src/static $(SITE_DIR)
	echo "$(COMMIT_NO)" > $(SITE_DIR)/commit.txt
	touch .site-generated

blog-index: .site-generated $(BLOG_DIR)/index.html $(BLOG_DIR)/index-chrono.html

miniblog: .site-generated $(MINIBLOG_DIR)/index.html

midi: .site-generated $(MIDIS)

typescript: .site-generated
	# funny hax
	$(MAKE) -f CheckTypescript.mk

# Build website styles
css: $(THEMES)

# Build RSS feed
rss: $(BLOG_DIR)/feed.rss

# Build individual elements
# site CSS
$(THEME_DIR):
	mkdir -p $(THEME_DIR)

$(THEME_DIR)/%.css: src/css/%/*.css | $(THEME_DIR)
	$(CAT) $(sort $^) > $@

# general templates
$(TEMPLATE_DIR):
	mkdir -p $(TEMPLATE_DIR)

$(TEMPLATE_DIR)/%.html: src/templates/%.html | $(TEMPLATE_DIR)
	cp $< $@

# blog
$(BLOG_DIR):
	mkdir -p $(BLOG_DIR)

# main RSS feed
$(BLOG_DIR)/feed.rss: feed.json | $(BLOG_DIR)
	$(PYTHON) $(MKFEED) $< > $@

# Specialized targets
$(TEMPLATE_DIR)/main.html: src/templates/main.html | $(TEMPLATE_DIR)
	$(PYTHON) $(MKTEMPLATE) $< "base=.$(EXTRA_VARIABLES)" > $@
$(TEMPLATE_DIR)/blog.html: src/templates/main.html | $(TEMPLATE_DIR)
	$(PYTHON) $(MKTEMPLATE) $< "base=..$(EXTRA_VARIABLES)" > $@
$(TEMPLATE_DIR)/blog_post.html: src/templates/main.html includes/blog_post_header.html | $(TEMPLATE_DIR)
	$(PYTHON) $(MKTEMPLATE) $< "base=..,header=<!--:include: includes/blog_post_header.html-->$(EXTRA_VARIABLES)" > $@
$(TEMPLATE_DIR)/subsections.html: src/templates/main.html | $(TEMPLATE_DIR)
	$(PYTHON) $(MKTEMPLATE) $< "base=..$(EXTRA_VARIABLES)" > $@
$(TEMPLATE_DIR)/not_found.html: src/templates/main.html | $(TEMPLATE_DIR)
	$(PYTHON) $(MKTEMPLATE) $< "base=$(EXTRA_VARIABLES)" > $@
$(TEMPLATE_DIR)/mdiocre.html: src/templates/mdiocre.html | $(TEMPLATE_DIR)
	$(CAT) $< > $@

# Make index
$(BLOG_DIR)/index.html: src/static/bloge $(TEMPLATE_DIR)/subsections.html | $(BLOG_DIR)
	$(PYTHON) $(MKINDEX_CAT) $^ > $@

$(BLOG_DIR)/index-chrono.html: src/static/bloge $(TEMPLATE_DIR)/subsections.html | $(BLOG_DIR)
	$(PYTHON) $(MKINDEX) $^ > $@

# Miniblog
$(MINIBLOG_DIR):
	mkdir -p $(MINIBLOG_DIR)

$(MINIBLOG_DIR)/index.html: $(MINIBLOG_SRC) $(MINIBLOG_SRC)/_.html | $(MINIBLOG_DIR)
	$(PYTHON) $(MKMINIBLOG) $^ > $@

# Midi
$(MIDI_DIR)/%.mid: $(MIDI_SRC)/%.csv
	$(CSVMIDI) $(CSVMIDI_OPTS) $< > $@

$(MIDI_SRC)/%.csv: $(MIDI_SRC)/%.mid $(MIDI_SRC)/meta.ini
	$(MIDICSV) $< | $(PYTHON) $(MCOPT) $* $(MIDI_SRC)/meta.ini > $@
