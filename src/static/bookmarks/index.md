<!--:title="Bookmarks"-->

## Bookmarks

Note that by listing these links, I do **not** imply endorsement of any of these authors' personal views. I just find their content interesting.

### Nice reading
* [Starshine's Pages](https://dfsshine.neocities.org/)
* [57296](https://57296.neocities.org/)
* [A Bad Guide to HTML1](https://owlman.neocities.org/www/html/)
* [Baloo](https://baloo.neocities.org/)
* [InvisibleUp](https://invisibleup.com/)
* [A Vernacular Web (observations from the 90s web)](http://art.teleportacia.org/observation/vernacular/)
* [Hypermedia Systems](https://hypermedia.systems/)

### Useful/informative
* [HTML Ampersand Hash Codes](https://dylsung.neocities.org/flux/amphashcodes.htm)
* [HTML General Escape Codes](https://fantasai.tripod.com/FHQR/Characode/EscCodes.htm)
* [xmlns Attribute Hack](https://apollo.neocities.org/style/xmlnshack.html)
* [Scott2's Website Building Tutorials](http://web.archive.org/web/20211209141453if_/https://sckewi.neocities.org/webtut/)
* [CUTCODEDOWN - Minimalist Semantic Markup](https://cutcodedown.com/)

<!--:mdiocre-template="../../built/template/subsections.html"-->
