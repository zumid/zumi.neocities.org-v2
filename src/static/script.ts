// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt
// Adapted from https://github.com/pritishvaidya/read-time-estimate
// (c) 2019 Pritish

function wordsCount(text: string) {
    const pattern = '\\w+';
    const reg = new RegExp(pattern, 'g');
    return (text.match(reg) || []).length;
}

function wordsReadTime(text: string, wordsPerMin?: number) {
	if (!wordsPerMin) { wordsPerMin = 200; }
    const wordCount = wordsCount(text);
    const wordTime = wordCount / wordsPerMin;
    return wordTime;
}

function humanizeTime(time: number) {
    if (time < 0.5) { return 'less than a minute'; }
    if (time >= 0.5 && time < 1.5) { return '1 minute'; }
    return `${Math.ceil(time)} minutes`;
}

function addTimeEstimates() {
	const og_text = document.getElementById("contents");
	if (!og_text) return;

	let prepend = `<span class="readtime instapaper_hide banner"><i>${
		humanizeTime(wordsReadTime(
			og_text.innerText
		))
	}</i><br></span>`;

	document.getElementById("contents")!.innerHTML = prepend + og_text.innerHTML;
}

// @license-end

// make a menu item appear selected if there's appropriate styling
let url = document.location.href;
let menu_links: { [key: string]: number } = {
	// "page_name": menu_item_index
	"home": 0,
	"about": 1,
	"bloge": 2,
	"stuff": 3,
	"bookmarks": 4,
	"tutorials": 5
}

while (1) {
// if we're in a file, don't do adjustments
	if (location.protocol === "file:") break;

// apply appropriate selector
	let match = /\/(\w+)\//gm.exec( // @ts-ignore
		(url.endsWith("/")? url : url+"/")
	);
	
	if (match) {
		var section_name = match[1];
		if (section_name) {
			document.querySelector("#main-menu ul")!
			.children[menu_links[section_name]]
			.classList.add("selected")
		}
	} else {
		// assume the home link is selected
		document.querySelector("#main-menu ul")!
		.children[menu_links["home"]]
		.classList.add("selected")
	}

// apply time estimates if we are in a blog
	if (url.indexOf("/bloge") > -1) {
		if (url.slice(-1) === "/") break;
		if (url.indexOf("index") > -1) break;
		if (url.indexOf("index-chrono") > -1) break;
		addTimeEstimates();
		break;
	}

	break;
}

// add the commit stamp at the bottom of the page

let crq = new XMLHttpRequest();
crq.addEventListener("load", function(){
	let commit = crq.responseText.trim();
	if (/^[0-9a-f]{8}/.test(commit)) {
		let commit_holder = document.getElementById("commit-number");
		if (!commit_holder) return;
		
		// create inner content
		commit_holder.appendChild(
			document.createElement('span')
		).innerHTML = 'commit ';
		
		// commit code
		commit_holder.appendChild(
			document.createElement('code')
		).innerHTML = commit;
	}
})
crq.open("get", `/commit.txt?v=${Date.now()}`)
crq.send();


