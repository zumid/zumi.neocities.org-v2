let _base: string;
let finalStyleTag: HTMLLinkElement;

// from sckewi: check preload supports
const has_preload = (()=>{
	try { return document.createElement("link").relList.supports("preload") }
	catch (e) { return false; }
})();

function themeURL(base: string, theme: string) {
	_base = base;
	if (theme) {
		return `${base}/css/${theme}.css`;
	}
	return `${base}/screen.css`;
}

function applyTheme(base: string, theme: string, shouldNotHide: boolean) {
	// don't bother if we don't support preload
	if (!has_preload) return;

	const newStyleTag = document.createElement("link");
	const hider = document.createElement("style");
	
	const removeOriginalCSS = ()=>{
		const original_css = document.head.querySelector("link[href$='screen.css']");
		if (original_css) {
			original_css.parentElement!.removeChild(original_css);
		}
	}
	
	const insertHider = ()=>{
		hider.appendChild(
			document.createTextNode(`
			body { opacity: 0; } .loaded body { opacity: 1; }
			@media screen and (prefers-color-scheme: dark) {
				html { background-color: #111; }
				.loaded { background-color: unset; }
			}
		`)
		);
		document.head.insertAdjacentElement("beforeend", hider);
	}
	
	const removeHider = ()=>{
		hider.parentElement!.removeChild(hider);
		newStyleTag.parentElement!.removeChild(newStyleTag);
		document.children[0].classList.remove("loaded");
	}
	
	if (!shouldNotHide) insertHider();

	newStyleTag.as = "style";
	newStyleTag.rel = "preload";
	newStyleTag.href = themeURL(base, theme);

	newStyleTag.addEventListener("load", ()=>{
			// remove previous style if any
			if (finalStyleTag) {
				finalStyleTag.parentElement!.removeChild(finalStyleTag);
			}

			finalStyleTag = document.createElement("link");
			finalStyleTag.rel = "stylesheet";
			finalStyleTag.href = newStyleTag.href;
			finalStyleTag.media = "screen,projection,tv";

			document.head.insertAdjacentElement("beforeend", finalStyleTag);
			document.children[0].classList.add("loaded");
			removeOriginalCSS();

			if (!shouldNotHide) removeHider();
		}
	);

	newStyleTag.addEventListener("error", ()=>{
			// immediately remove hider
			if (!shouldNotHide) removeHider();

			newStyleTag.parentElement!.removeChild(newStyleTag);
		}
	);
	
	document.head.insertAdjacentElement("beforeend", newStyleTag);
}

// add action to theme select button when page is loaded
window.addEventListener("load", ()=>{
		// don't bother if we don't support preload
		if (!has_preload) return;
		
		// add more options
		const optionsRequest = new XMLHttpRequest();
		optionsRequest.open("get",
			_base + `/themes.json?v=${Date.now()}`,
		);

		optionsRequest.addEventListener("load", function() { // need to use `this`
				const themeOptions = document.getElementById("theme-options");
				if (!themeOptions) return;

				const themeOptionsObj: {[theme_name: string]: string} = JSON.parse(this.responseText);
				for (const theme_name in themeOptionsObj) {
					const newOption = document.createElement("option");
					newOption.value = theme_name;
					newOption.appendChild(
						document.createTextNode(themeOptionsObj[theme_name])
					);
					themeOptions.appendChild(newOption);
				}
			}
		);

		optionsRequest.send();

		// add click event
		document.getElementById("theme-switcher")!.addEventListener("click", ()=>{
				const selectionName = (
					document.getElementById("theme-options")! as HTMLSelectElement
				).selectedOptions[0].value;

				window.localStorage.setItem("theme", selectionName);
				applyTheme(_base, window.localStorage.getItem('theme')!, true);
			}
		);
	}
);
