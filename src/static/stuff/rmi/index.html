<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>.RMI - the RIFF-based MIDI file</title>
		<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1">
		<link rel="stylesheet" href="style.css" media="screen,projection,tv">
		<link rel="stylesheet" href="print.css" media="print">
		<meta name="description" content="A binary-focused explanation of the .RMI format for MIDI files.">
		<meta name="author" content="Zumi">
		<meta name="keywords" content="rmi,rmid,midi,file,format,windows 95,reference">
	</head>
	
	<body>
		<header>
			<h1>
				<span>.RMI</span>
				<span>-</span>
				<span>the RIFF-based MIDI file</span>
			</h1>
			<nav>
				<ul id="menu">
					<li><a href="#intro">Introduction</a></li>
					<li><a href="#struct">General Structure</a></li>
					<li><a href="#disp">DISP</a></li>
					<li><a href="#info">INFO</a></li>
					<li><a href="#vers">vers</a></li>
					<li><a href="#dls">DLS</a></li>
				</ul>
			</nav>
		</header>
		
		<section id="changelog">
			<h2>Changelog</h2>
			<ul>
				<li>06 March 2022 - clarify position of the <code>vers</code> chunk</li>
				<li>05 March 2022 - written and published</li>
			</ul>
		</section>
		<main>
			<h2 id="intro">Introduction <a href="#">[top]</a></h2>
			
			<p>
				As the name implies, this is essentially just a standard MIDI
				file, wrapped in a <a href="https://en.wikipedia.org/wiki/Resource_Interchange_File_Format">RIFF</a> container. This allows a MIDI file
				to contain extra tags. But wait! MIDI files <em>does</em> specify
				<a href="https://www.mixagesoftware.com/en/midikit/help/HTML/meta_events.html">meta events</a> like copyright, text, track names...
				"Hmm... there's 'tagging' alright, but we want to tag these
				the same way we did for bitmaps and WAV files," says Microsoft and IBM.
				And so they did. The metadata contained within the RMI <em>was</em> used
				if you check the file properties of one:
				<br>
				<img src="prop.png" alt="
						Windows 95 file properties box of a file named
						'In the Hall of the Mountain King', showing the
						Midisoft logo image, copyright information,
						and other info such as Artist, Display Name and
						Subject.
					"
				>
			</p>
			<p>
				Since information on it is so sparse and fragmented across
				the different standards that's used, I decided to write them as
				one reference page, which hope will help demystify this
				format a little bit. I won't be covering <em>everything</em> though. :P
			</p>
			
			<h2>Structure</h2>
			<h3 id="struct">In general <a href="#">[top]</a></h3>
			<p>
				Simply put, it's just a regular RIFF file where everything is split
				in terms of chunks in this format: [fourCC] [4-byte length] [data...]
			</p>
			<p>
				The official specs can be found <a href="https://web.archive.org/web/20110610135604im_/http://www.midi.org/about-midi/rp29spec(rmid).pdf">here</a>, this page summarizes it for quick reference.
			</p>
			<table>
				<thead>
					<th scope="col">Hex Data</th>
					<th scope="col">Size</th>
					<th scope="col">Contents</th>
				</thead>
				<tbody>
					<tr>
						<td>52 49 46 46</td>
						<td>4 bytes</td>
						<td>
							"RIFF" in ASCII
						</td>
					</tr>
					<tr>
						<td>xx xx xx xx</td>
						<td>4 bytes</td>
						<td>
							The size of the entire file
							minus the 8 needed for the
							RIFF header.
							<br>
							This is encoded
							in <strong>little-endian</strong>.
							<br>
							If your file is 12 kB = 12000 bytes = 0x2EE0,
							then you fill in 0x2ED8 = D8 2E 00 00 here.
						</td>
					</tr>
					<tr>
						<td>52 4D 49 44</td>
						<td>4 bytes</td>
						<td>
							"RMID" in ASCII
						</td>
					</tr>
					<tr>
						<td>64 61 74 61</td>
						<td>4 bytes</td>
						<td>
							"data" in ASCII
						</td>
					</tr>
					<tr>
						<td>xx xx xx xx</td>
						<td>4 bytes</td>
						<td>
							This is the size for the contents
							of the entire MIDI file that
							immediately follows this.<br>
							This is encoded
							in <strong>little-endian</strong>.
						</td>
					</tr>
					<tr>
						<td>xx xx xx xx xx ...</td>
						<td>(variable)</td>
						<td>
							The MIDI file itself - it's
							literally a copy and paste job
							(with padding to make the file
							have an even number of bytes)
						</td>
					</tr>
				</tbody>
			</table>
			
			<p>
				The official RMID spec specifically calls for this
				exact order as the bare minimum for an .RMI file.
				So it's very easy to convert a .MID into an .RMI, and
				vice versa. As the purpose is to bolt additional
				standardized metadata onto a basic MIDI file, doing just
				that might be pointless, so let's look at the other chunks
				you might come across.
			</p>
			
			<h3 id="disp">DISP <a href="#">[top]</a></h3>
			<p>
				According to Microsoft's documentation on the then-new
				RIFF chunks (titled "New Multimedia Data Types and Data Techniques"):
			</p>
			<blockquote>
				<p>
					A DISP chunk contains easily rendered and displayable objects
					associated with an instance of a more complex object in a RIFF form
					(e.g. sound file, AVI movie)...
				</p>
				<p>
					...The DISP chunk is especially beneficial when representing OLE data
					within an application. For example, when pasting a wave file into
					Excel, the creating application can use the DISP chunk to associate
					an icon and a text description to represent the embedded wave file.
					This text should be short so that it can be easily displayed in
					menu bars and under icons.
				</p>
			</blockquote>
			<p>
				To summarize, in this case, basically "attachments" to the file.
				How are they defined? Well:
			</p>
			<table>
				<thead>
					<th scope="col">Hex Data</th>
					<th scope="col">Size</th>
					<th scope="col">Contents</th>
				</thead>
				<tbody>
					<tr>
						<td>44 49 53 50</td>
						<td>4 bytes</td>
						<td>
							"DISP" in ASCII
						</td>
					</tr>
					<tr>
						<td>xx xx xx xx</td>
						<td>4 bytes</td>
						<td>
							The size of the chunk
							minus the 8 bytes for the
							header, and including
							the 4 bytes of the following
							type definition.<br>
							This is encoded
							in <strong>little-endian</strong>.
						</td>
					</tr>
					<tr>
						<td>xx 00 00 00</td>
						<td>4 bytes</td>
						<td>
							The type of the data, see
							the table below.
						</td>
					</tr>
					<tr>
						<td>xx xx xx xx...</td>
						<td>(Variable)</td>
						<td>
							The data itself.
						</td>
					</tr>
				</tbody>
			</table>
			
			<p>
				The type is specifically defined to be Windows clipboard
				formats, which I'll save you the effort of looking through
				(for the curious, it's in <code>WinUser.h</code>, and also documented <a href="https://docs.microsoft.com/en-us/windows/win32/dataxchg/standard-clipboard-formats">here</a>):
			</p>
			
			
			<table>
				<thead>
					<th scope="col">Type</th>
					<th scope="col">Contents</th>
				</thead>
				<tbody>
					<tr id="tableref-text1">
						<td>01</td>
						<td>
							Plain text (ANSI) <a href="#footnote-text">[text]</a>
						</td>
					</tr>
					<tr id="tableref-bmp">
						<td>02</td>
						<td>
							a BMP image <a href="#footnote-bmp">[bmp]</a>
						</td>
					</tr>
					<tr>
						<td>03</td>
						<td>
							<a href="https://docs.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-metafilepict">Metapicture</a> file
						</td>
					</tr>
					<tr>
						<td>04</td>
						<td>
							a SYLK spreadsheet
						</td>
					</tr>
					<tr>
						<td>05</td>
						<td>
							a Data Interchange Format spreadsheet
						</td>
					</tr>
					<tr>
						<td>06</td>
						<td>
							a TIFF image
						</td>
					</tr>
					<tr id="tableref-text2">
						<td>07</td>
						<td>
							Plain text (OEM / codepage 437) <a href="#footnote-text">[text]</a>
						</td>
					</tr>
					<tr id="tableref-dib">
						<td>08</td>
						<td>
							a DIB image, this is used as a "file icon" <a href="#footnote-dib">[dib]</a>
						</td>
					</tr>
					<tr>
						<td>09</td>
						<td>
							A <a href="https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-dclb/af3b7f27-1021-46d8-ac0f-e0218bfb008d">color palette</a>, apparently can be connected
							to the DIB image
						</td>
					</tr>
					<tr>
						<td>0A</td>
						<td>
							Pen extension data for Windows for Pen Computing (probably irrelevant here)
						</td>
					</tr>
					<tr id="tableref-riff">
						<td>0B</td>
						<td>
							a sound file inside a RIFF container <a href="#footnote-riff">[riff]</a>
						</td>
					</tr>
					<tr>
						<td>0C</td>
						<td>
							a WAV file
						</td>
					</tr>
					<tr id="tableref-text3">
						<td>0D</td>
						<td>
							Plain text (Unicode) <a href="#footnote-text">[text]</a>
						</td>
					</tr>
					<tr>
						<td>0E</td>
						<td>
							an Enhanced Metafile image
						</td>
					</tr>
					<tr>
						<td>0F</td>
						<td>
							A <a href="https://docs.microsoft.com/en-us/windows/win32/shell/clipboard#cf_hdrop">zero-terminated list of files</a> (Windows 95/NT 4 and up only)
						</td>
					</tr>
					<tr id="tableref-locale">
						<td>10</td>
						<td>
							Language of the plain text (Windows 95/NT 4 and up only) <a href="#footnote-locale">[locale]</a>
						</td>
					</tr>
					<tr>
						<td>11</td>
						<td>
							a DIB image containing color space info (aka BITMAPV5, Windows 98/NT 5 and up only)
						</td>
					</tr>
				</tbody>
			</table>
			<ul>
				<li id="footnote-dib"><a href="#tableref-dib">[dib]</a>
					Converting a DIB to conventional formats is easy:
					<code>convert rippedImage.dib rippedImage.png</code>... as for vice versa, it seems bugged when I try it.
					<br>
					A workaround for now is to convert it to a BITMAPV3 image as described in the point immediately
					below, and then chop off 14 bytes from the start of the file.</li>
				<li id="footnote-bmp"><a href="#tableref-bmp">[bmp]</a>
					I haven't actually looked into this, you might be able to get away
					using BITMAPV3:
					<code>convert myImage.png BMP3:myImage.bmp</code></li>
				<li id="footnote-riff"><a href="#tableref-riff">[riff]</a>
					I'm not exactly sure what this means, yet. (assuming it's just a regular RIFF sound file...)</li>
				<li id="footnote-locale"><a href="#tableref-locale">[locale]</a>
					I assume this would be two bytes (little endian?), with the specific values defined
					<a href="https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-lcid/a9eac961-e77d-41a6-90a5-ce1a8b0cdb9c">here</a>...</li>
				<li id="footnote-text">[text]<a href="#tableref-text1">[1]</a><a href="#tableref-text2">[2]</a><a href="#tableref-text3">[3]</a>
					Line breaks are CR+LF (0D 0A), and ends with a null byte. (00)</li>
			</ul>
			
			<h3 id="info">INFO <a href="#">[top]</a></h3>
			<p>
				This is a RIFF LIST chunk that essentially contains the tags for the RMI file, each tag
				being its own RIFF chunk in the usual format. Here's the header for this chunk:
			</p>
			<table>
				<thead>
					<th scope="col">Hex Data</th>
					<th scope="col">Size</th>
					<th scope="col">Contents</th>
				</thead>
				<tbody>
					<tr>
						<td>4C 49 53 54</td>
						<td>4 bytes</td>
						<td>
							"LIST" in ASCII
						</td>
					</tr>
					<tr>
						<td>xx xx xx xx</td>
						<td>4 bytes</td>
						<td>
							The size of the chunk
							minus the 8 bytes for the
							header, and including
							the 4 bytes of the following
							ASCII identifier.<br>
							This is encoded
							in <strong>little-endian</strong>.
						</td>
					</tr>
					<tr>
						<td>49 4E 46 4F</td>
						<td>4 bytes</td>
						<td>
							"INFO" in ASCII
						</td>
					</tr>
					<tr>
						<td>xx xx xx xx...</td>
						<td>(Variable)</td>
						<td>
							The data itself, made up of several
							RIFF sub-chunks.
						</td>
					</tr>
				</tbody>
			</table>
			<p>
				A selection of tags are written below,
				a more complete list can be found <a href="https://exiftool.org/TagNames/RIFF.html#Info">here</a>.
				The tags here are assumed to be written as plain, null-terminated text.
			</p>
			<table>
				<thead>
					<th scope="col">Tag Name</th>
					<th scope="col">Contents</th>
				</thead>
				<tbody>
					<tr>
						<td>IART</td>
						<td>Artist of the composition</td>
					</tr>
					<tr>
						<td>ICOP</td>
						<td>Copyright information</td>
					</tr>
					<tr>
						<td>ICRD</td>
						<td>Date created</td>
					</tr>
					<tr>
						<td>INAM</td>
						<td>Title of the composition</td>
					</tr>
					<tr>
						<td>ISBJ</td>
						<td>Description of the file / additional info</td>
					</tr>
					<tr>
						<td>ICMT</td>
						<td>Comments about the composition or the file</td>
					</tr>
					<tr>
						<td>ICMS</td>
						<td>Who or which entity commissioned this file</td>
					</tr>
					<tr>
						<td>IGNR</td>
						<td>The genre of the composition</td>
					</tr>
				</tbody>
			</table>
			
			<h3 id="vers">vers <a href="#">[top]</a></h3>
			<p>
				The specification also defines a version number chunk, curiously. This is
				basically the version number of the file itself.
				According to the specification, this chunk is to be placed right after
				the midi data and before the INFO chunks.
			</p>
			<p>
				I haven't seen a file
				use this yet, but I thought this was interesting. Might as well write it here:
			</p>
			<table>
				<thead>
					<th scope="col">Hex Data</th>
					<th scope="col">Size</th>
					<th scope="col">Contents</th>
				</thead>
				<tbody>
					<tr>
						<td>76 65 72 73</td>
						<td>4 bytes</td>
						<td>
							"vers" in ASCII
						</td>
					</tr>
					<tr>
						<td>08 00 00 00</td>
						<td>4 bytes</td>
						<td>
							The size of this chunk's data (8 bytes),
							in little endian
						</td>
					</tr>
					<tr>
						<td>xx xx ww ww zz zz yy yy</td>
						<td>8 bytes</td>
						<td>
							Suppose a version number like ww.xx.yy.zz,
							each number is encoded as a 16-bit little endian
							number and arranged like that.
							<br>
							Example: version 1.0.24.490 is encoded as
							<ul>
								<li>ww ww = 01 00</li>
								<li>xx xx = 00 00</li>
								<li>yy yy = 18 00</li>
								<li>zz zz = EA 01</li>
							</ul>
							So the data will look like: 00 00 01 00 EA 01 18 00
						</td>
					</tr>
				</tbody>
			</table>
			<h2 id="dls">DLS <a href="#">[top]</a></h2>
			<p>
				Perhaps one of the coolest things I didn't know an RMI file could have,
				that is basically its' own soundfont to go along with the sequence data.
				This makes MIDI files closer to something like a MOD or an XM, but
				not bound by module speed and row/frame boundaries.
			</p>
			<p>
				As far as I know, DLS data is literally appended to the end of the RMID data.
				The difference is, the size of the RIFF file itself (at offsets 4-7) need to be
				updated to reflect the entire file.
			</p>
			<h2>Final words</h2>
			<p>
				It looks like this format was significant enough for it to be picked up by
				the MIDI Manufacturer's Association as a technical note RP-029 in 2000.
				But after a while, they might have realized that this format has its shortcomings
				(like the 4GB limit, or that it's best used with one vendor, idk),
				and developed the Extensible Music Format (XMF) to replace it a year later.
			</p>
			<p>
				XMF is apparently used in mobile settings, but the thing is I've never even
				seen one of these files whereas I've only seen a few RMI's by comparison.
			</p>
		</main>
	</body>
</html>
