<html>
	<head>
		<meta charset="utf-8">
		<title>MSGS Supported Events</title>
		<meta name="description" content="Basically, this is what the default Windows MIDI synth is capable of. It can't do much, but it's just enough to jam out to most MIDI.">
		<meta name="author" content="Zumi <https://zumi.neocities.org>">
		<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1">
		<link rel="stylesheet" href="style.css" media="screen">
		<meta name="keywords" content="msgs,midi,windows,synth,swmidi,gm.dls,capabilities,roland,microsoft">
	</head>
	<body>
		<h1>
			Microsoft GS Wavetable Synth (&ldquo;MSGS&rdquo;)
			Supported Events
			<br><span>-</span>
			<small>Updated
			<time datetime="2023-03-25T21:11:27+07:00">2023-03-25</time></small>
		</h1>
		
		<aside>
			<p>
				Ah, Microsoft GS Wavetable Synth. The default MIDI driver
				that comes with every version of Windows since Windows 98.
				But this isn't any old General MIDI synthesizer. It supports
				Roland's GS extension&mdash;later made into (significant parts of)
				GM level 2, because
				I guess Roland are the big shots of the industry. <a href="https://blackmidi.antifandom.com/wiki/Software:Microsoft_GS_Wavetable_Synth#:~:text=Cannot%20use%20variant%20instruments,%20despite%20sound%20set%20including%20some.">You'd
				think the &ldquo;GS&rdquo; in its name would make it obvious</a>.
			</p>
			<p>
				It is known to be very limited, basic and dry sounding.
				At the time of creation (around 1996 or so), some of these
				limitations could be by necessity. It had to be small so as not
				to take too much space, and it had to be performant while
				delivering pretty good sound. Nowadays, it's been mostly
				derided. Even Microsoft left this thing rotting, probably
				because &ldquo;nobody is playing MIDI files anymore&rdquo;.
			</p>
			<p>
				Its lo-fi and crunchy vibe may sound like a mockery
				of the legendary Roland SC-55 samples that it's based on, and
				not to mention it's very easy to clip this thing to h*ck.
				But&hellip; if you know what you're doing, you
				<em>can</em> make it <a href="https://youtu.be/HAIDqt2aUek">sound
				cool</a>, or even <a href="https://soundcloud.com/user-416698216-971657402">push it to its
				absolute limits</a>.
			</p>
			<p>
				Sadly, its ubiquity (piggybacking off of Windows')
				caused it to be known as &ldquo;the sound&rdquo; of MIDI,
				despite that MIDI (as a protocol) inherently can take on
				many different sounds. I do think this warps
				what people (other than electronic musicians) think
				of MIDI as a general.
			</p>
			<p>
				In any case, this document will attempt to explain
				what events this familiar software synth can handle,
				since existing documentation on this thing is sparse.
				It's best to assume that events not described here
				aren't processed by this synth. 
			</p>
		</aside>
		
		<p>
			Numbers denoted like <code>FF</code> represent a hexadecimal
			number. <code>F<var>x</var></code> means <code>F0</code> plus
			<var>x</var>, where <var>x</var> is a variable
			number from 0-15 (<code>0</code> to <code>F</code>).
		</p>
		
		<p>
			<strong>GENERAL NOTE:</strong>
			Roland's GS standard has the concept of <em>parts</em> as
			separate from MIDI <em>channels</em>. There are 16 of them,
			and by default, these match their respective MIDI channels
			(Part 1 maps to Ch.1, Part 2 maps to Ch.2, and so on).
		</p><p>
			Part IDs (which marks their location in a GS device's memory)
			are mapped in a weird way. Part 10 has ID 0.
			Parts 11 to 16 are assigned the IDs 10 - 15, while the
			rest has IDs matching their part numbers (e.g. Part 1 has ID 1).
		</p>
		
		<h2 id="messages">Messages <a href="#messages">#</a></h2>
		
		<dl>
			<dt>Note off (<code>8<var>x</var></code>)
			and note on (<code>9<var>x</var></code>)</dt>
			<dd>The most basic MIDI event.</dd>
			
			<dt>Control change (<code>B<var>x</var></code>)</dt>
			<dd>See <a href="#cc">the list of supported CCs</a>.</dd>
			
			<dt>Program change (<code>C<var>x</var></code>)</dt>
			<dd>For changing the currently playing instrument of a part.</dd>
			
			<dt>Pitch bend (<code>E<var>x</var></code>)</dt>
			<dd>For twangs and the like.</dd>
		</dl>
		
		<h2 id="cc">Control changes (CC) <a href="#cc">#</a></h2>
		
		<p>
			For this section, CC numbers will be given in decimal
			and will be represented as regular text.
		</p>
		
		<dl>
			<dt>Bank select (MSB: 0, LSB: 32)</dt>
			<dd>
				<p>
				Sets the bank for the next program change. This allows
				the use for variations on instrument sounds. For example,
				patch 81 is a square wave <em>lead</em>, but setting the bank
				MSB to 1 will produce as <em>pure</em> as approximation of a 50%
				square wave as possible. Setting it to 8 will give you a
				sine wave instead. You can refer to <a href="https://battleofthebits.org/lyceum/View/Specification+of+General+MIDI+and+Roland+MT-32+patches/#Roland%20GS%20extended%20patches">this list</a> for a short description
				of what variations are available.
				</p>
				<p>
				Keep in mind that setting this CC <em>after</em> a program change will
				<strong>not</strong> have any effect!
				In addition, this will only work <strong>after</strong>
				the <a href="#vendor-roland"><b>GS Reset</b> SysEx has
				been sent</a>. Some software such as <a href="http://www.vanbasco.com/karaokeplayer/">vanBasco's Karaoke Player</a>
				can do this automatically, however.
				</p>
			</dd>
			
			<dt>RPN select (MSB: 101, LSB: 100)</dt>
			<dd>
				Sets the RPN to be affected by the Data entry CC.
				See <a href="#rpn">the supported RPN list</a>.
			</dd>
			
			<dt>NRPN select (MSB: 99, LSB: 98)</dt>
			<dd>
				<p>
				Normally sets NRPN to be affected by the Data entry CC.
				In MSGS however, <strong>no NRPNs are supported</strong>,
				leaving this effectively useless.
				</p>
				<p>
				That's not to say it doesn't do anything, however.
				If this CC is encountered, MSGS always sets it to
				<code>3FFF</code>, the RPN reset ID.
				</p>
			</dd>
			
			<dt>Data entry (MSB: 6, LSB: 38)</dt>
			<dd>
				Sets the currently selected RPN or NRPN to the
				specified value.
			</dd>
			
			<dt>Modulation wheel (MSB: 1, <del>LSB: 33</del>)</dt>
			<dd>
				Sets an automatic vibrato effect, set to a rate of about 5 Hz.
				Its equivalent LSB CC is <strong>ignored</strong>.
			</dd>
			
			<dt>Volume (7)</dt>
			<dd>
				Sets the current part's overall volume level (0&ndash;127).
			</dd>
			
			<dt>Pan (10)</dt>
			<dd>
				Sets the current part's panning balance (0 = hard left, 64 = centered, 127 = hard right).
			</dd>
			
			<dt>Expression (11)</dt>
			<dd>
				Modifies the part's volume continuously. Useful for things
				like crescendos.
			</dd>
			
			<dt>Hold/sustain pedal (64)</dt>
			<dd>
				When set, this sustains currently-playing notes until
				it is reset.
			</dd>
			
			<dt>All sound off (120)</dt>
			<dd>
				Kills all sound indiscriminately. Can also be used as
				a "panic" button.
			</dd>
			
			<dt>All controllers off (121)</dt>
			<dd>
				Resets the state of all controllers. Specifically:
				<ul>
					<li>Volume = 100 (about -4 dB)</li>
					<li>Pan = 64 (center)</li>
					<li>Expression = 127 (max)</li>
					<li>Volume = <code>2000</code> (2 semitones)</li>
					<li>Mod Wheel = 0 (off)</li>
				</ul>
			</dd>
			
			<dt>All notes off (123)</dt>
			<dd>
				Kills all notes. If the Hold/Sustain Pedal is enabled,
				it <strong>waits</strong> until that particular CC is
				disabled first.
			</dd>
			
			<dt>Mono mode (126)</dt>
			<dd>
				Kills all notes and enables Mono mode for this
				particular part.
			</dd>
			
			<dt>Poly mode (127)</dt>
			<dd>
				Kills all notes and enables Poly mode for this
				particular part.
			</dd>
		</dl>
		
		<aside>
			<p>
				As is often noted, MSGS doesn't support effect CCs such
				as 91 (reverb send), 93 (chorus send), and 94 (delay send).
				Neither does it support ADSR manipulation CCs like
				72 (release), 73 (attack) and 75 (decay; but this is a
				GM2 CC).
			</p>
			<p>
				Personally I want the ADSR ones, but alas, none to be
				found. Wish it did.
				(&ldquo;You already have one: it's called
			<a href="http://coolsoft.altervista.org/en/virtualmidisynth">VirtualMidiSynth</a>!&rdquo;)
			</p>
			<p>
				There's another option, however. Edirol VSC 1.60 + <a href="https://www.vogons.org/viewtopic.php?p=313243">VST SysEx patch</a> + <a href="https://github.com/DB50XG/vstdriver">VSTi Driver</a> may be the closest direct substitute.
				Given the <em>very</em> similar sound (it was made around the time of MSGS after all, around 2001)
				<strong>and</strong> being actually able to read the ADSR CCs I wanted, it's like
				a better version of MSGS that almost retains the lo-fi-ness but has more support
				for effects.
			</p>
		</aside>
		
		<h2 id="rpn">Registered Parameter Numbers <a href="#rpn">#</a></h2>
		
		<p>
		RPN numbers are given as <code><var>xx</var><var>yy</var></code>,
		where <var>xx</var> is the RPN number's MSB, and <var>yy</var> is its LSB.
		</p>
		
		<dl>
			<dt>Pitch bend (<code>0000</code>)</dt>
			<dd>
				Range of bending for the Pitch Bend message.
				
				Upon data entry, <strong>only MSB</strong> is taken
				into account, while LSB is ignored.
				
				In this case, this means only the range in <em>semitones</em>
				are considered.
			</dd>
			
			<dt>Fine tune (<code>0001</code>)</dt>
			<dd>
				Transposes the entire part by some amount of <em>cents</em>
				relative to <code>2000</code> (= A 440Hz)
				Both LSB and MSB are accepted on data entry.
			</dd>
			
			<dt>Coarse tune (<code>0002</code>)</dt>
			<dd>
				Transposes the entire part by some amount
				of <em>semitones</em> relative to MSB = <code>40</code>
				(= A 440Hz).
				
				Upon data entry, <strong>only MSB</strong> is taken
				into account, while LSB is ignored.
			</dd>
		</dl>
		
		<aside>
			<p>
				If you were expecting to change the modulation
				level or speed here, just remember: this is MSGS.
			</p>
		</aside>
		
		<h2 id="sysex">System Exclusives (SysEx) <a href="#sysex">#</a></h2>
		
		<p>
			This section will use the following convention:
			<br><br>
			Let <var>SYSEX</var>, where <var>SYSEX</var> is an
			array containing the entire SysEx string as a series
			of 8-bit numbers from start (marked with <code>F0</code>) to finish
			(marked with <code>F7</code>). The array starts at 0, so
			<var>SYSEX[0]</var> = <code>F0</code>.
			<br><br>
			Array slices are written in Python's slice notation, so
			<var>SYSEX[:3]</var> == <var>SYSEX[0:3]</var> ==
			{<var>SYSEX[0]</var>, <var>SYSEX[1]</var>, <var>SYSEX[2]</var>}.
		</p>
		<p>
			Example: <samp><var>SYSEX</var> = <code>F0 41 10 42 12 40 00 7F 00 41 F7</code></samp>
		</p>
			<ul>
				<li><var>SYSEX[0]</var>: <code>F0</code></li>
				<li><var>SYSEX[1:3]</var>: <code>41 10</code></li>
				<li><var>SYSEX[3:7]</var>: <code>42 12 40 00</code></li>
				<li>and so on&hellip;</li>
			</ul>
		
		<p>
			In general:
		</p>
		<ul>
			<li>
				<var>SYSEX</var> must have a minimum length of 6 bytes.
			</li>
			<li>
				<var>SYSEX[2]</var> normally contains the device or
				sysex channel ID.
				It is <strong>ignored</strong> in MSGS.
			</li>
		</ul>
		
		<p>
			<var>SYSEX[1]</var> contains a vendor ID, of which only
			three are recognized:
		</p>
		<ul>
			<li>
				<code>7E</code>
				for
				<a href="#vendor-nonrt">Universal non-realtime</a>
			</li>
			<li>
				<code>7F</code>
				for
				<a href="#vendor-rt">Universal realtime</a>
			</li>
			<li>
				<code>41</code>
				for
				<a href="#vendor-roland">Roland</a>
			</li>
		</ul>
		
		<h3 id="vendor-nonrt">Universal non-realtime
		<a href="#vendor-nonrt">#</a></h2>
		<p>
			Only <b>GM Reset</b> is supported by this vendor ID:
		</p>
		<ul>
			<li>
				<var>SYSEX[3]</var> = <code>09</code>
				<br>
				<b>GM Reset</b>. Specifically:
				<ul>
					<li>Reset all parts data.</li>
					<li>Disable GS.</li>
					<li>Reset fine tune.</li>
					<li>Restore default scale tuning (and coarse tuning).</li>
					<li>Set polyphonic mode.</li>
					<li>Restore drums to Ch.10 only.</li>
					<li>Set master volume to 0.</li>
				</ul>
			</li>
			<li>
				Continuing from above, <var>SYSEX[4]</var> = <code>01</code>.
				<br>
				This enables strict GM mode. However, whichever value is found
				in here <a href="https://midi.org/midi/forum-old/3918-microsoft-gs-wavetable-synth-gm2#reply-3927">doesn't seem to actually matter</a>, and will set GM mode regardless.
			</li>
		</ul>
		
		<h3 id="vendor-rt">Universal realtime
		<a href="#vendor-rt">#</a></h2>
		<p>
			Only <b>Set Master Volume</b> is supported by this vendor ID:
		</p>
		<ul>
			<li>
				<var>SYSEX[3:7]</var> = <code>04 01 <var>??</var>
				<var>xx</var></code>
				<br>
				<b>Set Master Volume</b> where <var>xx</var> is the volume. 
				<var>??</var> is <strong>ignored</strong>.
			</li>
		</ul>
		
		<h3 id="vendor-roland">Roland <a href="#roland">#</a></h2>
		
		<p>
			For this vendor ID:
		</p>
		<ul>
			<li>
				<var>SYSEX</var> must have a minimum length of 11 bytes.
				This includes the checksum byte.
				
				<p>
				However, although the checksum byte <em>must</em> be present to
				fulfill the 11 bytes minimum, it is <strong>not
				actually checked</strong>.
				Keep in mind that setting an invalid checksum
				may render it not working on proper GS hardware.
				</p>
			</li>
			<li>
				<var>SYSEX[3:5]</var> must be <code>42 12</code>
				(pick GS model ID, send the following SysEx data).
			</li>
		</ul>
		
		<p>
			<var>SYSEX[5:8]</var> contains the GS address to be
			modified (and hence the operation to be done), and will
			be one of the following:
		</p>
		<ul>
			<li>
				<code>40 00 7F</code> = <b>GS Reset</b>.
				<p>
				This switches the
				synth to GS-compatible mode, and <strong>must be sent
				first</strong> in order to use the operations below,
				as well as the <em>bank select CC</em>.
				Any data following this reset address won't have any effect.
				</p>
			</li>
			<li>
				<code>40 1<var>x</var> 02</code> = <b>Set MIDI Channel for
				This Part</b>.
				<p>
				<var>x</var> is the part ID.
				</p><p>
				A single byte <code><var>yy</var></code> follows, and is
				the MIDI channel to map that part to. I believe this is
				expressed normally, with <code>00</code> specifying
				the first MIDI channel and <code>0F</code> specifying the
				last one.
				</p>
			</li>
			<li>
				<code>40 1<var>x</var> 15</code> = <b>Set as Drum Channel</b>.
				<p>
				<var>x</var> is the part ID.
				</p><p>
				A single byte <code><var>yy</var></code> follows, and is
				either <code>00</code> (for False) or <code>01</code>
				(for True).
				</p>
			</li>
			<li>
				<code>40 1<var>x</var> 40</code> = <b>Scale Tuning</b>.
				<p>
				<var>x</var> is the part ID.
				</p><p>
				Directly following this is the tuning adjustment for
				12 notes on the scale, made up of 7-bit numbers
				where <code>00</code> is an adjustment of -64.
				If tuning data is for less than 12 notes, it can
				be terminated by inserting an <code>80</code>.
				</p>
			</li>
		</ul>
		
		<h2 id="additional-info">Additional Information
		<a href="#additional-info">#</a></h2>
		
		<ul>
			<li>The maximum number of MIDI events it can
			handle per second is 1000.</li>
			<li>Maximum polyphony appears to be 32 voices, although some say
			it's 64 in Windows XP. "Certain sources" say this appears to
			be actually 48 (+6 overload voices). Whatever the case,
			it seems noticeably reduced by now.</li>
			<li>A quirk of the synth is that one note played in quick
			succession will <strong>immediately kill the previous instance
			of that note</strong>, say a snare drum with velocity 127
			and then another with velocity 59&mdash;the lower velocity
			snare drum will
			cut off the higher velocity one <em>including envelopes</em>.
			<p>This effect may
			be used for echo effects or a quick release, however
			this may not work as expected on more advanced synths.</p></li>
			<li>The lack of customization for CC 1 (Mod Wheel)
			may drive you to use the pitch wheel messages. In which
			case, ensure that the ranges are defined by setting
			RPN <code>0000</code> appropriately.</li>
			<li>MSGS <strong>prioritizes</strong> notes playing at
			lower channel numbers
			than higher ones, for example Ch.1 is prioritized over
			Ch.2. If you find yourself bumping up against
			the polyphony limit, consider giving your channel layout
			a look.</li>
		</ul>
		
		<aside>
			Got something to add? Feel free to <a href="https://zumi.neocities.org">reach out</a>.
		</aside>
		
		<h2 id="see-also">See Also <a href="#see-also">#</a></h2>
		
		<ul>
			<li><a href="https://battleofthebits.org/lyceum/View/midi%20(format)">BotB's Lyceum entry on MSGS</a>. Contains general info on how to compose for this thing and basically summarizes this entire page in practical terms (for most use cases).</li>
			<li><a href="https://github.com/microsoftarchive/msdn-code-gallery-microsoft/blob/master/Official%20Windows%20Driver%20Kit%20Sample/Windows%20Driver%20Kit%20(WDK)%208.1%20Samples/%5BC%2B%2B%5D-windows-driver-kit-81-cpp/WDK%208.1%20C%2B%2B%20Samples/DirectMusic%20Software%20Synthesizer%20Sample/C%2B%2B/control.cpp">control.cpp from the DirectMusic Software Synthesizer Sample of WDK 8.1</a> <a href="https://archive.is/wip/AeF7M">[archived]</a>. Allegedly, MSGS works pretty much the same way here, just without support for XG.</li>
			<li><a href="http://midi.teragonaudio.com/tech/midispec.htm">J. Glatt's unofficial MIDI specs</a> archived by Teragon Audio.</li>
			<li><a href="https://cdn.roland.com/assets/media/pdf/SC-55_OM.pdf">Roland SC-55 owner's manual</a>. This is known to be the synthesizer that MSGS emulates (poorly), and you may find more info about GS and Roland's SysEx structure here.</li>
			<li><a href="http://www.2writers.com/eddie/tutsysex.htm">Eddie's guide to <s>the galaxy</s> Roland MIDI SysEx</a>. This one has a section on calculating checksums, if you find the manual too technical to read through.</li>
		</ul>
		
		
	</body>
</html>
