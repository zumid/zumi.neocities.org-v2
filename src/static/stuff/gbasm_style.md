<!--:mdiocre-template="../../built/template/barebones.html"-->
<!--:title="Some Style Guide for GBASM"-->

This style guide is based on the `pret` style guide as seen [here](https://github.com/pret/pokecrystal/blob/master/STYLE.md).

Working on some disasm stuff, hopefully this should be a reminder.

## Code
```asm

; For code, use ONE TAB, followed by the INSTRUCTION, followed by ARGUMENTS
; separated with ONE SPACE each.

.SomeData:
	db 0 ; flags

.SomeCode:
	ld a, 2
	call .SomewhereElse
	jr .SomeCode

; Use spacing for math expressions.

	ld hl, wSomething + 10
	ld de, wFoobar + FOO_OFFSET

; Capitalize EQU.

NUM_SOMETHING EQU const_value - 1
UP_ARROW      EQU $f0

; Make sure the spacing lines up neatly. Spaces, not tabs, are recommended
; precisely because of this.

\1Field0c::      db
\1Field0d::      db
\1Field0e::      db
\1StopDelay::    db

; Don't have trailing spaces.
; Add newlines at the end of files. This makes it slightly easier to
; contribute new code.

; Add blank lines between different points of data. For a list of pointers,
; the table and the datapoint is separated by a blank line.
; Between each datapoint, separate each with blank lines.

Table::
	dw .Dataone
	dw .Datatwo
	dw .Dataone
	dw .Datathree

.Dataone:
	db 0, 1, 2, 3

.Datatwo:
	db 4, 5, 6, 7

.Datathree:
	db 8, 9, 10, 11

; Use HLI. Do not use HL+ / HL-, nor LDI / LDD. (pret)

	xor a
	ld c, 6
	ld hl, wRoom
.loop
	ld [hli], a
	dec c
	jr nz, .loop

; When the meaning of a hard-coded number isn't obvious, it should
; just stay as hex. (pret)

; Make sure there are no address comments - you should check the SYM files
; for that.

```

## Comments

```asm

; Use tabs for indentation, and spaces for alignment.
; When tabs get in the way of alignment, use spaces instead.

; Comments lead with spaces after the semicolon.

; 80 char soft limit. This isn't enforced, but you should try to keep lines from being any longer.
; rgbasm doesn't have newline escapes so there's no avoiding exceeding the limit for longer macros.

; capitalization and punctuation dont matter
; but it should be consistent with the surroundings

; Space out paragraphs with two newlines. Don't put ;s on blank lines.

; Code comments should be wrapped to whatever
; is most readable rather than what's closest
; to the 80 char limit.

; Especially since most code is going to be
; under 30 chars per line, including tabs.

; Comments should go above the code they're describing, not below, and not inline.

	ld a, [hl]
	add b
	ld [hl], a
	ret

; Avoid comments on the same line as instructions.

	cp 10
	jr c, .elsewhere ; don't do this

; If you have a larger comment and want to make
; a side note on a snippet of code:

	; You can indent the comment,
	; but each line should be shorter
	; and spaced away from the parent comment

	halt
	nop

; To comment out code, put the ; before the tab indent.

;	nop
	cp 10
;	jr c, .bye
	ret

; Generally, avoid the following:

Function0deadb33f: ; do foo
	ld a, 1
	ret

SomeFunc:
	ld a, 2
	ld b, 6
	call Function0deadb33f ; do foo
	ret

; /end avoid
```

## Labels

```asm

; ROM Labels

; Use one colon for code-wide labels.

PascalCase: ; label

; Double colons are exported by RGBASM, so use that for global labels.

PascalCase::

; No colons for local jump

.snake_case

; Use one colon for an atomic chunk of code or data that's local

.PascalCase:

; don't define unused labels when a comment would do

; Labels are prefixed with lower case letters depending on location
wPascalCase: ; wram
sPascalCase: ; sram
vPascalCase: ; vram
hPascalCase: ; hram
PascalCase:  ; rom

; Some constants are also prefixed
rBGP EQU $ff47 ; hardware register

; Most other constants should be upper case
UPPER_CASE EQU 1


; Long lists of constants should be aligned
SHORT_CONSTANT       EQU 1
LONGER_CONSTANT      EQU 2
PRETTY_LONG_CONSTANT EQU 3
TINY                 EQU 4

BUT_ONLY_RELATED_CONSTANTS EQU 5

```

## Directives

```asm

; meta and high-level directives should be uppercase
SECTION "section", ROMX
INCLUDE "filename"
INCBIN "filename"
my_macro: MACRO
	nop
ENDM
TEST EQUS "test"
PURGE TEST
TEST EQU 2

; data macros should be lowercase
	db 1
	dw 2
	my_macro SOME_CONSTANT

	; one exception is RGB
	RGB 31, 31, 31

; code macros are currently lowercase but this seems to be causing confusion with actual instructions
	ld b, TEST
	farcall DistantFunction
	ret

; the rest is up to you, just be consistent (prefer lowercase)
set X, 1
rept 10
	nop
endr

```

## Macros

```asm

when_in_doubt_lowercase: MACRO

; only shift if it's required or more readable

	; dont
	db \1
	shift
	db \1

	; do
rept 10
	db \1
	shift
endr

	; do
	db \1, \2, \3, \4, \5
	shift 5
	db \1, \2, \3, \4, \5

ENDM


UPPER_CASE_IS_OK_SOMETIMES: MACRO

; but I can't think of any examples besides ACRONYMS

ENDM

```
