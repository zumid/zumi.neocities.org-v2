<html>
	<head>
		<meta charset="utf-8">
		<title>MSU1 Abridged Specs</title>
		<link rel="stylesheet" href="style.css" media="screen,tv,projection">
	</head>
	
	<body>
		<h1>MSU1 Media Enhancement<br><small>The Abridged Specs</small></h1>
		
		<h2 id="whatis">What?</h2>
		<p>Essentially <i>SNES-CD</i> in the form of an emulator expansion chip. Also sometimes known as a "fancy ROM". Yet another project in a long line of Near's (Byuu's) work. It's supported in Snes9x (SNES only), higan (also with SGB), bsnes (also with SGB), and FX Pak (SD2SNES and also with SGB). Supports both audio streaming (through MSU1-formatted .pcm files) and general data streaming (including FMV, through an .msu data file).</p>
		<p>The <a href="#msufile">datafile</a> must be present to enable MSU1. If a datafile isn't needed, then it can simply be left empty. Two known versions exist - version 1 is the <a href="https://archive.is/BhIBx">initial specifications from 2012</a>. Version 2, first supported in higan v0.96, added the "resume" bit in the <a href="#trackcontrol">track control</a> register.</p>
		
		<p><b>Limitations</b>:
			<ul>
				<li>The data file can be used as ROM only (not for save RAM)</li>
				<li>Can only play one PCM track at a time. It may, however, be used with the SPC700 (or GameBoy hardware, in the case of SGB) to provide other music or sound effects.</li>
			</ul>
		</p>
		
		<h2 id="ports">Ports</h2>
		<h3 id="portoverview">Overview</h3>
		<p>The MSU1 registers uses CPU addresses $2000 to $2007. A breakdown of
		its functions for R/W is as follows.
		<table>
			<thead>
				<th scope="col">Address</th>
				<th scope="col">Read</th>
				<th scope="col">Write</th>
			</thead>
			<tbody>
				<tr>
					<td>$2000</td>
					<td>MSU1 status</td>
					<td rowspan=4>Data seek</td>
				</tr>
				<tr>
					<td>$2001</td>
					<td>Data read</td>
				</tr>
				<tr>
					<td>$2002</td>
					<td rowspan=6>Identification</td>
				</tr>
				<tr>
					<td>$2003</td>
				</tr>
				<tr>
					<td>$2004</td>
					<td rowspan=2>Track number</td>
				</tr>
				<tr>
					<td>$2005</td>
				</tr>
				<tr>
					<td>$2006</td>
					<td>Track volume</td>
				</tr>
				<tr>
					<td>$2007</td>
					<td>Track control</td>
				</tr>
			</tbody>
		</table>
		</p>
		<h3 id="status">$2000 (R) - Status</h3>
		<p>This register contains current information about the MSU1's state. The following bits map to these MSU1 states.
		<table>
		<thead>
			<th scope="col">7</th>
			<th scope="col">6</th>
			<th scope="col">5</th>
			<th scope="col">4</th>
			<th scope="col">3</th>
			<th scope="col">2</th>
			<th scope="col">1</th>
			<th scope="col">0</th>
		</thead>
		<tbody>
			<tr>
				<td>Data busy</td>
				<td>Audio busy</td>
				<td>Audio loop</td>
				<td>Audio playing</td>
				<td>Track missing</td>
				<td colspan=3>Version</td>
			</tr>
		</tbody>
		</table>
		</p>
		<p>Bits 0-2 indicate the current version of the MSU1 present. It can be
		used to check for a specific feature, such as <a href="#trackcontrol">track resume</a>.</p>
		<p>Bit 3 is set when the MSU1 cannot find some audio track to be played.
		This allows fallback to SPC700 music for select tracks.</p>
		<p>Bits 4-5 can be read to determine if an audio track is playing or
		is a looping track.</p>
		<p>Bit 6 is set when the MSU1 is currently reading an audio track. This
		effect is more obvious under slow hard drives or through the FX Pak, which
		includes the MSU1. You may check for this bit by using these instructions:
		<pre><code>	bit $2000
	bvs audio_busy	; branch if audio is busy.</code></pre></p>
		<p>Bit 7 is set when the MSU1 is currently reading from the data file. You may check for this bit by using these instructions:
		<pre><code>	bit $2000
	bmi data_busy	; branch if data is busy.</code></pre></p>
		
		<h3 id="seeking">$2000 - $2003 (W) - Seeking</h3>
		<p>These registers denote a 32-bit address in little-endian ($2000 is the Least Significant Byte). For example, a seek to address $524C would mean a write to $2000 in the form of: <code>4C 52 00 00</code>. <strong>The last write to $2003 triggers the seek</strong>, and will set the "Data busy" bit on <a href="#status">$2000</a> until done.</p>
		
		<h3 id="dataread">$2001 (R) - Data read</h3>
		<p>If a read is done on this port, then it will return the data from the current position in the data file, followed by the MSU1 incrementing the seek address on the file. Any read from this register <strong>requires the "Data busy" bit on <a href="#status">$2000</a> be cleared</strong>. Reading from the data file won't work otherwise.</p>
		
		<h3 id="identification">$2002 - $2007 (R) - Identification</h3>
		<p>When read from these addresses, it returns the expansion chip's identification
		string / "magic number". It should be a 6-byte string spelling "S-MSU1" in ASCII, in order (big-endian).
		This may be checked so that the game may still provide SPC700 music if the chip is not
		present.</p>
		
		<h3 id="tracknumber">$2004 - $2005 (W) - Set track number</h3>
		<p>This is a 16-bit number in little endian ($2004 is the Least Significant Byte), therefore a maximum of 65536 audio tracks can be supported. As with the <a href="#seeking">seek</a> register, <strong>the write to the last byte ($2005) triggers the audio load</strong>, and as such will set the "Audio busy" bit until the track finishes loading.
		
		<h3 id="trackvolume">$2006 (W) - Set track volume</h3>
		<p>This number scales linearly, from $00 = 0%, $80 = 50%, and $FF = 100%.</p>
		
		<h3 id="trackcontrol">$2007 (W) - Track control</h3>
		<p>This register controls MSU1 audio playback. The following bits map to these conditions.
		<table>
		<thead>
			<th scope="col">Revision</th>
			<th scope="col">7</th>
			<th scope="col">6</th>
			<th scope="col">5</th>
			<th scope="col">4</th>
			<th scope="col">3</th>
			<th scope="col">2</th>
			<th scope="col">1</th>
			<th scope="col">0</th>
		</thead>
		<tbody>
			<tr>
				<th scope="row">1</th>
				<td colspan=6>(reserved)</td>
				<td>Looping</td>
				<td>Play</td>
			</tr>
			<tr>
				<th scope="row">2</th>
				<td colspan=5>(reserved)</td>
				<td>Resume</td>
				<td>Looping</td>
				<td>Pause/Play</td>
			</tr>
		</tbody>
		</table>
		</p>
		<p>After <a href="#tracknumber">setting the track number</a>, the audio can then be set to be played by writing to this register, primarily by using bits 0-1, where bit 0 is set so that it may be played, and bit 1 is set if the audio is desired to be looped.</p>
		<p>Resetting bit 0 will stop the track. However, version 2 adds the ability to resume the audio track by setting bit 2. It might be possible to load and play another audio track, then return to the previous track by loading it again and immediately setting bit 2 to resume it.</p>
		<p>Keep in mind that <strong>any write to this register will require the "Audio busy" bit on <a href="#status">$2000</a> be cleared</strong>.</p>
		
		<h2 id="datafiles">Data Files</h2>
		<h3 id="msufile">[ROM name].msu (bsnes) - msu1/data.rom (higan)</h3>
		<p>Only one of these is required, and it must match the .sfc ROM name, only changing the .sfc to .msu. It stores additional data that should be loaded / streamed directly into SNES memory such as additional level data or FMV.</p>
		<p>If the MSU1 is needed for audio streaming only, this file should still be present but left blank, through e.g. <code>touch [ROM name].msu</code>, or by making a new text file and renaming it to <code>[ROM name].msu</code>.
		
		<h3 id="pcmfile">[ROM name]-[number].pcm (bsnes) - msu1/track-[number].pcm (higan)</h3>
		<p>These are audio files to be streamed by the chip, where [ROM name] matches the .sfc ROM name, and [number] is the MSU1 track index in decimal, without leading zeroes. For example, if $0010 is written (as little-endian) to the <a href="#tracknumber">track number register</a>, then the file [ROM name]-16.pcm will be loaded for streaming. The maximum file size allowed is 16 GB, or about 27 hours of audio.</p>
		<p>These are 16-bit, stereo (left first), little-endian PCM files at 44100 Hz with a simple 8-byte header, resulting in the following format:
			<table>
				<thead>
					<th>Position</th>
					<th>Data</th>
					<th>Comment</th>
					<th>Size</th>
				</thead>
				<tbody>
					<tr>
						<td>0</td>
						<td><code>4D 53 55 31</code></td>
						<td>"MSU1" in ASCII</td>
						<td>4</td>
					</tr>
					<tr>
						<td>4</td>
						<td><code>xx xx xx xx</code></td>
						<td>Loop point (in samples) as a 32-bit unsigned number in little-endian. If loop is undesirable, it should be set to all zeroes.</td>
						<td>4</td>
					</tr>
					<tr>
						<td>8</td>
						<td><code>xx xx yy yy, xx xx yy yy, ...</code></td>
						<td>Stereo samples, each 16-bit signed little endian. Left sample, right sample, left of next sample, right of next sample, and so on.</td>
						<td>(variable)</td>
					</tr>
				</tbody>
			</table>
		</p>
		
		<p>This means that they are effectively .WAV files. You can make one in Audacity by loading any audio file, saving it as a "RAW (header-less)" file (under Export -> format dropdown -> "Other uncompressed files") with the "Signed 16-bit PCM" encoding. Then, load the exported file in a hex editor and prepend the 8-byte header before the data.</p>
		<p>To make this process easier, use a tool such as <a href="https://github.com/qwertymodo/msupcmplusplus">msupcm++</a>, which takes in a <a href="example.json">JSON file</a> and outputs a bunch of .pcm files from any audio format (flac / wav) <!--CAUTION: if you give a shit, this tool will output high-frequency noise!--></p>
		<p>These PCM files can be played back in ffplay (without looping): <code>ffplay -ar 44100 -ac 2 -f s16le something.pcm</code> or in foobar2000 using <a href="https://github.com/qwertymodo/foo_input_msu">foo_input_msu</a>.</p>
		
		<h2 id="references">References</h2>
		<ul>
			<li>Kawa, 2012. <i>Programming the MSU1</i>. <a href="https://archive.is/BhIBx">Archived</a> from <a href="https://helmet.kafuka.org/msu1.htm">https://helmet.kafuka.org/msu1.htm</a>.</li>
			<li>Near and contributors, 2019. <i>bsnes-emu/bsnes - msu1.cpp</i>. <a href="https://archive.is/pBU3r">Archived</a> from <a href="https://github.com/bsnes-emu/bsnes/blob/master/bsnes/sfc/coprocessor/msu1/msu1.cpp">https://github.com/bsnes-emu/bsnes/blob/master/bsnes/sfc/coprocessor/msu1/msu1.cpp</a>.</li>
			<li>Near and contributors, 2019. <i>bsnes-emu/bsnes - msu1.hpp</i>. <a href="https://archive.is/LAK9r">Archived</a> from <a href="https://github.com/bsnes-emu/bsnes/blob/master/bsnes/sfc/coprocessor/msu1/msu1.hpp">https://github.com/bsnes-emu/bsnes/blob/master/bsnes/sfc/coprocessor/msu1/msu1.hpp</a>.</li>
			<li>Devin Acker and contributors, 2020. <i>devinacker/bsnes-plus - msu1.cpp</i>. <a href="https://archive.is/ClhQP">Archived</a> from <a href="https://github.com/devinacker/bsnes-plus/blob/master/bsnes/snes/chip/msu1/msu1.cpp">https://github.com/devinacker/bsnes-plus/blob/master/bsnes/snes/chip/msu1/msu1.cpp</a>.</li>
			<li>Devin Acker and contributors, 2020. <i>devinacker/bsnes-plus - msu1.hpp</i>. <a href="https://archive.is/oVavr">Archived</a> from <a href="https://github.com/devinacker/bsnes-plus/blob/master/bsnes/snes/chip/msu1/msu1.hpp">https://github.com/devinacker/bsnes-plus/blob/master/bsnes/snes/chip/msu1/msu1.hpp</a>.</li>
			<li>nocash, 2014. <i>MSU1 Specs</i>. [Forum thread]. <a href="https://archive.is/HGI94">Archived</a> from <a href="http://forums.nesdev.com/viewtopic.php?f=12&t=11004">http://forums.nesdev.com/viewtopic.php?f=12&t=11004</a>.</li>
		</ul>
		
		<h2 id="samples">Sample code</h2>
		<ul>
			<li><a href="sgb_msu1_bootstrap.txt">MSU1 interrupt bootstrapping code for Super Game Boy</a></li>
			<li><a href="msu1_snippets.txt">General snippets for MSU1 support</a></li>
		</ul>
	</body>
</html>
