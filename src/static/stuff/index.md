<!--:title="I'm Stuff"-->

## Random stuff

* [MDiocre - a static site generation script](mdiocre)
* [Time Machine](aesthetic)
* [MIDI Files](midi)
* [Splash Screen Repository](splashes)
* [Webdev Rant Containment](miniblog)

### Notes and References
* [Game Boy ASM style guide I think.](gbasm_style.html)
* [The MSU-1 Emulation Chip](msu1_notes)
* [.RMI File Format](rmi)
* [Microsoft GS Wavetable Synth Supported Events](msgs)

### Random stuff
* [Upgrade to Windows 10 NOW!](win10gret)
* [Upgrade to Windows 11 NOW!](win11gret)
* [Try HTML 8 today!](html8)
* [IPFS](ipfs.html)
* [Proof-of-concept encrypted page](test.html)
* [Splash RX no longer in development](srx.html)
* [70%](70)

### Tools
* [Font test](asciitest.html)
* [Animated chatlog generator](loresque)

### Shitposts
* [Manifesto](manifesto)
* [Where does Microsoft want to go today?](windows_ideator)
* [Clickshamed](clickshaming.html)
* [Break in case of internet fistfight](currentyear.html)
* [We are calling from Windows, you're system is infected](error111.html)
* [YTMND!](ytmn52.html)

<!--:mdiocre-template="../../built/template/subsections.html"-->
