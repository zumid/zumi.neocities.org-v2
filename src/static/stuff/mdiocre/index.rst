:mdiocre:`title   = "Home Page"`
:mdiocre:`section = "MDiocre v3"`
:mdiocre:`mdiocre-template = "../../built/template/mdiocre.html"`


.. container:: index-hero

   .. raw:: html
   
      <h1>Welcome to MDiocre</h1>

   A Python-based static site generator.
   
   Latest release: **v3.5.3** (2023-07-02)

   - `Get it from pip <https://pypi.org/project/mdiocre/>`_
   - `What's new <changelog.html>`_
   - `Source <https://github.com/ZoomTen/MDiocre>`_


Time to meet another static site generator you probably might not like. Basically it takes
these ingredients and spits out an entire site:

- An HTML template
- An article / page file formatted for MDiocre
- ...That's it, you literally just place them in the directory structure that
  best reflects your website.
  
Since it originated out of a personal-use tool, it's not exactly user-friendly
for most, but it does have the ability to just be plugged into any script, which
can be useful to hack out something that uses the variables of every page in
a website (for example, basic tagging and index pages)
  
It does have some pretty nifty features though:

- **Extensible** - MDiocre supports quite a range of markup, so long as
  it has comments or special markers for MDiocre to hook on. Currently,
  MDiocre comes bundled with support for the following markup:
  
  - HTML
  - Markdown (GitHub-flavored)
  - Zim Wikitext
  - ReStructuredText
  - Gemtext (experimental)

- **Dynamic** - Upon building, MDiocre's CLI generator copies your entire
  site directory structure as laid out on your source folder, processes
  the MDiocre-formatted files and copies the rest. That way, you can
  structure your pages as messy or as neatly as you like without adhering
  to some arbitrary standard that i.e. I impose.

- **Adaptable** - Besides just being a pretty interface, it's also a
  Python module. That way, you can create scripts to automatically generate
  RSS feeds, paginate, use tags... Then again, I'd need to make some sort
  of "standard library" for those soon, so you can just use them. There
  is documentation available though, if you want to make use of this.

