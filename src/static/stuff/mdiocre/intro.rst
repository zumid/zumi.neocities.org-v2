:mdiocre:`title   = "Introduction"`
:mdiocre:`section = "MDiocre v3"`
:mdiocre:`mdiocre-template = "../../built/template/mdiocre.html"`

Introduction to MDiocre
=======================

What's a MDiocre?
-----------------

**MDiocre** is a simple static website generator that takes specially-formatted files, as well as a template and inserts whatever the Markdown content is into the template. It does this by using specially-formatted comments which can set and get variables from and within the content, and to the template.

Why should I use it?
--------------------

Well, I don't know really. Give it a try and see if it's alright for ya. If you're looking for something sophisticated, you might be better off with `Jekyll <https://jekyllrb.com/>`_ or `Hugo <https://gohugo.io/>`_. Or anything from `here <https://www.staticgen.com>`_.

If you were looking for something more like this project except more user-friendly (and probably more solid too) I'd suggest `AutoSite <https://autosite.somnolescent.net/>`_.

What's the point?
-----------------

MDiocre was originally written in 2018 to generate a newer iteration of `my website <https://zumi.neocities.org>`_. It was initially several bash scripts that combined various tools with ``pandoc`` to create an easy workflow for me to blog, that can even be used on the go.

I then rewrote it in Python with the intent of having an easier, more "general" way of making static websites. I called this version 2.0. It turned out to be rather cumbersome with the configuration files and the concept of "submodules" and such. Nevertheless, this is what I used to maintain my website for some time. Another flaw with this iteration is that it assumes a ToC-like index in a particular format, and it could not be customized. The no-index pages had to be explicitly stated in the site configs.

In 2020, I rewrote it yet again to make it simpler to deal with. It is now free of the overkill ``pandoc`` dependency in any way, shape or form. It now depends on its namesake - Markdown - through ``python-markdown``. The downside is that each type of file has to implement its own parser, which is fine by me anyway. It doesn't really need any config files unlike 2.0 - it doesn't need the whole "process images and HTML separately" BS. Instead it processes MDiocre files and copies the rest.

The "submodule" paradigm has been replaced with that of **per-file template linking**. The downside is of course, more chores to be done when changing folder structures. But it can give you more control rather than having to fight the configs. And of course, it's also more modular and API-minded, such that you can create scripts that use MDiocre in addition to the generator itself. This modularity is also what made it possible for MDiocre to expand to other formats.

In summary, to date we now have 3 iterations of MDiocre:

* **version 1.0** : Hacked together Bash script: https://gitlab.com/zumid/zumisite-oldtools
* **version 2.0** : Interface-focused, monolithic Python script: https://github.com/ZoomTen/MDiocre/releases/tag/v2
* **version 3.x** : Extensible Python script (now)
