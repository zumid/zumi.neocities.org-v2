:mdiocre:`title   = "What to Know"`
:mdiocre:`section = "MDiocre v3"`
:mdiocre:`mdiocre-template = "../../built/template/mdiocre.html"`

What to Know
============

What's a "MDiocre file?"
------------------------
Literally any text format supported by MDiocre (currently Markdown, ReStructuredText, or Zim Wikitext) using specially-formatted comments for the respective format.

Specifically, it needs the following **variable** to be set to be considered one: ``mdiocre-template``.

Here's an example:

- HTML and Markdown: ``<!--:mdiocre-template = "your-file.html"-->``
- Zim Wikitext: ``[mdiocre: mdiocre-template = "your-file.html"]``
- ReStructuredText: ``:mdiocre:`mdiocre-template = "your-file.html"```

As you can see here, the comments follow each markup languages' format as much as possible. Exceptions apply to Zim, where comments aren't really possible, and ReStructuredText, where it is instead under a special ``mdiocre`` role.

What's a "variable?"
--------------------
**Variables** are basically comments formatted
in a particular way.

For HTML and Markdown, the first character after the comment markup should
be a colon (:), both for setting and getting variables.

There not many restrictions for variable names. It can have funky letters,
spaces, etc. EXCEPT for a `=`, since that is used for determining where the value starts.

Variables are defined simply in the form of `identifier = value`, where the
`identifier` is the variable's name, and the `value` can be a concatenation or
a simple value define.

A **simple value define** looks like this:

.. code-block::

  <!--: String = "Hello!" -->

This sets the variable named ``String`` to contain the word ``Hello!``. This works only if both quotation marks match. There is also another way you can set a variable: **concatenation**...

.. code-block::

   <!--: String2 = String, String -->

Basically it puts two variables together. For this one, ``String2`` will have the value of ``Hello!Hello!``. To separate the two, concatenate a space after the first variable:

.. code-block::

   <!--: String2 = String, " ", String -->

Here, ``String2`` is ``Hello! Hello!``.

To **get** a variable - that is to put its value in the current position in the file - it is as simple as simply saying the name of the variable (and nothing else)
within the comment.

Any instance of this:

.. code-block::

   <!--: String -->

Would be replace with whatever is stored in ``String``, that is simply the word, ``Hello!``.

What's a "template?"
--------------------

Templates usually are simple HTML pages, but with special HTML comments which are to be replaced by the MDiocre conversion process.

One notable aspect of a template is the comment markup even belongs in
parameters, so the template files may not necessarily be well-formed HTML.

How do I maek page?
-------------------
The main operation of command line version of MDiocre needs 2 arguments, namely
the source directory and the destination (build) directory.

The app will copy each file in the source directory recursively and copy them
to the respective folders in the build directory, making them when necessary.
In the case of ``.md`` files, it will parse them. If it finds a
``mdiocre-template`` definition linking to a valid file, it will convert the
file into a usable HTML page.

Consider a folder named `pages` which are full of pages and misc. files that
are targeted to be copied and published, and that the build folder be named
`publish`. MDiocre would be invoked as:

.. code-block::

   mdiocre pages publish

When run, a detailed output of what MDiocre is doing will pop up quickly in
the command line window. If this is undesired, the CLI also offers a "quiet"
option, as `--quiet`:

.. code-block::

   mdiocre --quiet pages publish
