:mdiocre:`title   = "MDiocre Changelog"`
:mdiocre:`section = "MDiocre v3"`
:mdiocre:`mdiocre-template = "../../built/template/mdiocre.html"`

What's new?
===========

3.5.3 (2023-07-02)
------------------
* Hopefully fixed oversight with rebuilding the file name when
  compiling Typescript files...

3.5.2 (yanked) (2023-07-02)
---------------------------
* TypeScript files should've been copied over as-is to the build
  directory. Apparently that didn't happen -- that's fixed now.

3.5.1 (2022-10-18)
------------------
* Fixed an oversight regarding Zim Wikitext: ``*.zimtxt`` was accidentally
  left out of the supported file extensions list.

3.5 (2022-06-29)
----------------
* Rough support for relative links in Gemtext parser.

* Supports TypeScript compilation of individual files through ``tsc``.

  Known limitations:
    * You cannot use modules (import, export, require, etc.)
    * However, you can use references to "include" type definitions
      like so: ``/// <reference types="../MyTypes" />``
      (without the file extensions)

* Redirecting logs are now made a little better.

* More changes to how the Wizard reports stuff.

* A basic plugin system is now supported. As a result:
  
  * Passing a parser name (string) into ``switch_parser()`` is only
    limited to internal parsers, and as such this way of switching
    parsers is considered deprecated.
  * ``switch_parser()`` should be used with classes (not instances of
    classes) derived from BaseParser.
  * Every parser has a ``FILETYPES`` array that contains which file extensions
    will be supported by the parser.
  * The wizard will detect internal parsers *as well as* parsers from modules
    that start with ``mdiocre_``.
  * Whichever ``mdiocre_`` module that implements a parser should have it
    under a folder named `parsers`, e.g. for a ``txt`` parser for plain
    text files, it should be under ``mdiocre_txt.parsers.txt``, and its class
    should be ``TxtParser``.

3.4 (2022-06-27)
----------------
* Added a `Gemtext <https://gemini.circumlunar.space/docs/gemtext.gmi>`_ parser.
  However, there's a couple of implementation differences from other parsers, namely:
  
  * Lines starting with ``=>`` used to indicate links now act like lists of links, instead
    of paragraphs or whatever.
  * Links can be embedded inside paragraphs and lists by surrounding it with parentheses.
    For example: ``Regular text (=> http://127.0.0.1 Link text)``
	
	However, if you wish to adhere to the specification, you can simply... not do that.

* If "an error occured" in the MDiocre wizard, it will now describe briefly what went wrong.

3.3 (2021-08-24)
----------------
* First PyPI release!

* Added error-handling to the wizard, so it wouldn't just willy-nilly convert
  rst files as markdown :P

3.3.dev2 (2020-04-30)
---------------------
* Use GitHub Flavored Markdown (gfm) by default

3.3.dev1 (2020-04-22)
---------------------
* Fixes a bug I accidentally re-introduced where it isn't playing well with
  quotation marks. :P

3.3.dev0 (2020-04-21)
---------------------
* Math expressions are removed, as there is very little use for them.

* You can define custom function calls using Python, with the
  ``using`` keyword, which will **execute raw Python from a file**,
  so use it at your own discretion. For example, a script above the
  website source directory, named "echo.py", contain the following
  definitions:
  
  .. code-block:: python
  
    def echo(my_string):
        return f'echo! {my_string}'
  
  Which is then called by invoking: ``<!--: using: ../echo.py -->``
  
  And then can be used by enclosing the following in parentheses - 
  the function name as well as the arguments: ``<!--: my-var = (echo "hello") -->``

* The much-needed ``include`` keyword arrives: ``<!--: include: includes/header.html -->``.

  This will include a file relative to the source directory as if it was
  pasted right into the currently processed file, allowing for some more
  dynamic web element manipulation.

3.2 (2020-09-15)
----------------
User
~~~~
* Zim Wikitext support now added! However, because MDiocre at the moment relies
  on file extensions, the file name has to end in a `.zimtxt`. Additionally,
  MDiocre commands will be in the form of ``[mdiocre: <command>]`` - you simply
  type it in when writing a Zim document. The number of spaces around the command
  doesn't really matter as long as they are on one line.

API
~~~
* ``mdiocre.core.MDiocre.sub_func`` has been removed.

3.1 (2020-09-13)
----------------
User
~~~~
* Introducing a simple Tk GUI for MDiocre (`mdiocre_gui.py`). Its features
  currently match that of the CLI version (`mdiocre.py`), but individual file
  conversion and string conversion is planned.

* Added sample scripts that use the MDiocre API
    * RSS feed generator (`samples/make_feed.py`)
    * Index page generator, with pagination (`samples/make_index.py`)
    * Tags page generator (`samples/make_tags.py`)

API
~~~
* The comment-parsing system has been changed in `core`. Instead of going
  through the string directly through `re`, it goes through a parser, which
  really just implements the same functions. This opens up the possibility
  for other formats to be added as an input to MDiocre. ReStructuredText
  support can be added due to this.

* The `Wizard` can now take a `callback` function. The callback function is
  run after each file is processed, and passes a dict containing the original
  file name, the converted file name, as well as the root directory.

* The `MDiocre` class can now take a `parser` or `parser_name` option.
  The `parser` option must be set to a class inheriting `BaseParser`, or
  you can set the `parser_name`. Built-in ones currently include:
  `markdown`, `html`, `rst`. If both `parser` and `parser_name` are
  defined, `parser` takes precedence.

* ``mdiocre.core.MDiocre.sub_func`` has been moved to
  ``mdiocre.parsers.sub_func``. The former will be removed in MDiocre
  3.2.

3.0 (2020-08-31)
----------------

All-new rewrite of MDiocre, with the aim to "modularize" it and keep it easy-to-use.
