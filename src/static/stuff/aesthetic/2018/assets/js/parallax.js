// Sloppily modified to add extra classes. Zumi 2019.

(function() {

	function check() {
		var current_scroll = $(this).scrollTop();

	    oVal = ($(window).scrollTop() / 4);
	    oValt = ($(window).scrollTop() / 1.75) - ($("#contains").position().top / 1.75);
	    oValo = ($(window).scrollTop() / 1.75) - ($("#contains2").position().top / 1.75);
		$(".parallax") .css({
        	'background-position': '50% calc(50% + ' +  oVal + 'px)'
	    });
	    $(".parallax-two") .css({
        	'background-position': '50% calc(50% + ' +  oValo + 'px)'
	    });
	    $("#parallax-self") .css({
        	'top': 'calc(' +  oValt + 'px)',
        	'position': 'absolute',
        	'right': '0',
        	'min-width':'100%',
        	'z-index': '-100',
        	'display':'initial'
	    });
	}

    $(window).on('scroll', check);

})();
     
