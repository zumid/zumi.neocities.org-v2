<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Moving back lol"-->

<!--:longdate="2021-03-30T00:39:01+07:00"-->
<!--:lastedited="2021-04-08T00:00:00+07:00"-->
<!--:category = "site-updates"-->

Well, it's been a fun ride. Though not really, since I don't even update it that much.

Noooo I don't mean *here* of course, I meant my [short-lived](owning_a_domain.html) [space](hello_domain_site.html) away from neocities. A space that, among other things, provides some HTTP-only stuff so you can look at it with real Windows 95 or something. I can't be arsed to renew domain and host services because it might not be all that useful, expect scalpers to take it or remain a parked space forever.

There were only a few things anyway that I hosted there. A file dump, a mirror of my 90s page, a little wiki that ran on PMWiki, and my other blog that ran on [Pico CMS](https://picocms.org/). That wiki my friends never used anyway, since there wasn't much incentive to contribute. I didn't really do much of anything else on it until the hosting inevitably expired though.

**I'm trying to port the posts from my other blog, so things may look wonky this time around!**

### Manual SSL

The hosting service I used, Hostinger, offers automatic SSL cert stuff. Didn't feel like adding it and using more of my already-tight budget, so I opted to manually doing the SSL stuff. Oftentimes I would be late to renew my certificate, so my website would've ended up being locked / "unsafe!" on many modern browsers, until I renewed it.

I used to use an online service to do the renewal stuff, but that has since shifted to a sign-up service, which I didn't like. So I had to make do with `certbot`. In short, here's the steps I had to take:

#### Create cert

`sudo certbot certonly --manual`

Enter e-mail address, accept mandatory IP logging, place files as per instructions.

#### Register certificate and chain

Copy paste the contents of (or upload)

```
/etc/letsencrypt/live/*/fullchain.pem
```

and

```
/etc/letsencrypt/live/*/privkey.pem
```

to its respective slots in the web hosting file manager.

Do the above for all domains and subdomains to be certified.

#### Renew cert

`sudo certbot renew`, then reregister.

#### Considerations

I also noted some security considerations based on a recent happening:

- Do not register all of owned domains in one go unless they are related to one another (like subdomains and websites that you want to be explicitly linked) [the resulting cert info will show which domains the cert is for, wouldn't want dirty laundry in there]
    
- Delete the contents of the `acme-challenge` folder immediately after certificate renewal

### FTP

Sometimes manipulating files through FTP can be slow. I used FileZilla to transport my files, later using Thunar's network browsing feature. Either way, they're both slow.

When I uploaded the wiki, it had to go through a metric ton of files. I'm conviced that I uploaded so many files at one time that it probably promptly IP-blocked me for a while. And no, investing in a VPS wasn't really in my budget either.

Perhaps I would be better served with uploading just one zip and then have some dummy script explode it into functional pieces?

### Other stuff
I was limited to two subdomains, which was actually perfect for my needs. One is for `spaces` which is supposed to be a thing for me or others to host sites on, in the end I just hosted my blog there and some file format documentation drafts. The other one is the aforementioned `wiki`, which I even had "wiki farms" set up (that's what they subwikis apparently).

I also had some other stuff on there, like a tutorial on making a simple site for character info. Of course it's based on PHP + MySQL, and there was a demo that went along with it. I might repost it here some time.

Also [this](https://gitlab.com/zumid/owoifier-api) lmao, my pretty crappy attempt at a "RESTful" API for some joke.

<hr>

I planned to continue this, but uh.. turns out I really don't know what else to put here.

Man, reading that [initial blog](owning_a_domain.html) again made me feel kind of dumb lol. It was good to get away with owning a domain and paying for web hosting for once, but once I learned how to `/etc/hosts` and "host" a bunch of services all on localhost all willy-nilly and string it all together, it felt pretty limiting. But I guess I got what I paid for, lmao.

Because now I don't think it's just websites that I want to host, I wanna host an IRC server, a Gemini server, self-hosted cloud stuff, random dumb services, etc. etc. Even I don't really like Apache all that much and would rather use Nginx (Hostinger uses LiteSpeed, which I think is a derivative of Apache?)

In which case I really should invest in a VPS some time. My friend offered one up for maybe like 1 or 2 weeks, but that short period of time where I had it running was pretty glorious.

I did find Ngrok to be a fine service to be securely hosting all of my shit, when I hosted my IRC server the origin whois all pretty much said `localhost` instead of my actual IP address, though I'm not sure if it's Ngrok doing it or the server software (`ngircd`)

Midterms are coming next week and the assignments are just out the ass and whatever time I have left to clear my mind I can't use it for other stuff, I'm just pretty drained now lol.
