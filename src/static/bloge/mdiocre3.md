<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "MDiocre 3.0 (and Site Updates)" -->

<!--:longdate    = "2020-08-31T14:31:14+07:00" -->
<!--:lastedited  = "2020-08-31T14:31:14+07:00" -->
<!--:category    = "tools" -->

### MDiocre 3.0

After like a year or so, I realized how... oof my static site generator is. Two config files, a system that smells, and having to explicitly specify which pages should not have fixed ToC-like index pages.

Soooo I completely rewrote it. The result? No more config files, no more additional software dependencies, and cleaner directories.

By the time this post is up, I would have updated [the GitHub repo](http://github.com/zoomten/mdiocre) and published [*some* documentation](../mdiocre3doc/index.html) for it...

### Site Updates

Because I completely redesigned MDiocre, I also had to rip apart my website's source, and upon looking at it, I knew I had to hand pick everything and move stuff carefully, because my website's source directory was fractured.

How fractured? There's an `extras` folder which contained the Neocities `stuff` folder and `splashes` repo, `img` was another folder that had all the site images... It didn't reflect what my actual website structure is on Neocities, and is overall hard to navigate.

With the new system, the source will now almost directly correspond to the build. The almost here is even less of an "almost" than the previous iteration, since I still have to generate the feeds and blog index automatically... Hopefully after this whole refactoring thing, I have an easier time dealing with this lol.

The site now purely runs on Makefiles and Python scripts without Bash scripts hodge-podged in. I also took this opportunity to also update the blog index page a little bit. Even if the automated summary is a bit smashed... 😅

Seeing how long this index page is, I may need to implement pagination sometime. Maybe later.

