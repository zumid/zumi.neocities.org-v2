<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="New look!"-->

<!--:longdate="2018-10-20T23:32:00+07:00"-->
<!--:lastedited="2023-03-26T21:40:36+07:00"-->
<!--:category = "site-updates"-->

**[View this post in its original context here](../stuff/aesthetic/zumi_2018/new_look)**

So I decided to give my website some sort of overhaul. It probably sucks, but I'm feeling sort of nostalgic, kinda. You know how web pages actually *were* pages? That's the kind of thing I'm aiming for here.

It also gives me an excuse to fiddle around with static site generation using scripts and all that skidaddle-skidoodle. Templates, converters, and all that. I'm still looking for an efficient workflow though, figuring out how the directories are all structured and maybe naming files automatically?

The whole layout could have been easier done with tables, but I don't want to look ancient or something despite how I could achieve a similar result with the difference being speed, familiarity, and just the fact that CSS is pretty much of a clusterfuck as all "languages" are here in tech world no matter if it's a programming, scripting or markup language. (Oh god, XML. Eugh.)

I kind of doubt that I'll be updating this, given my limited time and... come to think of that, I'll probably transfer all the scripts over to my phone. Now all I need to do is to memorize the cURL Neocities POST request by heart and maybe get used to Markdown a little bit more. (how do tables work here?)

I'm considering a guestbook but eh, I'll think about it.

Also despite my best efforts, the sidebar *still* doesn't work on Opera Mini. Well, the website looked like crap on it anyway, I gotta fix that there.
