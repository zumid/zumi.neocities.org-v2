<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Happy World Emoji Day! (or what I think should be it) "-->

<!--:longdate="2019-10-12T06:22:01+07:00"-->
<!--:lastedited="2019-10-12T06:22:01+07:00"-->
<!--:category = "rants"-->

### TL;DR
World Emoji Day should have been October 11. Oh, also blame Unicode for all your emoji representation problems.

### Long answer

<figure class="fig-right">
<img src="img/world-emoji-day.jpg" alt="World Emoji Day">
<figcaption>We're having a party.</figcaption>
</figure>

Ahh, emojis. Those tiny pictures 🙂 that we use today, displacing the function of emoticons `:)` of yonder days. Some love it, some loathe it. Either way, we're all used to it, I use it all the time... going like 💩💩💤👌👌👌😂😂😂😂😂😂😂😂😂😂😂😂😂 wh

Anyway...

Emojis are so significant today that there needs to be a day for it. ...and a movie, for some reason. But there's one detail that annoys *me* in particular.

World Emoji Day is officially commemmorated on *July 17*<sup>8</sup>.

What's the problem with it? It only makes sense, right? That's the date that shows up when I put a calendar on iMessages?

Well, people only think emojis are the stuff on iPhones. When it comes to emojis, people act like Apple is the sole arbiter, making it as if Apple is in control of emojis. I mean, that's sort of because of Apple making phone tech <strike>accessible</strike> marketable to everyone even if it costs a premium.

In the mainstream, Apple is hailed as the innovators - although I doubt the accuracy, I can at least understand why. It's really impressive how Apple was able to do a 180°, turning itself from a dying company to a tech giant way back in the 2000s. Steve Jobs really knew how to make things marketable, it's just a matter of "marketing and perceived quality trumps technical matters".

Considering this, it might be why everyone thinks Apple decides which emoji gets to be included. After all, their designs are what pops up to mind when people think of emoji, me included. They took advantage of this status<sup>3,</sup><sup>4</sup> because... well, why wouldn't they?

Oh, not to mention the iPhone X (and 11), the newest in the line of iPhones that Apple has invented. Ten years' worth of building on iPhone et al. tech, advancing in both hardware and software fronts, has given us [the one thousand dollar emoji machine](https://www.youtube.com/watch?v=sjlHnJvXdQs), as well as the [fidget spinner camera](https://www.youtube.com/watch?v=cnnK6CIzPzU). Nice.

If we look back on its' history, what we call "emoji" (special characters used in place of traditional emotions) could be found as early as 1997, albeit only in Japan.

From the Wikipedia article,

> The earliest known mobile phone in Japan to include a set of emoji was released by J-Phone on November 1, 1997. The set of 90 emoji included many that would later be added to the Unicode Standard, such as Pile of Poo, but as the phone was very expensive they were not widely used at the time.<sup>5</sup>

And not only was it Japan-only, it was pretty restricted and non-universal. Every operator had their own emoji standards and variants - so that private-use area in Unicode (at the time) that was being used for emoji was free-to-nab. It was incompatible with one another.<sup>6,</sup><sup>7</sup>

Enter **Google and Apple**, who saw the emoji trend in Japan and wanted them to be accessible to everyone. Of course, they could have reimplemented emojis themselves. But that would mean restriction and incompatibility. What they instead opted to do is try to put them in a standard that ensures emojis would be the same character everywhere. **Unicode**.

Unicode is an encoding system already used around the world, facilitating digital transmission of Latin and non-Latin characters. In other words, thanks to this system, people from around the world can type in their native script and it will still display everywhere else without complicated configuration. No longer would we figure out how to set our browsers to Shift-JIS whenever we want to read Japanese text, or Big5 if we want to read Chinese. No longer would people make their own encoding system just to have their native language properly represented. This standard is maintained by the **Unicode Consortium**, a non-profit organization.

Quote again from Wikipedia:

> The additions, originally requested by Google (Kat Momoi, Mark Davis, and Markus Scherer wrote the first draft for consideration by the Unicode Technical Committee in August 2007) and Apple Inc. (whose Yasuo Kida and Peter Edberg joined the first official UTC proposal for 607 characters as coauthors in January 2009), went through a long series of commenting by members of the Unicode Consortium and national standardization bodies of various countries participating in ISO/IEC JTC1/SC2/WG2, especially the United States, Germany, Ireland (led by Michael Everson), and Japan; various new characters (especially symbols for maps and European signs) were added during the consensus-building process.

After 3 years of long discussion in the Unicode Consortium, emojis were finally made universal with the release of [Unicode version 6.0](https://www.unicode.org/versions/Unicode6.0.0/) in 2010. They added a whole ton of emoji (716) from the mobile operators into their own Unicode blocks and retroactively classifying icons already defined in prior versions of Unicode (144) as "emoji".

With emoji being standardized, companies now have to design their own designs of the defined emoji. Even if the emoji characters themselves are the same everywhere, what they look like is free to choose as long as they fit the description. That means emoji sent from an iPhone look different on an Android, and different on Windows 10. *However*, they all convey the same expression that was intended. It just so happens that Apple gets the spotlight.

To put it simply... Apple, Google, Microsoft and others make their emoji *designs, following* the standard. Unicode make the *descriptions, defining* the standard. See also [here](https://www.npr.org/sections/alltechconsidered/2015/10/25/451642332/who-decides-which-emojis-get-the-thumbs-up).

So what does July 17 initially signify? It was the debut date of iCal<sup>1,</sup><sup>2</sup>, the calendar application for Mac OS X (macOS). It was within Apple's tradition of putting inside references to important dates relating to the company, like how 9:41 appears on every iPhone promotional picture, referring to the time of day the iPhone was first introduced.

Basing World Emoji Day on an Apple emoji, to me, represents how more effective associating an idea is to a well-known brand than it is to associate it with the greats who aren't as well known. It's because of this that other companies start bandwagoning, changing *their* dates on *their* calendar emoji to Apple's July 17. Quite worrisome in my opinion.

Should we set it to November 1? That's just "Emoji Day"; it represents the invention of emoji as we know it. As mentioned before, it was still limited on invention.

I propose that we should set it to **October 11**. This date signifies the finalization and release of Unicode 6.0 on October 11, 2010. Unicode 6.0, as stated above, marks the first time Unicode acknowledged emoji, and thus also marking its widespread access.

However, if we insist on marking dates based on a popular emoji design rather than significance, just set it to January 1 or something. I don't know.

### Edit (2021-04-10)

The biggest irony that BTFO's this entire article is that Apple is in the consortium as a full voting member, so their influence is undeniable. And they won't hesitate to change their **example glyphs** to reflect how everyone else is actually using it. And since everyone else follows Apple, you know what that means. \*sigh\*.

When it comes to emoji, Unicode is kind of like a dictionary. In the sense that, it's both as prescriptive as it is descriptive. Prescriptive as in, here, here's how this codepoint is meant to be used. Descriptive as in, oh, I see everyone is using it this way. Time to change my references then, I must be outdated.

Well, corpos gotta corpo.

<figure>
<a class="image-link" href="https://twitter.com/FakeUnicode/status/989660568565960705"><img src="img/even_unicode.png" alt="Imp"></a>
<figcaption>👿</figcaption>
</figure>

### Sources
* <sup>1</sup> [AppleInsider - Happy birthday to Apple's iCal, which immortalizes its July 17 release date in an icon and emoji](https://iphone.appleinsider.com/articles/18/07/17/happy-birthday-to-apples-ical-which-immortalizes-its-july-17-release-date-in-an-icon-and-emoji)
* <sup>2</sup> [Apple - Apple Introduces iCal](https://www.apple.com/newsroom/2002/07/17Apple-Introduces-iCal/)
* <sup>3</sup> [AppleInsider - Apple Celebrates World Emoji Day with Tease of New Options in iOS 11 High Sierra](https://appleinsider.com/articles/17/07/17/apple-celebrates-world-emoji-day-with-tease-of-new-options-in-ios-11-high-sierra)
* <sup>4</sup> [MSN - Apple emoji will soon include people with curly hair, white hair and superpowers](https://www.msn.com/en-us/money/other/apple-emoji-will-soon-include-people-with-curly-hair-white-hair-and-superpowers/ar-AAAaj39)
* <sup>5</sup> [Slate - Emojis Are No Longer Cool in Japan](https://slate.com/technology/2015/12/emojis-are-no-longer-cool-in-japan.html)
* <sup>6</sup> [WebDesignerDepot - The Surprising History of Emojis](https://www.webdesignerdepot.com/2016/10/the-surprising-history-of-emojis/)
* <sup>7</sup> [The Verge - How Emoji Conquered the World](https://www.theverge.com/2013/3/4/3966140/how-emoji-conquered-the-world)
* <sup>8</sup> [World Emoji Day](https://worldemojiday.com/)
