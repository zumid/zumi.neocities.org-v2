<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "More on my crappy blog system" -->

<!--:longdate    = "2019-02-24T00:00:00+00:00" -->
<!--:lastedited  = "2019-02-24T00:00:00+00:00" -->
<!--:category    = "site-updates" -->

This is sort of unexpected - my blogging system that I thought was safe enough - turns out it wasn't!

As I said, I plan to release this system - but really, it's not really optimized yet for general use. For one, the "template" was made up of individual parts, one for the header, one for the metadata, and another for the footer. If one were to change it, it would be a serious pain.

So, I ended up completely reworking the entire system with new features. The code went from a single, simple script file to an entire module, and from a simple generator setup it *ballooned* into a hot mess. Looks like I'm overwhelming myself for being a "simpleton".

Yeah, not only can this new system build blogs, it generates entire websites - expect some sort of changes soon.

This new system incorporates variables, and while it is hell enough to implement (I can't token properly) it can really be useful if you want to design a bunch of templates.

I'm naming this system "MDiocre", because one, I'm not confident enough of this (even if it works), and two, it's main focus is Markdown.

I'm already halfway through writing the docs on this thing and there will be a chain of exams next week so maybe I'm not ready... at least not yet.
