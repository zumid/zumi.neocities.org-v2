<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Update... again"-->

<!--:longdate="2019-05-20T03:52:35+07:00"-->
<!--:lastedited="2019-05-20T03:52:35+07:00"-->
<!--:category = "site-updates"-->

So I finally got up and hauled ass to Git. Why the hell didn't I think of this before?? All this time I was clumsily storing project files on every computer and phone, when I could have just done a `git pull` on my website files whenever I need to update it!

As a result of my oopsie, I end up updating the entire blog every time I write a new entry. That's the second excuse I don't upload much here. The first one? School wh

I still have no script for automatically updating my website *in Neocities*. I could have written one, but I don't want to make its' system think I'm deliberately spamming. I could try to make a script that downloads the file in question and then compares it, but then again, it spams API for requests. I wish I had the money to donate so I get a little less worried since there'd be more bandwidth allowance... or whatever. idk. (On second thought: I'd just need to update these three: `index.html`, the blog posts, and RSS, wink wink)

### What's new?

<ul>
<li>
<b>I have a Twitter now</b>: <a href="https://twitter.com/zzdaxuya">@zzdaxuya</a>
<p>I'm not even sure if I'll ever use this. Might get eaten up alive... or maybe not.</p>
</li>
<li>
<b>Updated my CSS.</b>
<p>There was a bug which made everything look really weird and spaced out. Turns out it was my lazily applied styling on <code>main *</code> instead of taking the time to specify every element. As a result, each individual markup under <code>&lt;p&gt;</code> has a margin of 5px. That's fixed now.</p>
<p>Added new classes which support footnotes and blockquotes, as well as better handling of images. I'm gonna need them later.</p>
</li>
<li>
<b>Reading time.</b>
<p>Boy, I hope that little quip doesn't show up in Reader View, because that's gonna be redundant and conflicting. Anyway, I've implemented a really basic read time calculator from <a href="https://github.com/pritishvaidya/read-time-estimate">this kind fellow</a>. It's very inaccurate, but hey, I don't want heavyweight champion libraries weighing down my real site. Oh, and good news - this is pretty much the only thing in the main site that requires Javascript so hopefully it won't break anything.</p>
</li>
<li>
<b>RSS feed's coming.</b>
<p>So you can have my pointless crap on RSS too! Wow, I can't believe it's not already dead by the hands of big social media companies and their walled gardens! (like Twitter, at least there's still TwitRSS if you want some freedom with your shit. btw Reddit still has RSS along with their old, very customizable site design, so props to them)</p>
<p>Oh yeah, and props to <a href="https://pypi.org/project/text-summarizer/">this</a> at least I have a description I can put into my RSS feed without sacrificing much time writing it. Even if it makes little to no sense, because nothing I write ever is wh</p>
<p>(edit: it's now the entire page. Should I do that though? I like reading full articles from NewsFox and Feeder...)</p>
</li>
</ul>

I... think that's about it? Looking at my git logs, anyway.

### One more thing

[MDiocre's readme](https://github.com/ZoomTen/MDiocre/blob/master/README.md) is now up. Very minimal, untested but hopefully will give you some sort of cleaner idea on how to use the thing.
