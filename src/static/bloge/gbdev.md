<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "Exploring a few alternative Game Boy assemblers" -->
<!--:description = "My experiences with GB dev, my worries and my choices for running to once the storm hits. Also, possible Dunning-Krueger moment." -->
<!--:longdate    = "2022-12-07T11:47:27+07:00" -->
<!--:lastedited  = "2023-04-22T00:06:11+07:00" -->
<!--:category    = "musings" -->

<!--### Nostalgia

I've never actually had a Game Boy, or any of its variants. I don't know its outside specifics, or the feeling of holding one. I *did* however had a Game Boy Advance SP at one point, and played a bit of Pok&eacute;mon Ruby on there along with a bunch of NES bootlegs pumped in through PocketNES. But even then, never, during that time, an actual GB or GBC cartridge.

My only experience with the Game Boy has thus far been emulation. Recalling my first days, I think I "got" Super Mario Land, Snoopy's Magic Show, some other games, and then the first 2 Pok&eacute;mon gens. I was trying programming in QBasic at this point, an extension of "playing on the computer". And naturally, these two combined make an interest in ROM hacking.

ROM hacking at the time was still more of editing binaries directly. Resources include: hex editors, hex to ASCII table translation files ("table files" for short), tile graphics editors, game-specific level and map editors. You also had documents explaining what someone else found while reverse-engineering games, location maps, how to change X Y and Z, etc.

I remember when I thought that "Red Version" on the Pok&eacute;mon Red title screen was just a string I can change. Imagine my shock reading `"Red Version" not found`. I didn't know any better, and certainly not the fact that it's just a 2-color graphic hidden somewhere in the title screen code, found if you search in a tile editor with the color set to "1bpp".

Soon, changing bytes in a binary turned into an interest in assembly. A folder full of ROMs slightly changed from one another, usually in the title screen. At the time, the Pok&eacute;mon disassemblies were far from complete, still riddled with `INCBIN "baserom.gbc"`. ASM hacks, especially the [full color hack](https://www.youtube.com/watch?v=d35D8n6Z7DA), seemed like wizardry back when I was still dealing with mostly changing some text using table files and experimenting with different ways to put custom music.

<figure class="fig-right">
	<img src="img/table_file_music.png" alt="Left: a table file made for the sole purpose of typing in sorta-readable music commands. Right: a text file with music commands corresponding to the aforementioned table file.">
	<figcaption>
		Yes, I actually tried this.
	</figcaption>
</figure>

(Seriously, I've had fun making stuff in FamiTracker and then transcribing it into a hex editor. Either that or it's managing to come up with some form of syntax or MML "compiled" using [crumb](https://www.romhacking.net/utilities/650/).)

My first ASM tools were [GameBoy Assembler Plus](https://www.romhacking.net/utilities/873/) and [Gameboy Assembly Editor](https://www.romhacking.net/utilities/282/). These were good for making small edits to routines and changing a line of code or two. They can also disassemble a bunch of code, but GBAP has a slight edge in that it can try disassembling something and then being able to save the results to a file. I played with them for quite a bit.

But then, for larger stuff (where you also need to define data and such), I needed the big guns in the form of RGBDS. At this time, the version that was accessible to me was [Otaku no Zoku's](http://otakunozoku.com/rednex-gameboy-development-system/) version. It's the same one that was used for the Pok&eacute;mon disassemblies at the time. But that's a toolkit used for *making games*, yet I want to use them for binary hacks.

At the time, I figured out the linkfile format only for full ROMs. But what did I do upon writing that custom start menu routine or full screen text box with some text in it? 12-year old me literally inspected the OBJ files that RGBASM outputs and inserted it into the game. Labels get compiled into some temporary placeholder for the linker to put together, so my file had a `StartPoint equ $7c49` at the start, and instead of `ld hl, MyData` I did `ld hl, MyData + StartPoint`. It works because that would force them into numbers (I think.)

Over time, I got pretty good with it, helped immensely by the completion of the Pok&eacute;mon disassemblies. They were in a state where they can be fiddled around freely. A hack I was doing in binary form up until that point, I began transferring asset by asset into the new codebase. I had also kept the ASM files I'd made for the binary version and was able to extend it.

Eventually, I did other hacks, mostly adding in small features and whatnot. And then I started writing a bunch of little "demos", sound engines, video games... That growth was helped by BGB's excellent debugger. Really easy to use compared to some other debuggers. I do believe that emulator alone is why I got pretty good at Game Boy assembly specifically, and may have contributed to what's to come&hellip;

### The CoolNow&trade;ening

Yeah&hellip; and then things happened. The Game Boy suddenly became a hot retro platform to develop for, toolchains are pumped out for this thing, tech media begins to promote it, and a new community has made itself the large, diverse and friendly face of Game Boy dev.

The change was made clear &mdash; when I considered that, when I learned this stuff for the first time, my resources were [Devrs](https://devrs.com/gb/), the [ASMSchool tutorial](http://gameboy.mongenel.com/asmschool.html), [Wichita State University's course on Game Boy assembly](https://web.archive.org/web/20080407005134if_/http://cratel.wichita.edu/cratel/ECE238Spr08), and [Pan Docs](https://problemkaputt.de/pandocs.htm) in plain HTML. But now, there's a new ["Pan Docs"](https://gbdev.io/pandocs/) &mdash; which also adds more detailed information about the Game Boy as well as its quirks and gotchas, which people can also contribute to (through GitHub). I do also find the modernized RGBDS much more accessible &mdash; and there's loads of genuinely useful things in there now, including native character maps, `rgbgfx` for png-to-2bpp conversion, easy linker file syntax, among other QoL improvements.

[GBDK](https://gbdk.sourceforge.net/), the C compiler for Game Boy, is also seeing major updates and modernizations in the form of [GBDK 2020](https://github.com/gbdk-2020/gbdk-2020), as well as a tool called [GB Studio](https://www.gbstudio.dev/) &mdash; for people who just like the Game Boy and want to chip in with less difficulty (though makes most of its output same-y, imo)

It seems Game Boy development is growing exponentially, and things are looking very good for it. Maybe... a little **too** good for its own good. There seems to be a law that once something starts getting popular, it begins attracting misunderstandings, and eventually drama or a major conflict within (or without) its community. Sooner or later, something's gonna break and a fallout ensues. Some people call it a [degradation of a hobby](https://i.kym-cdn.com/photos/images/original/002/169/949/6e0.jpg), some call it a bubble, a tragedy of the commons, whatever. I call it "getting Cool Now&trade;"

> <b>Cool Now&trade;</b> (adj.) - When a niche that you partake in sees so much popularity and polish, that &mdash; despite increased accessibility for newbies and oldbies alike &mdash; leaves it open for shenanigans that will soon be associated with your niche, and possibly you by association.

It's not that I don't *want* a community or to remain unrecognized, it's just that I think that it may be accelerating too fast for me. And in this sense, I may be seeing that GB dev is heading for its peak. Although it's the best it's ever been since... 1998, maybe, but we're in a completely different generation that tends towards different attitudes. I can't help it &mdash; it's not like this is avoidable, nor shouldn't you be happy to be in a *community*, but I'm saying that I'm just wary.

I sum up all of this whenever I say: **GB dev is Cool Now&trade;**.

As much as I love RGBDS, I sense some "weird" signs here too. The same software culture being applied here, rapid releases which deprecate a lot of stuff really quickly. And since some of the maintainers are the same ones maintaining the Pok&eacute;mon disassemblies, the disassemblies inexplicably become tied to the RGBDS version.

The thousands of ROM hacks based off these disassemblies can't all possibly update to the latest sensibilities on the spot. Some of these may be unable to build, and will need a "version to compile with" indicator somewhere (if not already enforced by the RGBDS version check). This is actually the main reason I've developed [rgbenv](https://github.com/ZoomTen/rgbenv), a shell script in the manner of `pyenv` that compiles some RGBDS version for you. It currently assumes a C-styled workflow, so this too shall pass should a Rust rewrite consume RGBDS, as it would be the case for Cool Now&trade; software made in `{d:CURRENT_YEAR}`.

Modern RGBDS spoils me, and the effort to [standardize](https://rgbds.gbdev.io/sym/) things related to GB dev is commendable. But I can't imagine if everything came to a head and the community is left in absolute shambles, after what I call peaking Cool Now&trade;, and then splintering off. If this were to happen, where do I go?

(If it appears like I'm viewing RGBDS somewhat symbolically of the "bigger" GB dev community and not as a tool in its own right, I suppose I can grant you that observation. I don't know, man. Should you also view Rust as a tool in its own right? In my opinion, yes &mdash; but that may be hard to do given its community's position&hellip;)
-->

RGBDS is considered the *premier* assembler for developing games for the Game Boy, indeed it's seen popular usage and has grown into a powerful suite over the many years it has existed. It's pretty accessible, and there's loads of genuinely useful things in there, including native character maps, `rgbgfx` for png-to-2bpp conversion, easy linker file syntax, among other QoL improvements. But as much as I love it, I feel like something weird is up. The same software culture being applied here, rapid releases which deprecate a lot of stuff really quickly. That becomes a problem when dealing with old projects which I may not have the time to rewrite... funny how it goes needing to rewrite an assembly program just to keep up with the pace of things!

### Looking at a Few Alternatives

Despite the main GB dev community positioning RGBDS as The Standard Environment&trade; for developing Game Boy programs, several alternatives exist. They may or may not be of taste if you're coming from RGBDS like I am, and some even require different assumptions on assembly programming. Just cursory comparisons, and may not even be fair to their actual capabilities. I'm also not really looking at things like ROM mapping and symbol file creation (useful for debugging). If you're using one of these in a pipeline, your mileage may vary.

#### Vasm

A multi-arch assembler, aiming to be portable. Z80 (and Game Boy ASM) are included as an official module among many other architectures including well-known ones like 6502, M68k and PowerPC, as well as some out-there ones like RPi VideoCore, PDP-11 and the TR3200.

For an assembler, it's got quite a choice when it comes to number prefixes. You can prefix hexadecimal not only with `$`, but `0x` and `&`. You can also do `2Ah` instead of `$2A`. In other words, it's got something for everyone.

[Its docs](http://sun.hasenbraten.de/vasm/release/vasm.html) describe the basic syntax of things, but not much examples. It's probably just me, but I'm having a bit of trouble with what's supposed to be `bss` sections that doesn't start at 0 (like WRAM needing to start at `$C000`), so I'm using "virtual sections" provided by the `dsect` feature. Also with regards to wrangling the sections to fit the Game Boy's memory layout and bank switching, I'm using `rorg` for them instead.

Since `org` specifies entirely new sections, I decided to use `align 3` for all the rst and interrupt vectors. `align 14` is quite useful for filling in the rest of the bank with a huge swath of nothing.

It's worth noting that [ChibiAkumas / Akuyou](https://www.chibiakumas.com/) uses this assembler for his delightfully multi-platform assembly tutorials.

Sample code [here](img/gb-test.vasm.txt).

#### Bass

Another multi-arch assembler, written by you-know-who. Supports the Game Boy target architecture under the name of `gb.cpu`. Aims to be a straight-forward, no-frills, extensible assembler. No linkers, etc, just a plain assembly compiler seemingly following the Unix philosophy. Perfect if you want to make patch ROM hacks (since there's a "modify" switch), though it's also because of this also that you might need to do manual linking.

Funny now that I've said I was trying to use RGBDS for binary hacking, then this exists&hellip;

Compared to the others, the writing style for this target is more C-influenced. There's things like scoping, functions as assembler directives, and then some. It supports anonymous local labels up to two within a single scope (any more than that and it refuses to work, <small>because your code smells, probably</small>). Defining memory is a bit tricky here. It does,  however, support expressions like `while`, which is useful for making unrolled loops. Be wary of gotchas like the fact you need to skip the spaces when doing indirect addressing. Also, I can't seem to find an `include` directive?\* At least, not what I think might be.

All in all, if you target this assembler, coding feels a bit like you're writing inlined assembly in C. You may need to get or write helper tools if you want to work with it, especially for header checksums.

Sample code [here](img/gb-test.bass.txt).

**NOTE**: I've been told that the `include` equivalent is called `insert`. I'll want to try this out later.


#### WLA-DX

Yet another multi-arch assembler. But it's a *very* popular alternative for RGBDS, and for good reason. Although Game Boy ASM is just one of the many architectures it supports, it doesn't just support its assembly language &mdash; but also has its own specific features &mdash; like being able to define headers easily through `.gbheader` and automatically calculating checksums at link time.

Although I probably shouldn't be surprised, since this *was* originally a Game Boy development suite, which then was extended to all sorts of systems because it was **that** good. SNES developers can enjoy the same luxuries WLA-DX offers, since it supports all 3 architectures you have to program for it: 65816, SPC-700, and SuperFX.

It has support for all sorts of funny memory layouts. Although it can feel a bit daunting about having to manage these things, it makes it possible for you to specify weird memory layouts if you have to program some other funny systems. It also has a lot of things that RGBDS even aspires to have, like structs (very much useful for virtual sprite management, inventories, or party member data), tables (useful for quickly asserting some fixed data structure), and frankly a *lot* of other things.

There's loads of options if you want labels. Although local labels are specified with a `_` prefix, you can have multiple levels of "local" by specifying a number of `@` prefixes. Sometimes, a piece of code may have multiple different parts. A one-level local label is bound to have names like: `.okay`, `yep`, `.ok2`, `.done`, which may be confusing in a large file. Multi-level labels might be able to help with this. Anonymous local labels are also available, useful for simple loops.

Point is, this thing is all about control. It's got [excellent documentation](https://wla-dx.readthedocs.io/en/latest/index.html), and I do recommend reading it. It's got explanations of everything *and* some examples, which helps if you want to unleash the full power of this beast.

Sample code [here](img/gb-test.wladx.txt).

#### ASMotor

This is where it all began. Or at least, part of where it all began. RGBDS's [history](https://archive.is/MmQSw) notes that SurfSmurf / Carsten S&oslash;rensen developed ASMotor first in 1997, and then in 1999, Otaku no Zoku / Justin Lloyd uses ASMotor to create a compiler for Game Boy assembly, called RGBDS.

Looking around a bit, it would seem that things are a bit confused. Straight from the horse's mouth, it appears that (like WLA-DX) S&oslash;rensen made [RGBDS](https://web.archive.org/web/19980623225041if_/http://www.matilde.demon.co.uk/rgbzone.htm) *first*, then extended it as a suite of assemblers called [ASMotor](https://web.archive.org/web/19980623225035if_/http://www.matilde.demon.co.uk/asmotor.htm). The RGBASM (xAsm) history page mentions 1996-10-01 as the first release, while *first ASMotor release* is specified as 1997-07-03. Perhaps the fact that ASMotor (and xSomething) is mentioned more than RGBDS gave the *impression* that ASMotor came first. If it's not that, then an alternative interpretation of ASMotor being RGBDS's "spiritual successor" possibly could be the fact that ASMotor supports more architectures than just the Game Boy. There's also [this thread](https://github.com/gbdev/rgbds/issues/128) in which both S&oslash;rensen and Lloyd explains it from their own sides.

Either way, ASMotor today is very similar to how RGBDS was in its early days. Whereas RGBDS as we know it now deviates a ton and is a lot more modern, ASMotor remains "stable" and faithful to its roots&hellip; perhaps a little *too* faithful.

A comparison to a modern RGBDS workflow is inevitable. For starters, if you want each section to be in its own bank, you'll have to specify them manually (as with other assemblers). In this regard, `rgblink` has the upper hand, since its link file can also double as a bird's eye view of the ROM. Some oddities exist, like that `endm` has to be indented first. Syntax differs still, such as `jp [hl]` but not `jp hl`, and `ldi [hl], a` `ld [hl+], a` but not `ld [hli], a`. And as described in my sample code, unused space between sections will **always** be filled with `FF` regardless of what you set with `-z`, *unless* you explicitly use `ds` to specify unallocated space. Group names like `HOME` and `CODE` *do* need to be in all caps (`HOME` == `ROM0`, while `CODE` and `DATA` == `ROMX`).  Also, importing/exporting symbols manually is a thing you do here, though you can also use the global export marker `::`.

Symbol files are generated with xLink's `-m` flag, and while its syntax is compatible with bgb's implementation of the .sym format, not every label is present &mdash; seems to be only the ones that are used somewhere else. This does not appear to include local labels.

Its [docs](https://github.com/asmotor/asmotor/tree/master/doc) help a *little* bit, but they fall a little short. For example, the `HRAM` and `VRAM` group symbols don't appear to be documented, and the closest thing would be the GB examples that S&oslash;rensen gives in a separate repo. Even [the old docs](http://web.archive.org/web/20170316222649if_/http://otakunozoku.com/RGBDSdocs/) were more complete. Although unlike the old xLink, modern xLink doesn't seem to accept WLA-styled linkfiles anymore.

There is, however, one cutting edge of today's ASMotor: the ability to inline code and data literals. So instead of
```
Test:
	ld de, Text
	ld hl, wTilemap + (20 * 2) + 5
	jp PrintText

Text:
	db "Hello", 0
```
you can do:
```
Test:
	ld de, { db "Hello", 0 }
	ld hl, wTilemap + (20 * 2) + 5
	jp PrintText
```
Very much useful if all you want is to display text and not have to name every single string that appears.

S&oslash;rensen also provides `xgbfix` separately as part of the ASMotor suite. If you do want to ditch RGBDS entirely (and you're also familiar with the RGBDS of old), this *should* be a valid choice since all you need (except for `rgbgfx`) has its equivalents here.

Sample code [here](img/gb-test.asmotor.txt)

<!--### Overall

This is just what I personally feel. I'm in no position (technical or managerial) to say something about GB dev as a whole, but this is just what I got from it.

So far, if the whole thing comes to a head, my best candidates personally would be either WLA-DX (for its power) and ASMotor (for familiarity). Though realistically, I think I would still be using an older version of RGBDS.

If I can talk so much about passion, where's the work to back it up? Well, [here it is](https://gitlab.com/zumid/the-storage-room). It's incomplete, needs a bunch of optimizations, but the basics are there&hellip; kinda. I haven't yet got the guts to try making something that requires constantly-loading parts and collision &mdash; y'know, something resembling an *actual* game (platformers and RPGs among them).

I just hope it won't get this bad.-->

**OVERALL NOTE:** If you're feeling salty, right click and view page source to read what I *really* feel&hellip;
