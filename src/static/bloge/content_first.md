<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "Write stuff on a website" -->

<!--:longdate    = "2019-07-25T00:44:56+07:00" -->
<!--:lastedited  = "2019-07-25T00:44:56+07:00" -->
<!--:category    = "rants" -->

**TL;DR**: Got markup-pilled.

Yawnfest incoming.

I saw a few articles that grieves the state of the web as it is today. Websites that are too big, websites that straight-up refuse to work without JavaScript. To maintain a sane web, you have to recall the early days of WWW, but do it without leaving the comfort of current year's trendy design. Those articles prompted me to refine my principles on making a website. Minimizing frameworks, avoiding jQuery (and other such conveniences) was already on my list. But I hadn't seen them as integral requirements then.

Looking for best practices, I discovered that the core of it all is creating your *content first*. Looks like the most obvious thing on the planet, but a lot of my time making general websites till then was focusing so much on mock-ups. When the time came to implement them, I fixated on what it will look, forgetting how accessible or graceful it looks on anything other than the latest and greatest stuff (or a decent connection).

I was tempted to put all sorts of Bootstrap stuff to make my work easier, yet when it's time to customize them... that's more of a mess. Between the existing display override classes and the thorough Bootstrap style, adding my own styles made it more complex than it needs to be.

Hoo boy you don't wanna talk about WYSIWYG editors. We all know that they make the most redundant stuff that will bite you back in the ass when it comes to editing an old document. How did that text suddenly get bold? Hey, I don't remember setting a small font size here. Anyway...

Of those articles, one emphasizes on [the content-first thing](https://cutcodedown.com/) , and another agrees on a [more semantic web](http://www.kapowaz.net/articles/cargo-cult-css) . Article on [progressive enhancement](https://cutcodedown.com/article/progressive_enhancement_part1) pretty much brings it home. By focusing on the content and core markup first, I'm set to improve accessibility and SEO from the get-go, without needing a ton of plugins and more frameworks for that. It has, pretty much, changed my perspective on web designing. I do have reservations though, such as using more of the semantic HTML5 when needed.

Content and design are equally important... Using the principles set forth there, I begin to shape the form of the text-mode site that a significant 1% will see. Then for the other 99%, I shape the layout that's fit for the present day, making the best use of what we have in CSS.

When I designed a site this way, I felt some new kind of satisfaction. It was, you would say, empowering. Being in control of what markup you put in, and how it makes content readable feels pretty amazing. Taking back control by actually understanding web standards and using them to my advantage gives the same effect as manually customizing a framework style, only with less wild hairs. That being said, with great power comes great responsibility. Spaghetti code (and extra CSS bloat) can attack at any time, and I'm already feeling the pushbacks from it. 

Mobile-first design is also very useful considering our market, and yes, you can do that too. In fact, you could hotswap styles since HTML code is our document definition. Adhering to the ideal "HTML=document markup, CSS=layout and presentation" works wonders. I feel a little safer now.

In fact, I might just rework my website.

Yep 👀

I feel like my stuff is a bit of a mess lately. Doesn't look broken, but hey, with a newer, crappier look that looks ""modern"" I could clean up my act and make my website slightly more okay.

The difference is that I'll actually test my website without any styling.
