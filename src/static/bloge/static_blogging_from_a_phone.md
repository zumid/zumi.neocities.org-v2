<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Static Blogging from a Phone"-->

<!--:longdate="2019-02-18T00:00:00+00:00"-->
<!--:lastedited="2019-02-18T00:00:00+00:00"-->
<!--:category = "site-updates"-->

As mentioned before, I have a system which (is supposed to) ease the process of making my static blogs.

I could have used Jekyll, but I'm a simpleton and I wanted it to be optimized for Neocities, so I built my own even if it's lackluster. (might try porting later idk)

It's really just a build system that generates HTML pages from Markdown files. Now that it's rewritten in Python, I can rest a little bit easier. Probably because that other one (written in `sh`) looks... dirty. Feels like a kludge of different awkward `sed` commands and pipes. Eugh.

(eh, I might find a way to release this as a generic package. Right now it's personal-use.)

I had hoped to have this system running on my Android phone, so I can write anywhere. I thought it would be a piece of cake with Termux installed.

A little bit more complicated than that, it turns out.

My system requires Pandoc, since it is a really versatile tool for converting all sorts of documents of any format - even MediaWiki (Wikipedia, Wikia/FANDOM, etc.) - a markup format I've long since forgotten, mainly because we all left Wikia at this point.

In this case, I use it to convert raw markdown text into HTML snippets to put in my page template. And I don't have to go through one of those converter sites to use it!

Anyway, since Python is already installed in Termux, I just needed Pandoc.

So I turned to Termux's package manager, `pkg`, and asked it for `pandoc`. It replied "no packages found". I then asked the Internet for answers.

Turns out pandoc isn't in the official repos because Haskell isn't in it, and Haskell isn't in it because it's "too slow for a phone." Huh.

Initially I tried to just run the pandoc ARM binary (from the debian repos) on Termux. Obviously that didn't work.

Then I found this project called [AnLinux](https://github.com/EXALAB/AnLinux-App), which provides an actual Linux environment on top of Termux. Eh, sure, why not. The app is really just a command generator to paste into Termux (`wget | sh`, lmao)

The command downloads and installs the base system of some distribution (Debian in my case) as well as the script to enter the environment. I ran it on my home directory and I got 2 files: one's a jumbled mess that is probably the system and the other's a customizable script named `start-debian.sh`.

I edited the script file in order to access my home directory (which contains the folder for my blog setup), which is simple as uncommenting a single line.

Issuing `./start-debian.sh`, I booted up the Debian virtual environment. Weighing in a couple of hundred MB, it was really barebones, which is cool.

I installed `python3`, `pandoc`, and `pip` (in case I needed more modules) and whatever else necessary (like less and nano)

I tested pandoc. To my delight, the program runs. Next, I tested the scripts. It runs as well, and it successfully builds! The upload script runs similarly well.

Sidenote, this is all running as `root` on the virtual env, which makes me slightly uncomfortable LOL

So here I am writing this on a phone, under all that stuff. Maybe I shouldn't burden myself and just link to Blogspot or something, but it's fun!

Now I need to think of how to easily include images so it wouldn't just be a wall of text. Oh, and possibly making it WYSIWYG too, just because.

Anyway, if anyone knows of an alternative to Pandoc that's just as easy to use but can actually run inside Termux, let me know!
