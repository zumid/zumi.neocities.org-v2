<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "boom\, college" -->

<!--:longdate    = "2019-09-15T21:22:30+07:00" -->
<!--:lastedited  = "2019-09-15T21:29:03+07:00" -->
<!--:category    = "personal" -->

So... college started last month and as a result my days get really hectic really fast. All of my time is pretty much eaten up by assignments and random crap. Where does that leave my own projects? To the back of the line, that's where.

I can't even track where I left off on my own projects. Now that's where WIP comments are handy, but I guess I forgot to put them on the assumption that I will finish them. Whoops.

Yeah. That's my update. And I *still* have a couple of assignments to finish.
