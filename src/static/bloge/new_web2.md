<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Is it refresh time? I think it's refresh time."-->

<!--:longdate="2021-04-10T19:41:55+07:00"-->
<!--:lastedited="2021-04-10T19:41:55+07:00"-->
<!--:category = "site-updates"-->

Whooooaaaaa, what happened?!

Well, yeah! I think it's about time I changed up my design, anyway. Although it uses less images than before, I tried to keep it sort of old-school style with maybe a pinch of modernity slipped in. This time though my aim is to make it Web 2.0-ish, plus the full-on skeuomorphism rather than the bevel-and-emboss-lookin' ""neomorphism"" that people are doing lately.

And as always, I try to keep the images as small as possible. The blurred background here is rendered as a JPEG at a really low res, so it weighs 4kb. The biggest site-image by far is the header at 33kb, though maybe I can get rid of that.

<figure class="fig-left">
<img src="img/newweb_lightdark.png" alt="Light and dark themes of the new layout.">
<figcaption>Light and dark.</figcaption>
</figure>

Since I basically neglected to make a dark mode for my previous layout, I might as well do it here. Different modes need different backgrounds, both of which I made a long time ago. Maybe I'll offer them up as wallpapers at some point. Until then, enjoy the nice prerendered blur lol

<figure class="fig-left">
<img src="img/newweb_mobile.png" alt="Mobile menu 'combo box'.">
<figcaption>Glossy buttons, nice gradients.</figcaption>
</figure>

Also, even I'm trying to go for a Web 2.0-look, I think I ended up making it look like iOS &lt;= 6. Hey, why not grab stuff from it outright? I figure I might as well go all the way. Voila, the menu's a combo box on mobile now. And just look at that glossy button. I'm fairly proud of it. :D

Also since Samsung likes to replace random non-emoji Unicode symbols with layout-ruining emoji, I figured that I might as well just skip the hamburgers (even as a CSS layout, I'm too lazy for that) in favor of English text.

Now as I take the opportunity to completely redesign the layout, might as well look at my source folder... it's looking a little unwieldy, and it's kind of a mess. Or it's just that I don't like how it's laid out, and I should think of something.

I think last time I had the build process create files *inside* the source folder... yikes. Either way I completely remade the structure so that it's easier for me to read, that it doesn't do that, and that the build folder can be deleted safely. I now have one Makefile instead of several across different folders (which were... ehhhh.)

I also rewrote my RSS generator, which previously depended on `python-feedgen`. Now it just depends on `python-lxml` which `feedgen` depended on anyway. My reasoning is that `python-lxml` is more readily available on several distro's repos since enough stuff depends on it enough to warrant a separate package.

Speaking of, they're all contained in `tools/`. I made submodules for everything now, so everything's roughly in one place and you only need maybe 2 packages instead of several. Because I really don't feel like maintaining more virtualenvs, man. I now maybe need like one package (`python-markdown` for MDiocre)

I also made sure the fonts are self-hosted this time instead of sending you to good old Mr. Google. You can thank me later, lol

Of course, since it's built to "just work" for now, nothing's very optimized yet. This time around though, yes! You *can* look at the [source](https://gitlab.com/zumid/zumi.neocities.org-v2) this time! Anyway, back to studying for my midterms...
