<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Theming this Site, and Other Stuff"-->
<!--:description="In which I take a look at the carcasses left behind by the sheer enthusiasm of early and mid 2000s WWW."-->
<!--:longdate="2021-12-07T13:29:35+07:00"-->
<!--:lastedited="2023-06-29T11:00:26+07:00"-->
<!--:category = "site-updates"-->

Each time I aim mindlessly on the Internet, I stumble upon some visual styles that evokes simpler times. That is, anything but the extremely rounded, flat, and animation-intensive ones of today &mdash; that hides how much of a clusterfuck of the underlying system is inside. Sometimes I gaze upon something skeuomorphic, sometimes it's Aero, sometimes it's Holo. And sometimes, it's the industrial embossed look of the 90s.

This site already is styled after the glossy stuff seen on Windows Vista and its contemporaries (OS X 10.5, iOS 4). But I guess I needed to also make my site look like all those other stuff, too. My friend's site has this theme switcher, so I was inspired to put one up on mine as well.

So from now on, this site will use JavaScript not only for the blog reading time, it also uses localStorage to save theme preferences - rest assured, everything will still be okay if you have NoScript.

### About that New Theme
<figure class="fig-right">
    <img src="img/site_holo.png" alt="My website, with the makeshift Tron-like theme.">
    <figcaption>Glows like an agent</figcaption>
</figure>

The first theme I'm trying out here is Android's Holo theme. Not the Holo that most people know, however. It's the early "Holo" look that was on Android 3.x (Honeycomb). I just love the cheesy Tron look they have going on back then, it's underrated as hell. I suppose this wasn't liked more because 3.x was a rushed, tablet-only release. Even then, as I'm currently (painfully) writing Android apps for an assignment, the Fragments stuff they introduced here is pretty neat as well.

I borrowed some code from the existing [Holo Web](https://holo.zmyaro.com/) project, these look pretty faithful already. I'll probably have the later (4.x) Holo look here eventually &mdash; especially since these are the ones I've actually grown up with.

<aside>
	<p>
		Speaking of Honeycomb: I actually went to the trouble of trying to install it on Android's virtual machine.
		It was not pleasant. It's also ARM-only.
		For best results, you should get an old version of the Android SDK
		and run it under Windows. I also downloaded the Honeycomb images through Android Studio.
		You'd need to do some fiddling with the AVD and images directory.
	</p>
	<p>
		Pardon my vagueness, but through a <strong>lot</strong> of fiddling it's a miracle that it even
		works <em>at all</em>. It's slow as molasses, and the touch emulation doesn't even work.
		Using keyboard navigation however works. There's no 3D graphics to be seen here, so the
		Holo experience suffers quite a bit - at least I was able to experience a bit of Honeycomb
		at all. But I'm getting sidetracked, so let's get back on track.
	</p>
</aside>

I plan to eventually have a wide range of themes representing different eras, maybe ones with icons as well. And yeah, that includes the original Material Design and (if I can even pull it off) the new "Material You" design. On second thought, I suppose this is just my Time Machine page but extended to the entire website, huh.

### Reducing Dependencies

Another thing I've done is make it easier for my own site to build by reducing the dependencies needed. In the end, `mdiocre` and `docutils` are now pretty much the *only* real dependencies (with *pygments* being optional). Most of this work was writing replacement tools that just do one thing I really needed from each. Boy, was it satisfying.

The first step on reducing dependencies was by rewriting the RSS feed generator. It used lxml, which &mdash; on some setups &mdash; is a pain to install. So, I made it just use Python's built-in XML serializer. There is still yet some quirks to be fixed, but I think it works well enough.

Next up, I "flattened" the templates by just doing it in HTML instead of Pug and `pypugjs`. Since I really needed to change the links to the base path, this is just handled by `mktemplate.py`.

Last, I decided that I was just gonna do my stylesheets in plain CSS. Each style is its own folder with a bunch of CSS files that are then strung together by the make process. My color themes rely on `prefers-color-scheme`, something that `lesscpy` doesn't support &mdash; so that's why there was a separate `color-themes.css` that was included.

The overhead that was reduced by getting rid of `color-themes.css` turns out to just be replaced with the theme switcher feature, and the desire to just have one master template. Because everything is just one template, the blog timer script is on every page now. Cringe, I know. But no, it gets cringier than that.

The theme switcher feature uses a script in the HEAD. Apparently, it does work to reduce flash-of-unstyled-content. Well, if your stuff is cached anyway. Mileage may vary depending on browser and connection circumstances, but we'll see.

Next up: [Themes, part 2](themes_2.html)
