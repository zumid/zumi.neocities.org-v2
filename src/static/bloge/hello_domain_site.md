<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Hello domain site."-->

<!--:longdate="2020-03-18T17:05:07+07:00"-->
<!--:lastedited="2020-03-18T17:05:07+07:00"-->
<!--:category = "site-updates"-->

Holy damn it's a long time since I blogged here. Anyway I bought my domain
name to play around in. And a cheap hosting, which I got my own wi-fi blocked
from because I accidentally sent way too many requests or something. Could
be when I wanted to upload >1000 files for a blog or something.

[More on that here](https://spaces.sarajyeo.xyz/Zumi/bloge/owning-a-domain).

Now I can basically do whatever I want, from dynamic blog/webpages, wikis,
forums, letting really old browsers see my shit...

I'm not gonna point that here, though. It's a one-year plan I'm honestly
hesitant to extend (I don't even know if I can maintain it or let it
fester). And once that's over, I'm gonna export the site to my own computer
and the new bloge converted to this static site format.
