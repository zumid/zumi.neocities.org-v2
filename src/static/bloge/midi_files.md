<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="It's time to make some MIDI covers."-->
<!--:description="Time to wake up and SMELL the pain, then make a bunch of MIDIs :)"-->
<!--:longdate="2021-10-29T23:54:38+07:00"-->
<!--:lastedited="2021-10-29T23:54:38+07:00"-->
<!--:category = "behind-the-scenes"-->

It's fun to make MIDI covers again (even if I don't make that much in the first place!). MIDI used to be a big thing, basically when your connection is too slow to get MP3's and such, or you can't outright get some song, you bet someone's probably made a MIDI version of it. A ton of Geocitizens share their collection [(as I do now, lol)](/stuff/midi/) for people to jam to. Sometimes they even love a MIDI so much that they make users have to listen to it when loading their website. Boy, that totally didn't get annoying. Tooootally.

Now there isn't actually one "MIDI sound" - after all, it's a standard, meaning that it'll sound different depending on the device that plays them. I think that's part of the charm, you can play MIDI files on anything from little bleep-bloopy chips to rockin' FM machines all the way to fancy software wearing fancy soundfonts.

Naturally, I gotta have a point of reference when I make these things. So when we say "the MIDI sound" we typically refer to the default synth that came with Windows - the Microsoft GS Wavetable Synth. You may also have heard of its soundbank - `GM.DLS`. It's a shittified version of the bank used in [Roland's SCC-1](https://soundprogramming.net/soundcards/roland-scc-1/) soundcard, which in turn is based on the SC-55 Sound Canvas. Shame it got degraded so bad, and Microsoft never bothered to license a better one. Because it's the one everyone is used to, when I make my MIDIs I usually want to target this.

### Sequencing

Take the [Giga Size](/stuff/midi/mine/gigasize.mid) cover I recently did. First I have the source track, of course. Maybe I'd double that, only pitched up by a whole octave so I could listen to the bass better, and maybe expose some additional tones in the process. Basically, making a MIDI might be considered poor-man's recreation, but I had fun all the same.

<figure>
    <img src="img/gigaqueen_setup.png" alt="Screenshot showing the Giga Size cover setup consisting of source tracks and patterns for MIDI tracks.">
    <figcaption>The project setup.</figcaption>
</figure>

Because I'm trying to target the "default MIDI sound", I'd wanna set up a thing to simulate it. I use FL Studio mostly on Linux (lol), so Fruity LSD is a no-go. However, people have rendered the default MIDI soundbank as soundfonts, possibly to fall back on. You have `GMGSx.sf2` (which, I guess is almost a rip of the Windows version) that comes with SynthFont, as well as `Scc1t2.sf2` (a direct rip of the SCC-1 bank that also has more sounds). I combined it with this nice VSTi called the [BassMidi VSTi](https://www.kvraudio.com/product/bassmidi-vsti-by-falcosoft) that takes this *and* allows setting a MIDI port to play multiple channels simultaneously. I think the other soundfont-related VSTi's are specialized to play like one instrument, so this definitely hits the mark.

<figure class="fig-left">
    <img src="img/bassmidi_plugin_setup.png" alt="BassMidi plugin setup, showing MIDI input port set to 0.">
    <figcaption>Setting up BassMidi VSTi.</figcaption>
</figure>

I load it into the `Default Soundfont` input under `Soundfont/Bassmidi Setup`. There's a `Maximum Polyphony` option, which I can use to simulate the Windows synth choking on it when too many voices are played at the same time. I do test my MIDIs in Windows XP and Windows 10 (dual-boot lol) to see if they retain similar mixing and stuff like that. And I did find that somehow Windows 10's synth limitations are *worse* than XP's. Some notes would just cut seemingly because of too much polyphony, when I didn't have that problem in XP.

<figure class="fig-left">
    <img src="img/bassmidi_vsti_setup.png" alt="Part of the soundfont setup menu showing that the GMGSx soundfont has been loaded.">
    <figcaption>Loading that jank soundfont.</figcaption>
</figure>

I cover away, trying to transcribe as much of the music as possible. I can then add some rough automation events at this time (which will be cleaned up later). Mostly pitch bends, through the `Channel pitch` control.

Now if you thought graceful degradation just applies to shabby websites like this, turns out this is also applicable to MIDI! Some devices can't play pitch bends properly, so the way to solve it is to write the final note that it slides to, *then* perform a pitch bend from some value to 0. Some people do it backwards, so that results in dud notes when the MIDI's played with a device that either bends too much or too little.

Somehow, General MIDI also encodes a bunch of sound effects as recognized patches. It'll sound good using my reference soundfont, but not for others. So I just place them in notes that fit with the key of the song... most of the time. I sometimes forget to do that and the result is... eh.

### Refining

Once everything sounds good, I then feed it into Rosegarden to edit it in a little more detail. The interface is a bit weird in comparison - there's a score view (is cool but idk), and the piano roll is called a "matrix" instead. For this one, I needed to start a Timidity daemon to preview sounds with: `timidity -iA`

I just get there to do some post-processing stuff like tidy up the channels and possibly add a bunch of markers or text events for attribution, loop points and such. Tidying up events is a matter of trying to remake them using `Controllers -> Insert Pitch Bend Sequence...`. How it works is that you select a note for it to hook up to, since the percentage units seem to be tied to it. I'm not sure what the pitch amount relates to here, but that's done through trial and error. What I then would get is the same pitch bend sequence, but neater.

<figure class="fig-right">
    <img src="img/rosegarden_pitch.png" alt="Rosegarden pitch sequence settings">
    <figcaption>wheeeeeeeeeeeeeeeeeeeeeee</figcaption>
</figure>

In the case of this cover, I also did a channel volume sequence in the rhythm guitars intro, just to "force" a decay effect I guess. Now the thing is that the volume of the track in Rosegarden itself doesn't change, but you have a volume controller. So I guess that would be handy.

<figure class="fig-right">
    <img src="img/rosegarden_pitchevent.png" alt="The event editor showing three specific events: controller 100 and 101 set to 0, and controller 6 set to 12.">
    <figcaption>Ooh, I wonder what this could be.</figcaption>
</figure>

One thing I should mention are three specific events in particular:

  * Setting controllers 100 and 101 to 0 sets the next MIDI [Registered Parameter Number](https://www.recordingblogs.com/wiki/midi-registered-parameter-number-rpn) to change. So from the RPN table we can see that this points to the **pitch bend sensitivity parameter**.
  * Setting controller 6 would set the actual value for that parameter. Here it's 12. I have no idea what it would initialize to had these two events been omitted.

Yeah, I had to debug MIDI files too. I found that bit out when I was figuring out why my pitch bends didn't work.

Also, the neat thing about Rosegarden is that it saves MIDIs but it's using [running status](http://midi.teragonaudio.com/tech/midispec/run.htm). Basically it's a way so if two or more events have the same type you just need to define the type once, which ultimately compresses the MIDI data. The drawback is that this can make things confusing, so that's why some other editors - even if they can read these files - will save these as regular MIDIs without running status.

Before I forget, Rosegarden isn't really *that* stable for me on Linux. And it crashes on Windows. But I don't wanna go back to Anvil Studio, so...

### Preparing for Publishing

So after another round of exporting MIDI, I inspect the data using Nineko's MIDI Event Editor [(direct download)](http://digilander.iol.it/projectchaos2/MidiEE030.rar). After like 10 years, I haven't found (or maybe aware of) any similar tool that actually does the job of low-level editing MIDI files without adding extra/opinionated shit - a step above maybe using a hex editor. I've been meaning to make my own implementation(s) of this tool but I haven't got around to seriously completing any of them yet.

<figure>
    <img src="img/midiee.png" alt="Nineko's MIDI Event Editor showing a few meta events.">
</figure>

Anyway, Rosegarden seems to insert copyright info, time signatures and other metadata that I don't think is necessary enough. That copyright one might be problematic to upload to vgmusic, even if it's a placeholder. After cleaning the MIDI up, it's pretty much ready to go wherever I wanna upload it.

### Ow, my ears

So with making MIDI out of the way, I wanna rant about one more thing about MIDI in general.

I've said that it's nice to have a diverse sound set when it comes to these things. But the major **pain** is having the *volumes* or everything but the actual sample vary wildly! There's several factors to that, including a track's volume, individual note's velocity, hell, *the velocity curve of the synth?!* It's just madness.

Alright, let's have two software synths that take one soundfont. I try it on Timidity++, it sounds great! But on FluidSynth it sounds like shit, whether it be overpowered instruments or too much chorus effect. Worse, the latter is so easy to integrate that just about everyone uses it. So I have to be careful to pick one that sounds good with both, because I'd never guess.

Even worse, **loudness wars** even seem to exist here. I see MIDI files that just have the volume of everything maxed out. Even *that* seemed to die out in the general music industry nowadays due to streaming services normalizing everything... yeah, try doing the same with basic MIDI. Coupled with the issue of inconsistent volumes and everything, you can be sure that if you're not careful, you *will* blow out your ears just trying to enjoy a nice MIDI.

So... that's why I just assume everyone's gonna be using Windows Media Player to play my MIDIs even if I use a lot of different stuff myself.

Next up: [MIDI part 2: raising some bars](midi_files_2.html)
