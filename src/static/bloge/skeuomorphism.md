<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="'Skeuomorphism' hacks on top of flat design"-->
<!--:description="What if you come from flat design, but you wanted to bring back 2000s-esque details, class and etc. Let's review recent attempts..."-->
<!--:longdate="2022-01-26T08:42:15+07:00"-->
<!--:lastedited="2022-01-26T08:42:15+07:00"-->
<!--:category = "musings"-->

It's the 2020s, which means that the kinds of interfaces I grew up with - having taken for granted, shrugged off, but I sorely miss now that I associate flat design with sorrowful adulthood and the realization that life doesn't seem all that simple and "free" anymore. Maybe I'm older now, so I view whatever was alive alongside my simple, hapless young self with rose-tinted glasses.

Flat design is perfectly okay, and in a lot of cases far more practical than what came before, which people call "skeuomorphic design", that is, mimicking real life stuff down to the textures. However stylish it looks, to a lot of people it sort of reduces design such that the only difference in implementation is merely font, color scheme and proportions.

<figure>
	<img src="img/ios6v7_locserv.jpg" alt="Skeuomorphic, fig. iOS 6. Flat design, fig. iOS 7. Pictured: Location services menu." width="300">
	<figcaption>the unsoulification</figcaption>
</figure>

### Why

People point to responsive design and scalable graphics as the death of skeuomorphic interfaces. Skeuomorphic design by its very nature is detail-oriented, and that can result in a Thing using more resources for gradients and textures (be it image-based or generated in real-time). So people got rid of all that, and stuck to the bare basics of graphics - shapes, colors and sometimes even motion. Those three elements alone is what's being pushed in a lot of flat-oriented design languages in the 2010s and 2020s.

Now that displays are smaller, in the same way that logos should always be "readable" at small sizes so as to recognize it, UI should be fit for the smallest design so details must be reduced... at least, that's what they say.

<figure class="fig-right">
	<img src="img/android12.jpg" alt="Android 12 redesign: Material You (are an idiot)" width="300">
	<figcaption>☺ Ha, haha ha ha ha ha ha ha! ☺</figcaption>
</figure>

I'm willing to argue that flat design sort of "democratized" graphic design in general, in the sense that anyone with even a little experience making something in Canva can fill in for me when I'm not "available"...

My experience as a design director for a small student-run organization has showed that when I implemented one of the new skeuomorphic-inspired styles, not many can emulate it (despite showing them a quick tutorial for how to do what I did). But then I went back to a pastel-like flat design language, and suddenly they can all do that and blend in easier.

So in that sense, in the same way that frameworks make it "easier for collaboration", flat design is relatively consistent and doesn't have much of a curve (relative to what came earlier), so anyone can join in and collaborate on something. It also makes it easier for a commoner to make something that society has deemed "looks good", something that you more than likely need to hire an actual designer for in decades past.

I should be pointing out by now that flat design is nothing new, you could check other articles covering flat design and they'll tell you that it goes all the way to Bauhaus, but I'm not gonna go deep into that.

Anyway, people realized that flat design has its [accessibility issues](https://www.nngroup.com/articles/flat-design/) when used in a digital context and tried to add back depth to it. Google's attempt &mdash; Material Design &mdash; reconciles that by imagining the UI as a stack of imagined "digital paper" with their own physics as to how they should interact. The addition of shadows help to differentiate some elements to others, such as buttons.

Then people thought it wasn't quite enough. History rhymes, so people begin adding back in 3D effects, and looked at the old designs for inspiration. Meanwhile, Google took a step back and got rid of shadows in Android 12...

### How did it go?

<figure>
	<img src="img/neumorphism.png" alt="Google search page showing Neumorphism" width="300">
	<figcaption>Barely visible</figcaption>
</figure>

tl;dr: Sad.

The sad attempt a few years back by people trying to bring back "skeuomorphism" is what they call "neumorphism". Drape the screen in either white or black, draw some faint outlines because they sure love their micro contrasts. Job done. It's even *more* same-y than flat design. At the very least, flat design actually has *dominant* colors!

Here, everything looks like it's been imprinted onto a surface by way of Filter &rarr; Emboss. Nothing actually looks real, either. It's just real "pretty". So pretty that it's [bordering on invisible](https://andersen.sdu.dk/vaerk/hersholt/TheEmperorsNewClothes_e.html). Some people say that it's a pathetic "innovation" by people too scared to leave flat design, and I tend to agree on that.

<figure class="fig-right">
	<img src="img/claything.png" alt="Example image of claymorphism" width="300">
	<figcaption>image from <i>Malewicz</i></figcaption>
</figure>

More recently, people are using actual 3D for elements, some people call this "claymorphism", because everything looks like it's been made out of clay. A lot better than the inaccessible stuff from prior, but it still suffers from "hey let's slap bevel and emboss on our flat stuff and call it a day".

And personally, I'm also a bit jaded from the whole friendly facade everyone puts on, because of the darker undertones behind it all (between corporate culture, cryptobros and accepted extremism alike). It's built to be friendly to the point of downright condescension, and that's not a bug.

I've always said these two are just hacks on top of flat design, as opposed to an actual attempt at bringing back the skeuomorphic spirit. I don't know why, but that may be because it has less graphical fidelity than the old school stuff.

<figure class="fig-left">
	<img src="img/skeuocord.png" alt="Skeuocord theme" width="300">
	<figcaption>Skeuocord by <i>Marda33</i></figcaption>
</figure>

Disappointed by the two, some people then try emulating 00s skeuomorphism. But I feel like a few of them do it too much. Gradients, textures and strokes *are* the basic elements of doing skeuomorphism, since they allow different types of material to be represented. But sometimes, in trying to sell something as reminiscent of the old, you put too much and it doesn't look all that great.

<figure class="fig-left">
	<img src="img/zoid.png" alt="Zoid theme" width="300">
	<figcaption>Zoid by <i>Theme Kings</i></figcaption>
</figure>

Then there's people who [haven't changed at all](http://themekings.net/), perfectly embodying the spirit of 00s-era skeuo. But as I said, in today's environment with such a diverse amount of screen sizes, you can't exactly bring back 100% of the 00s soul, since you also need to consider if the layout would size properly... then again, you could of course have two entirely different themes, only split by breakpoint. But that's not 100% practical.

### My hot take

So how do I see things? First, some more inspiration:

Design critic [Eli Schiff](https://elischiff.com/) (of @humansofflat fame) seems to favor what he calls ["dimensional design"](https://archive.fo/qm4Xr). Compared to what people call skeuomorphism, I understand it as "basically that but without the excessive detail". In other words, skeuo with class. Certainly looks like the design language that Apple has adopted circa 2013 or so. In that respect, Flash buttons of early 00s Newgrounds can be considered skeuomorphic. You have material emulation, and you have detail. But it still looks toony, so people don't consider that classy.

Responding to the visual refresh of macOS 11, he put out [a more tasteful reimagining](https://dribbble.com/shots/12304683-Damning-the-dead), political rumblings aside. People had thought the puffy look that might've precipitated "claymorphism" is a return to dimensional design, but it isn't as "classy", and has the faux-friendliness look I described previously.

Next, to people with preinclinations about what they think the "old-school" design looks like:

To repeat, skeuomorphism is, at its core, emulating the real to make new things familiar. When the digital realm is a new field, you want to throw some kind of real-world material at it to look as if the user can interact with it. In that sense, a rectangular element should look like some real surface, which can be accomplished by placing textures in it. When thinking of material, **not everything should be glossy!** If you did that, you might have only seen Windows Vista. Now, think of real world material &mdash; what do they look like? Take your pick:

<ul class="sample">
	<li class="_matte">Matte</li>
	<li class="_glossy">Glossy</li>
	<li class="_leather">Leather</li>
	<li class="_wooden">Wooden</li>
</ul>

<style>
	.sample {
		display: flex;
		margin-left: 0 !important;
		list-style-type: none;
		text-align: center;
		gap: .5em;
	}
	
	.sample > * {
		flex-grow: 1;
		padding: 1em;
		background-color: #555;
		color: #fff;
		text-shadow: 0 2px #222;
		border-radius: 5px
	}
	
	.sample ._matte {
		background-image: linear-gradient(
			to bottom,
			transparent,
			rgba(0, 0, 0, .40)
		);
		box-shadow:
			0 0 0 1px inset rgba(255, 255, 255, .08);
	}
	
	.sample ._glossy {
		box-shadow:
			0 0 4px inset rgba(255, 255, 255, .36);
		background-image: linear-gradient(
			to bottom,
			rgba(255, 255, 255, .27),
			rgba(255, 255, 255, .07) 50%,
			rgba(255, 255, 255, .01) 50%,
			transparent
		);
	
	}
	
	.sample ._leather {
		box-shadow:
			0 2px 9px inset rgba(255, 255, 255, .2);
		background-image: linear-gradient(
			to bottom,
			transparent,
			rgba(0, 0, 0, .60)
		),
		url("img/skeuo_leather.jpg");
		background-size: 100%;
		background-blend-mode: multiply;
	}
	
	.sample ._wooden {
		box-shadow:
			0 2px 3em inset rgba(255, 255, 255, .2);
		background-image: linear-gradient(
			to bottom,
			transparent,
			rgba(0, 0, 0, .60)
		),
		url("img/skeuo_wood.jpg");
		background-size: 100%;
		background-blend-mode: multiply;
	}
</style>

Despite calling for the return this kind of stuff, I do still think we can dial it back a bit. If in the 2010s you have literal photos representing objects, let's use simplified but depth-like representations, that means using FontAwesome and the like. If in the old school you have every kind of material imaginable, let's pick one that's no-frills but still makes elements distinct enough from each otherr.

Effectively, that is to say that just like neumorphism and claymorphism, this should be feasible as a hack on top of flat design. Only that this is closer in spirit to the 00s and 10s skeuo, yet still feasible, accessible, and doesn't look condescending. Just something that doesn't want to get in your way, that's all. We [do](https://developer.mozilla.org/en-US/docs/Web/CSS/gradient/linear-gradient()) [have](https://developer.mozilla.org/en-US/docs/Web/CSS/box-shadow) [the technology](https://developer.mozilla.org/en-US/docs/Web/CSS/text-shadow), after all.

<figure>
	<img src="img/sample.png" alt="A sample design that I did, which is not flat but not entirely skeuo either." width="300">
	<figcaption>These buttons are so satisfying to style, ngl.</figcaption>
</figure>
