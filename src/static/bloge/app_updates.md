<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "App Updates" -->

<!--:longdate    = "2019-02-17T00:00:00+00:00" -->
<!--:lastedited  = "2019-02-17T00:00:00+00:00" -->
<!--:category    = "rants" -->

Oop, here's some sort of an incoherent rant. I like writing those for some reason.

Good ol' updates; they (are supposed to) bring various bugfixes which improve performance, new and exciting features, make an app more functional, and important security enhancements. On a phone it seems to happen more frequently, probably because most people (including me) live more on a phone than on a desktop.

Aaaand sometimes they fall flat on its face.

* **"Various bugfixes"**, aka crappy changelogs. When the changelogs are vague and only contains that one sentence. Especially if I can't even see what's changed - some developers say "Performance improved" on their changelogs and leave it that way. At least with open source ones I could *potentially* see what was the matter by looking at the commits (even better when there is a bug tracker!)

* **"New and exciting features"** adds bloat, but when done correctly, it minimizes it. More often than not, I see that with each version of some app, some X new feature adds a few extra megabytes to the package. It's okay... when you do it a few times a year. But when you push several new versions in one week it's gonna run my data plan real damn fast. "That's a non-issue! Everyone has Wi-Fi now!" Um, no. And furthermore, *no.*

    Only places I know that have some okay Wi-Fi are my home and my local Starbucks. Everywhere else, I have to rely on my limited 1GB plan that eats my lunch money every once in a while.

* **"Make an app more functional"**, relating to the above. When an app releases a new feature, am I gonna use it that much? I don't know, some people have a use for it - but I may not. Especially if they're essentially redundant - that's bloat I don't need! Is it okay to stick with the old version? The answer's a big fat **no**, why?

* **"*Important security enhancements*."** \*shudder\* All of the problems don't matter if this is brought to the discussion. I understand, no program is without its holes - especially security holes that pentesters and crackers can play with to their leisure. Especially crackers. Ransomware... fucking yawn man, at least throw me animated fire on my screen.

    Anyway, it's very important to patch them up immediately once one is spotted and CVE'd. But when you hide it under all the bloat, and consequently use it as a rationalization to lock previous versions, that's where I can't stand it.

Because I see a trend these days, what's with various applications *forcing* you to update? Even with the point of (muh) security, *why do updates have to be so painful?*

Windows 10 shuts you down in the middle of doing something just to install its `precious updates`, and WhatsApp just blocks you with a big screen until you install a new version that has... "stories" or whatever. Yeah, it's Facebook, but my classmates live there so what the fuck.

I'd be more willing to update if there's a big API change (like encrypted chat), but for shitty features adapted from Facebook that this app doesn't need? No thanks.

And... oh god, *Google Play*. I almost forgot about that one. Why does it ever decide to update so randomly, and doesn't care whether or not I am on mobile data?! "Uh this is really important I have to update." No, not right now dammit. I'm on a- "Fuck you I'm doing it anyway". Goddamn.

I'm aware that on Google Play there is an option to defer *app* updates when you're not on Wi-Fi, but not *Google Play itself*! Why is that, or am I just too stupid to not notice some other option that does exactly that?

"You may not like their enforcing of updates, but they certainly have the right to" - correct, I could use alternatives, but we all know convenience and pragmatism just throws them off the wayside. No one's using these besides me and I look like a weirdo!

"Updates are like this because y'all are idiots", what, you think? Some people refuse to update for a reason. I've heard of people not doing it just because they're happy with what they have. Again, convenience. You know how Windows 8 went...

I've heard of enterprises that still use 90s software. Some banks still use COBOL, even. I mean it's a time bomb a-ticking, but data porting is expensive. Only thing that will motivate them now is [Year 2038](https://en.wikipedia.org/wiki/Year_2038_problem).

\*sigh\* How do we fix this then?

Well, if automatic updates are the best way to go, at least be way, way more careful.

*Use more descriptive change logs.* That's the number one reason anyone's gonna want to update. They want a good reason to update their crap. I'm pretty sure that's one factor behind people not wanting to update Windows - the update description only has a portion of what it does when it actually does so much else! Wanna be technical? Go for it! Forgetting something? Version control system!

*Try not to bloat*. Might fail when you're hooked on hype-building.

And the most important might be **don't make updates so painful**. Windows updates are naturally painful, largely due to how it's built, and it'll take never to actually sort the root of the problem. The proposed solution, "active hours", are shitty and sometimes doesn't work. Just have people manually schedule them and notify them when it's about to update or something. Furthermore, actually check whether or not someone's using mobile data / metered connection and act accordingly.

Or actually, let's go back to the dark ages and say "manual updates" - at least you have some degree of control so that it won't suddenly break down when you're presenting an important matter. Painless!

There we go, some actual content instead of a plain ol' lorem ipsum
