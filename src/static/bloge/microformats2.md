<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="(Moved) Microformats2"-->
<!--:description = "These are nice to have." -->
<!--:longdate="2020-02-08T23:16:00+07:00"-->
<!--:lastedited="2020-02-08T23:16:00+07:00"-->
<!--:category = "musings"-->

These are nice to have.

### Micro-what?
 Today I learned about microformats in HTML. Specifically, microformats2. They're a system that makes it "easy" to provide semantics while also adding class hooks.

I find it to be a nice compromise between OOCSS/BEM-type methodologies and straight-up depending on markup.

So you have h-something which defines a sort of 'element'. Inside it would be things like `p-title` which are properties of said element, like names, titles, numbers. `dt-starttime` would be a special kind of property that denotes dates for, e.g. events and publishing dates.

There is a predefined 'vocabulary' for µf2 that defines what sort of elements and properties are 'standard', but I feel it's a little limiting. It doesn't reflect too much to how the web works in 2020, plus it's still a 'draft' and the reference site is pretty outdated. No wonder why no one ever cared about µf2 (other than Mastodon), which is sad because the other approaches seem messier. Schema.org wants me to add useless bloat, for example.

The vocabulary can be a little archaic and unflexible, but hey, it works.
They apparently allow vendor extensions to be a thing, so I'll just use `h-zumi-exhibition` for a gallery or something.

The only way I know µf2 is still used in 2020 is the pronoun tag drafts. Bit of a relief, I guess.

The main selling point for me is that µf2 can effortlessly turn a website into an API. Here's an example... Code for a project:
<code><pre>
&lt;div class="h-x-card"&gt;&lt;!-- card --&gt;
            &lt;time class="dt-published" datetime="2020-01-18"&gt;2020-01-18&lt;/time&gt;
            &lt;h3 class="p-name"&gt;Summary of Physics 102&lt;/h3&gt;
            &lt;ul class="x-info"&gt;
              &lt;li&gt;&lt;a class="p-author" href="#"&gt;Varia121&lt;/a&gt;&lt;/li&gt;
              &lt;li class="upvotes-info"&gt;&lt;a class="upvote-actions" href="#"&gt;▲&lt;/a&gt;&lt;span class="p-upvotes"&gt;7349&lt;/span&gt;&lt;/li&gt;
            &lt;/ul&gt;
          &lt;/div&gt;
          &lt;div class="h-x-card"&gt;&lt;!-- card --&gt;
            &lt;time class="dt-published" datetime="2020-01-25"&gt;2020-01-25&lt;/time&gt;
            &lt;h3 class="p-name"&gt;Study notes!&lt;/h3&gt;
            &lt;ul class="x-info"&gt;
              &lt;li&gt;&lt;a class="p-author" href="#"&gt;AriH&lt;/a&gt;&lt;/li&gt;
              &lt;li class="upvotes-info"&gt;&lt;a class="upvote-actions" href="#"&gt;▲&lt;/a&gt;&lt;span class="p-upvotes"&gt;2304&lt;/span&gt;&lt;/li&gt;
            &lt;/ul&gt;
          &lt;/div&gt;
</pre></code>
These define 2 article previews that only contain basic article metadata. Pass it through a [parser](https://go.microformats.io/), and...
<code><pre>
  "items": [
    {
      "type": [
        "h-x-card"
      ],
      "properties": {
        "author": [
          "Varia121"
        ],
        "name": [
          "Summary of Physics 102"
        ],
        "published": [
          "2020-01-18"
        ],
        "upvotes": [
          "7349"
        ]
      }
    },
    {
      "type": [
        "h-x-card"
      ],
      "properties": {
        "author": [
          "AriH"
        ],
        "name": [
          "Study notes!"
        ],
        "published": [
          "2020-01-25"
        ],
        "upvotes": [
          "2304"
        ]
      }
    }
  ],
  "rels": {},
  "rel-urls": {}
}
</pre></code>
Little unwieldy, but it could work... And yes, even official examples use lists with one element. I may not have been using this "microformats" as intended... Google only seems to recognize µf1: https://search.google.com/structured-data/testing-tool.

Microformats are useful for, e.g. pulling contact info and map info. In which case I can call mine a "µf2-styled" markup with none of the extra microformat perks. Though I suppose µf2 is a draft-spec, so no wonder Google can't support it now.
