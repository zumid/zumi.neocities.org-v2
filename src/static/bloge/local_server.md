<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title =         "Using a phone as a local server, and how it drove me insane" -->
<!--:description =   "Or, just another case of users not being able to do what they want." -->
<!--:longdate =      "2022-10-25T00:04:13+07:00" -->
<!--:lastedited =    "2022-10-25T00:04:13+07:00" -->
<!--:category =      "rants" -->

I work with multiple devices, and I need to work on something using any of those devices at any one time. Sure, I could copy the files, but how do I make sure that any change I make won't conflict with one another? For most people, this is a solved problem. They can sync up anything just by using Google Drive or version control systems or something. In fact, [who needs file systems](https://www.theverge.com/22684730/students-file-folder-directory-structure-education-gen-z) when you've got the Cloud&trade;, and it's very convenient?

But&hellip; what happens if I don't want to use third party services? Maybe I have some something that's more "private" that I don't wanna risk getting leaked out because it's on the Internet, even if vendors say that they have the strongest locks ever totally you guys, pinky-promise.

Sure, I can do it offline, and I have, via a USB flash drive. But the thing with them is that it doesn't "feel quite at home" having my projects all stored in there, and since they're so small it might be easy to lose them. And that's when I thought of self-hosting.

Self-hosting is kind of a noble thing to aspire for, isn't it? The idea of being on your own two feet and all that. Well&hellip; it gets pretty expensive, especially if you want to make sure that everything is *yours*. A decent VPS that lets me do *things* is not quite in my budget, and so would a Raspberry Pi. And even then, there would be concerns with both: A VPS I'd still rent from someone else (i.e. I'm still trusting the vendor with my data), and going with a Raspi server from my home Wi-Fi isn't guaranteed to be reliable anyway, in terms of accessing it from outside.

Given that, I'd thought I'd try some other compromise&hellip; using my phone as a server. I didn't think it would be a walk in the park, but I didn't really expect to jump through so many hoops as well. So the thing is that this is not a "public" file server, so it might be alright to just host something on a local Wi-Fi. The mobile hotspot feature seems fitting here.

### Let's try self-hosting on a phone!

There are some apps on my phone which do provide some sort of *server* functionality, like Amaze which has the ability to start an FTP server. But syncing up project files through FTP isn't very convenient and feels clunky. Plus, I might need to set up a server both on my computer and on my phone, which might not always be possible.

And then I thought of Git. I knew that it can basically do retrieve-and-sync from some upstream, and that anything can set up a Git remote that everyone can pull and push to, so why not try just that? I have Termux installed, which lets me have a very Unix-like environment within Android, and which has a wide variety of packages, among them Git (and SSH, which will be used for authentication).

On the phone, installing those works as you'd expect. Create a new directory, run `git init --bare` on it, set up SSH and starting `sshd`. So far so good.

### What can possibly go wrong?

What to do next should be straightforward&hellip; on a computer.

But, because it's a phone, of course it's gonna be a trial by fire because of it wanting to lock everything down. For one, look at how the home directory is represented:

```
/data/data/com.termux/files/home
```

Since this is where everything accessible is under, I have to type this whole thing everytime I do something via SSH that has something to do with files in the home directory. Which, speaking of, is the place I dumped git-bare folders on. Just to add to that, they use a different port for SSH. Not 22, but **80**22. So that's another thing to remember: to use `-P 8022` and explicitly specify that port number in Git URLs.

Right, so I've generated my key and copied it over to my phone, minding that port number. But then... how do I clone it? The thing is, I need to know what my phone's IP was when devices are connected to its hotspot. Running a few localhost services myself, I found that out initially by accessing my computer's local IP address and then checking the site access logs. Quite roundabout, but gets the job done.

Once I found out my phone's IP address, I could then run a `git clone` from that IP, followed by the port number, followed by that annoying directory name and then the git repo. After that, I could just push and pull to my git repo on my phone, so long as I'm connected to its hotspot. Fortunately, it doesn't change every time I deactivate the hotspot.

### Right?

But unfortunately, [it will](https://forum.xda-developers.com/t/how-do-i-assign-a-permanent-static-ip-address-to-hotspot-in-android-10.4037021/) whenever I restart the phone. Another way of illustrating how phones (especially in Android) lock people down more and more each passing year. Obviously, this means that it might not be a good long term solution, but I'll want to force this, whatever it takes.

It was at this point that I remembered to use the `ipconfig` command to check what my hotspot's IP address is, and then just use it in the git remote configs. But then I have the problem of using raw IP addresses on disparate git folders, in disparate machines. So then if my phone restarts, I'd have to change all of these addresses.

And then, I remembered another trick: using the `/etc/hosts` file to specify my phone's IP address and "alias" it into some readable "domain name". Well, not exactly in the sense of `something.com`, but more along the lines of `myphone`. Since I often dualboot, this configuration needs to be synced manually across all machines *twice*.

All of this works, of course, in the hackiest possible way. It feels like quite an event whenever I `push` or `pull` from the git repo hosted on my phone, but other than the hellish maintenance&hellip; it works? Kind of? I feel like&hellip; if anything, it goes to show how much more limited phones are by design, unless you root it or jailbreak it (which comes with its own risks but hey).

### If only&hellip;

It doesn't have to be this way, right? Can users be trusted to do *anything*? I know, security risks and how unlikely your average Joe will hack around to make his devices do what he wants by any means possible&hellip; but, if you declare [the PC dead](https://www.zdnet.com/article/the-pc-was-supposed-to-die-a-decade-ago-instead-this-happened/), why replace it with something so locked down that you can't run a serious workflow in it? And why'd you then rebuild the PC, but with this [exact same pattern](https://www.google.com/chromebook/)? And even then, no one likes it, even forcing [better options](https://galliumos.org/) on it.

> Ask your local gamer if they can install 8 high end video cards on a cell phone. Ask your local manufacturer if they can run their industrial supply line off of an iPad. Ask a movie junky if they can put their 60 terabyte RAID array in an e-Reader.
>
> Some idiots realized all they ever used a computer for was looking at pictures of cats and random clips from "Family Guy", and replaced it with a mobile device. That does not, by any stretch of the imagination, mean that the PC is dead.
>
> &mdash; [Nathan Lineback](http://toastytech.com/guis/win84.html)

I used to think Android was the "freer" platform, only because it had Bluetooth on it (I hated how "exclusive" AirDrop is and how everything is opaque in iOS) and that it's "open-source". But what I have aren't those decidedly open-source versions. Despite rooting being piss-easy, I'm still scared that I could brick something. I'm using the same stuff pretty much everyone else uses.

To be fair, at least some vendors still let you see your device file system in Android instead of throwing everything into some weird cloud and pretend that's all you need to know. I've had a hard time helping someone move arbitrary files in iOS. I could segue into how Apple ruined everything for everyone and singing praises for any GNU/Linux phone (Purism, Pinephone, etc.) and all that&hellip; but I digress.

&hellip;In the end however, after all that, I just said fuck it and just used [Syncthing](https://syncthing.net/) instead. :p
