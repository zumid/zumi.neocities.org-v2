<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="(Moved) Oh boy. An actual domain?"-->

<!--:longdate="2020-03-17T18:00:00+07:00"-->
<!--:lastedited="2020-03-17T18:00:00+07:00"-->
<!--:category = "site-updates"-->

Yeah, it took me this long to buy one.

So one of the things I really wanted to do is to own a website for me and my friends to play around in. This
idea popped up a long time ago, when we were still dumping stuff in semi-private wikis. The wiki platform
we were using at the time started to show its true colors, slowly being overrun with heavy ads, autoplaying
videos and endless recommendations. Customization is starting to get limited, with wiki skins being
removed and user preferences being dulled down. I was the most angry about the gimped customization possibilities,
since it struck what I nerded and practiced the most - hacking around and seeing what 
kinds of fancy stuff can be
put into a wiki page. It was at this point that I started thinking about self-hosting
everything, but I had neither the money nor the courage to take action.

I daydreamed about adminning
a sort of "space" like how I was trusted to admin a wiki because friend status or whatever. It was not so
much moderating a wiki as it was to make it so goddamn fancy your eyes will literally sparkle. Back in
the old wiki days, I was obsessed with finding every system message page, stylesheets, and scripts and
modifying the ones I could see affected in plain sight. Those admin privileges let me make the modifications I think
looks good, and doing a bunch of experiments. There was a few friends following my steps out of curiosity, but the others just went "Yeah sure, do
whatever."

I remember writing down domain names for me to eventually buy one of them, sketching out website layouts,
subfolder configurations...  I looked into various free services.
None of them seem fitting since there are always strings attached, which... yeah, that's to be expected.
Discussed a bunch with my friends and laid out my ambitious plan to... just buy a domain! And that went
on for a while, while never actually doing it. **Until now.** I can't believe it's been that long. Why?
I haven't earned my own source of income yet. (Still haven't, hope that changes) I've only gained that
courage because my savings are enough to buy a one year plan, and then there's still some left for other things.

I had one domain name I thought of while daydreaming all this, but it's not this one. It's connected to one of my former
friends' intellectual property, and I thought I shouldn't touch on that. I ought to change that, but what to?

I was out of ideas. But the hosting service I went to, Hostinger, has a free xyz domain bundled with basic hosting, and I thought of an old thing that was posted while
I was kicked out for being a meanie (putting it lightly), and I felt was directed to me - [XYZ](https://www.youtube.com/watch?v=LEXYGOV4_gQ).
Kind of related to SNSD being our inside thing because one of my friends was a fan of the group. "ABC & XYZ, sarajyeo, sarajyeo". "get lost" -
I felt that. So! Sarajyeo.xyz it is. Sounds scammy. But whatever.

In the meantime, I made websites for myself. The first of which I hosted on a free web host. Got shut down because of a lack of visits. It was
a really crappy site made in SeaMonkey Composer (the son of Netscape Composer). That thing runs HTML 4.01, and has images **as text data!**
Man that was a nightmare. Next one was a bit like [this page](https://zumi.neocities.org/index_old.html) with links to some of my other stuff.
RIP to that as well. Then I found Neocities. Extremely intuitive. No frills. I've been managing [my site](https://zumi.neocities.org) over there since. It's been going on for like 5 years at this point.

Thing is, that's a static website running purely on files. I can't provide dynamic stuff for friends like blogs or wikis there. There's MDWiki, but only I can add to, edit and remove from it. It made it necessary for me to come up with a [script to make my entire website](https://github.com/ZoomTen/MDiocre) so that when I change something on it, I won't have to go through every page or risk having the pages look all different. Which, on the plus side, made me practice some coding.

In conclusion, I bought this for me and friends. I hope I can manage.
