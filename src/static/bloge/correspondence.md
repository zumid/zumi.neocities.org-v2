<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title =         "An update on correspondence" -->
<!--:description =   "Boy. I *really* need to check my e-mail." -->
<!--:longdate =      "2022-04-14T18:38:17+07:00" -->
<!--:lastedited =      "2022-04-14T18:38:17+07:00" -->
<!--:category =      "personal" -->

I gotta admit, I haven't checked my e-mail all that much, despite it literally being smack-dab at my front page. Mostly because it's bogged down with default provider e-mails that I was too lazy to change the settings for, and partly because my choice of e-mail provider isn't too optimal (but it works anyway).

But now I've cleaned that up and honestly I didn't really expect anyone to actually send me e-mails! And actually commenting on my crappy blogposts! It means a bunch to me, and I think it's motivating me to keep on maintaining this space. (It also tells me that this approach of writing down my e-mail address by hand as an image works pretty well to deter spammers)

Since my e-mail provider is offering a new e-mail domain, I'd thought that I might as well use it. Hence, I've now updated my e-mail address to reflect that, and hopefully make it easier to search my inbox through :)

Some of you asked for things I did on my YouTube channel through my e-mail, and while e-mail is all well and good I think there's a bit of a higher chance that I'll respond if you leave it in the video you're asking about. It sucks that I need a certain amount of subscribers to even have a comments tab on my channel page (wink wink nudge nudge) but what can you do with practically a monopoly, lmao.
