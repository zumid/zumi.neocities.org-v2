<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "Commenting is a thing apparently" -->

<!--:longdate    = "2019-09-18T22:03:05+07:00" -->
<!--:lastedited  = "2019-09-18T22:25:48+07:00" -->
<!--:category    = "site-updates" -->

So I found a commenting system similar to Disqus.. only less corporate-y and more iframe-y. The most Web 2.0ish name ever: Comntr.

Because of Chrome's security policies, this may not work in it. [Alternatively...](../..)

**Additional heads up** - the captcha won't work unless you add comntr.live to your security exceptions (outdated certificate, apparently)
