<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Themes, part II"-->
<!--:description="I try to fix the theme switcher. I might as well add some new themes just for the hell of it."-->
<!--:longdate="2022-01-25T22:18:17+07:00"-->
<!--:lastedited="2022-01-25T22:18:17+07:00"-->
<!--:category = "site-updates"-->

Continuing from [a previous post](themes_etc.html), I've made some adjustments. There was still some *very* obvious <abbr name="Flash of unstyled content">FOUC</abbr>. My initial implementation was naive, adding the stylesheet tag at the same time as the page loading, and inlining it didn't actually do anything. Initially I suspected that was because the stylesheet was inserted on the page load event, but that wasn't really the case as the stylesheet doesn't show up until it's 100% loaded. So I decided to look for inspiration in Sckewi's [Static Ultra](https://sckewi.neocities.org/static-ultra/).

### Dealing with FOUC. Again.

<figure>
	<img src="img/perftabtest.png" alt="Chrome devtools Performance tab.">
	<figcaption>
		Look at that paint order.
	</figcaption>
</figure>

Static Ultra uses preloaded assets to ensure smooth theme switching. After asking Sckewi himself for some executive summary, I set out to work on a new implementation. Instead of being inlined, this would be a new file entirely. A raw script tag is still yet placed on the page to provide the relative URL that points to the root of the website (in an attempt to make the site also fit to be opened using file:// URLs, and since a couple of templates spawn from one HTML template).

The way SU does it is: on first load, the script inserts a style tag containing a "hider", which functions as a sort of "curtain" for the content, making it invisible. Only when the content is loaded would then it "open the curtains" by removing the style tag. The stylesheet itself, as mentioned, is preloaded. Static Ultra has a <abbr title="XMLHttpRequest">XHR</abbr> fallback when the `preload` attribute isn't supported, which I didn't implement. I also didn't implement cache-busting (the whole timestamp thing at the end of the URL that serves no other purpose).

My first attempt at implementing it resulted in the Neocities thumbnailer failing to render my site. As it turns out, I simply haven't completed the fallback for the unsupported `preload` attribute. Through a bit of digging, [Neocities' thumbnailer](https://github.com/neocities/screenjesus/), uses [SlimerJS](https://slimerjs.org/) as its' visual user agent. As stated on its website, the library only supports up to Firefox 59. I tested my site in that specific version of Firefox and yes, it does check out &mdash; no content. `preload` was not officially supported in Firefox [until version 85](https://caniuse.com/?search=preload).

The problem was solved by wrapping the lines that add the "hider" style inside a function, which is what SU already does and should've implemented from the start, lmao. With that finally working on FF59, content is actually showing up again in the Neocities thumbnailer.

### More Theme Support

Up until now, there had been 2 themes made. The original "web v7" theme for the site, and the Honeycomb theme I detailed in my previous post. The theme options were previously baked into the template. It was soon apparent that this is not very optimal since under this condition, every time I try to add more, I would have to update *every page of my site*. So I simply just put the theme list inside a json file: `/themes.json`, which would simply point to the CSS files and write their respective titles in the selection box. I then had them load inside the theme selector once the page loads using a XHR.

The result is a more scalable (gotta use that word!) and flexible system, and to celebrate I made two more themes: the Android 4.x Holo theme mentioned in the previous post, and a Geocities theme.

I definitely experimented with the iframe-like look of the sidebar for the Geocities theme, despite not actually having any iframes to boot, and I think I did a pretty good job here.

<figure>
	<img src="img/theme_holo2.png" alt="My website with the Android 4.x Holo theme.">
	<img src="img/theme_geocities.png" alt="My website with the Geocities theme.">
	<figcaption>
		The new themes added in today's update
	</figcaption>
</figure>
