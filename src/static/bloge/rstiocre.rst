:mdiocre:`mdiocre-template = "../../built/template/blog_post.html"`

:mdiocre:`title       = "MDiocre 3.1 (a.k.a RSTiocre)"`
:mdiocre:`description = "MDiocre 3.1 beats 3.0 out of the water with GUI and ReStructuredText support."`
:mdiocre:`longdate    = "2020-09-13T17:19:35+07:00"`
:mdiocre:`lastedited  = "2020-09-13T17:39:58+07:00"`
:mdiocre:`category    = "tools"`

College starts tomorrow (yes, online), so I'm taking this chance to update and
release version 3.1 of my static site generator. Not many changes from the user
side, however improvements have been made under the hood. These under-the-hood
changes are what made it possible for me to finally implement `ReStructuredText <https://en.wikipedia.org/wiki/ReStructuredText>`_
support, something I've kinda planned for a long time. In fact, this page is
written in ReStructuredText instead of Markdown!

Starting in this version, there's a GUI you can use (just run
``python mdiocre_gui.py``). It runs on Tk, which is pretty much included with
every desktop Python install. I would have used PyQt, but that's way too much
overhead for such a simple tool. And yeah, it runs terribly right now, locking
when you click on "Process" until it finishes. It's pretty slow building this
website using the GUI, but I want to put threading on it (a concept I barely
understand right now lol)

I've also wanted to build index page generation functionality in MDiocre, but
seeing how 2.0 went, I decided it would be Python scripts for now, instead of
something accessible from the CLI. Now that I think about it... I might consider
making that functionality accessible from the GUI. Maybe later, since I barely
have enough time to write up big features lmao. Not only is there an index page
generator, I also threw in the feed generator for my site as well.

Tags, that's another one. While tags isn't "officially" supported in MDiocre, the
``samples/make_tags.py`` script should make up for it. In fact, if you plan to
work with that script, just know that `tags` are an ordinary MDiocre variable.
It should contain space-seperated tags, and the script will automatically make
pages for it. However, the pages themselves won't be modified with tag links...
should I do that?

Anyway, I plan to add the features I added to the scripts, to here. I'm talking
split up index pages, tag lists... Maybe adding a "tag index" page?

Anyway, that's it I guess. I'll update the `docs here <../mdiocre3doc/>`_ later,
but for now you can check out what's on the `repo <http://github.com/zoomten/mdiocre>`_.
