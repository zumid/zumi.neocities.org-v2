<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "Pleroma" -->

<!--:longdate    = "2019-05-21T10:46:59+07:00" -->
<!--:lastedited  = "2019-06-17T15:44:20+07:00" -->
<!--:category    = "personal" -->

Experimenting with an Pleroma instance [@zzdaxuya](https://stereophonic.space/zzdaxuya). God, that name is barely memorable at first glance. It's easier for me to wrap my head around than Matrix chat, but that's probably because I only read the "how to set up a server" part of Matrix.

Because of Pleroma's compatibility with Mastodon, I can follow accounts that are with Mastodon (a bunch of cool linux-related accounts are there). That's cool I guess. I still need to read up on how each instance relates to one another, but I assume that each instance keeps its' own messages. Can I export my statuses/toots/whatever to another instance? I might have to read up on it.

Not sure why I bother with this when the instance I'm in allows a 500k character limit, but I guess I need to justify updating my sidebar.

Because static site, I have to update pretty much every page everytime I add something to the sidebar. I have taken the Javascript approach before (by having a script to include some HTML in the page), but it makes the Wayback Machine crawls to my site look like shit.

Hope you don't mind me silent-editing my previous posts since I'm too lazy to add a "last-edited" field on all of them.

*And yes I know that instance is full of anime nsfw, don't bug me about it I don't post any*
