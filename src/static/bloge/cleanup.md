<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "Cleanup time" -->

<!--:longdate    = "2019-06-21T00:48:20+07:00" -->
<!--:lastedited  = "2019-06-21T00:53:22+07:00" -->
<!--:category    = "site-updates" -->

So. Fueled by cofe, I decided to do a cleanup on my website's directory and sync it up with my personal git repo. I moved all the stuff in `pages/` to... well... `stuff/`, and made it use its own images directory. Cleaned up some unused stuff except the ones that I remember being linked to publicly. After all I shouldn't just use Neocities as Dropbox Parte Tres.

Cleaned up the links in my pages so that it uses relative linking instead. So in the event that I decide to buy a domain and server/host for myself, hopefully I could just simply move everything without much trouble. Er, I probably should just try this with my own machine using `httpd`. Maybe later.

Made my stylesheet use Sass as well, and although I can see some epic potential for this thing, I'm just not using it enough yet. With Sass being a legit excuse, I'll find the time to open the repo soon for whoever's interested. If you choose to stroll through the commits, you'll probably have fun looking at some embarrassing shit. I think I simply need a moment of acceptance.

By the way, I found out that my [Bootstrapped](../bootstrapped/) page takes almost 20mb of your data. Exactly according to plan. That being said, I wouldn't advise opening that shit with mobile data. Or on a crappy computer. It might kill both. Such is modern design.

Reminds me. I should check how much my page weighs...

Hm. According to Chrome the index page of my website weighs about 8% of what Google's search page loads. Nice. Now I just need to make it more [accessible](https://twitter.com/kentcdodds/status/1083073242330361856).

Found a "bug" in MDiocre. Maybe I should add a "list subdirectories' files" option in it. Right now the MDiocre doc thing is displaying in the index. Sticking this as a self-reminder of sorts.
