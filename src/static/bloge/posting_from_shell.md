<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Posting from Shell"-->

<!--:longdate="2018-10-21T08:37:00+07:00"-->
<!--:lastedited="2019-05-20T00:00:00+07:00"-->
<!--:category = "site-updates"-->

Well, this should be fun. How about making a blog post through a shell script? Well, it's not entirely shell I gotta give it that but, it's like WYSIWYG editors, except it's definitely not WYSIWYG. Instead it's Markdown, if you've ever used Reddit or Discord you should be familiar with it.

The way this works is by making a temporary file which will then have the title and date automatically added to it. Should I automatically invoke the update_blogs script from here? It could be good I think.

Stay tuned for the scripts, they might be posted to my GitHub

**EDIT 2019-05-20**: Sike, it's on [GitLab](https://gitlab.com/zumid/zumisite-oldtools). It's a kludge and a mess; you have been warned. If you like pain and want to use it anyway, make sure to edit the default stuff.
