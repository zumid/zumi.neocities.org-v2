<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="Random things I think about lately"-->
<!--:description="Incoherent ramblings of shit I barely know anything about and have but zero non-anecdotal evidence."-->
<!--:longdate="2025-01-29T20:30:25+07:00"-->
<!--:lastedited="2025-01-29T20:30:25+07:00"-->
<!--:category = "musings"-->

As I find more things to bitch about, this may expand. Couldn't be a Twitter thread because you can't see it anonymously without a proxy now.

### Communities

> Something must give when you join a "community".

I've had to leave my own principles at the door when joining a company, so that I may serve the company's principles. In that same way, some part of me must change when I'm in a community or a clique (let's be frank). Whether it be sooner or later. When I spend a lot of time with a community (especially if I find myself most comfortable in it), it can be quite a challenge to "be myself" again.

Something as simple as password hashing. At my job, my boss wanted me to use standard hashing algorithms on the basis that they are already well-proven cryptographically. Things like argon2 aren't needed, he says, SHA2 and up are already enough. Even when the algos are *actually* designed for passwords. At home, I made myself think twice before using argon2, despite being the *thing it was made for*.

It's probably because of wanting to avoid the "personality A with friend group B" problem. The less there is to remember the easier it will be for me. The horrifying part about it is that this can be socially engineered to steer people towards a certain personality and outlook, whether actually beneficial or not.

The one thing I hate about its logical conclusion is that I see a lot of people, in a bid to become "unique", actually be the opposite—they are *carbon copies* of each other. I don't mean that in the usual "oh everyone are sheeple and I'm the only enlightened one" but no, they are *literally* predictable. And it is so frustrating. I really do not want to lump people into stereotypes but here they are *actively* becoming the stereotype. I know some of why that is, but it still perplexes me.

### Art

> *Good* art has the ability to successfully evoke—in others—the feeling, impression, or emotion that you want.

From what I've observed, it seems to fall within how successfully someone can imitate a certain style or aesthetic. Even when talking about what "traditionally" counts as art—you know, nature, anatomy, portraits and the like. In those cases you're considered "good" when you make something that to others are perfectly life-like. Exceptional, if your art is practically indistinguishable from a photograph.

In my neck of the woods, that's chiptune… Some people make chiptune that could pass off as a genuine chart-topper. No, the things that literally sound like Skrillex circa 2012 despite being made on an actual microwave. THAT kind of good. On another—retro design—some people make stuff perfectly embodying an era with none of the things that can give away that it's made today with rose-tinted glasses.

The people that I know who do this kinda thing do it *consistently*, I guess that's why everyone knows them. I admire them greatly, but also secretly jealous. The kind of drive needed for me to up my craft, even if I beat myself up constantly for not meeting their standards. In fact, to me they are unapproachable even when they otherwise aren't, or aren't meaning to be. I think to myself, "They must be inundated with utter peasants that I wouldn't even be worth their time", even when they aren't. Better safe than sorry.
