<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "TFW your retro site don't work in Netscape" -->

<!--:longdate    = "2019-06-18T11:04:29+07:00" -->
<!--:lastedited  = "2019-07-27T00:48:45+07:00" -->
<!--:category    = "musings" -->

With a name like Neocities, sure enough everyone's treating it like Geocities - and in exactly the same way, too. Web 1.0 emulation and HTML 4.01 galore, this entire site is full to the brim with late 90s and early 2000s nostalgia.

Despite all the ***ａｅｓｔｈｅｔｉｃ*** that everyone has designed that evokes a yearning for a time gone by and captures its essence, it sort of falls flat in its face when you can't even open it using very old browsers.

Even if you made your site as compatible as possible - even designing it *with* a very, very old browser in mind - it still can't be accessed with it. It's not the fault of the HTML, mind you, it's just the server that's too new.

<figure class="figure-left figure-size-2">
<img src="img/ie4-https-error.png" alt="IE4 errors out.">
<figcaption>Trying to run shit on IE4 go bye-bye.</figcaption>
</figure>

I have actually designed [this page](../../page/ancient.html) with Netscape Navigator 4.x in mind, but the browser itself cannot open the live version of it. Not to mention I'm kind of violating HTML 4.01 by having an `<audio>` tag because modern browsers.

You see, Neocities uses HTTPS and if you visit the HTTP site it will automatically redirect you to the HTTPS site. HTTPS employs something called <span title="Scrambling your data which makes it unreadable by anyone except you and Neocities." style="border-bottom:1px dotted black">"ciphers"</span> to encrypt connections, and since ciphers can be cracked at anytime, the cipher calculations (or algorithms) need to be updated every now and then.

Browsers that do not support HTTPS are just out of the question, while problems arise with browsers that have basic support for it.



<figure class="figure-right figure-size-2">
<img src="img/netscape-https-error.png" alt="Netscape errors out.">
<figcaption>Netscape go kaboom.</figcaption>
</figure>

A given browser is only programmed with a certain set of cipher methods it can decrypt. If a cipher that the browser has is not supported anymore, then that browser can't open the site. They can't read this newer and more secure cipher scheme because, well, they're old.

Seems like a bummer, but HTTPS is pretty necessary these days.

Neocities has adopted this policy [for two years now](https://blog.neocities.org/default-ssl.html), and won't be backing down soon. Progress, yay.

Here is the entire list of supported ciphers as of this writing, straight off of nmap:

```
PORT    STATE SERVICE
443/tcp open  https
| ssl-enum-ciphers: 
|   TLSv1.0: 
|     ciphers: 
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - A
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA (rsa 2048) - A
|     compressors: 
|       NULL
|     cipher preference: server
|   TLSv1.1: 
|     ciphers: 
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - A
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA (rsa 2048) - A
|     compressors: 
|       NULL
|     cipher preference: server
|   TLSv1.2: 
|     ciphers: 
|       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_AES_256_GCM_SHA384 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_GCM_SHA256 (dh 2048) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA256 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA256 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - A
|       TLS_RSA_WITH_AES_256_GCM_SHA384 (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_GCM_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
|       TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256 (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_CHACHA20_POLY1305_SHA256 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_CCM_8 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_CCM (dh 2048) - A
|       TLS_ECDHE_RSA_WITH_ARIA_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_ARIA_256_GCM_SHA384 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_CCM_8 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_CCM (dh 2048) - A
|       TLS_ECDHE_RSA_WITH_ARIA_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_ARIA_128_GCM_SHA256 (dh 2048) - A
|       TLS_ECDHE_RSA_WITH_CAMELLIA_256_CBC_SHA384 (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA256 (dh 2048) - A
|       TLS_ECDHE_RSA_WITH_CAMELLIA_128_CBC_SHA256 (ecdh_x25519) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA256 (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - A
|       TLS_RSA_WITH_AES_256_CCM_8 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CCM (rsa 2048) - A
|       TLS_RSA_WITH_ARIA_256_GCM_SHA384 (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CCM_8 (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CCM (rsa 2048) - A
|       TLS_RSA_WITH_ARIA_128_GCM_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA (rsa 2048) - A
|     compressors: 
|       NULL
|     cipher preference: server
|_  least strength: A
```

I tested Neocities with a couple of ancient browsers, and here was the verdict:
 
 * Netscape Navigator 3.0 (1996):
   nope, no common encryption algorithms. Even if some of them are mentioned in its `about:` screen, the ones that Neocities uses are all too damn new by comparison.
 
 * Netscape Navigator 4.0 (1997):
   still nope
 
 * IE4 on Windows 98 (1998):
   nope , also bluescreened a number of times.
 
 * Netscape 6.0 (2000):
   won't even pop an error message
 
 * Netscape Navigator 4.8 (2002):
   zilch 
 
 * Firefox 1.0 (2004):
   complains of a broken certificate.
 
 * Firefox 1.5 (2005):
   **works**, supports RSA+AES256.
 
 * Netscape 9.0 (2007):
   Of course it works, it's basically a reskinned Firefox 2.0.
 
 * IE8 on Windows XP (2009):
   throws up a generic error page.
 
 * IE11 on Windows 7 (2013):
   works 

Basically, Neocities only works with browsers made ≥ 2005. Sorry guys :(

That being said, I found a few workarounds that might be of interest. [The Web Rendering Proxy](https://github.com/tenox7/wrp), that turns webpages into clickable imagemaps. It works, but since you're essentially looking at a screenshot of your website (with clickable maps) it won't display any fancy animation. To be fair I was using the rewritten beta version, but I'm sure it'll do more or less the same using the older python2 version.

[HTTP10Proxy](https://www.jwz.org/hacks/#http10proxy) is a thing but I can't seem to get that to work, maybe it'll actually run for you?

Easiest way would just be to download an entire website onto a folder and then run index.html from an old browser of choice, but that kind of takes the fun away and it wouldn't be the same. Oh well.

<figure class="figure-size-5">
<img src="img/oldbrowser-wrp.png" alt="Netscape + WRP.">
<figcaption>Fun but not authentic.</figcaption>
</figure>

<figure class="figure-size-5">
<img src="img/oldbrowser-actual.png" alt="Netscape + actual files.">
<figcaption>Authentic but not fun.</figcaption>
</figure>

Here's another cool trick. If you've got Python 3 already on your `PATH`, you can try going into some directory and opening up a command prompt there. Execute `python -m http.server 8088`. That will run a server from your machine on port 8088.

Then go in your VM's Internet Explorer or whatever, and type in `http://`, whatever local address (192.xx, 10.xx, etc.) you have. Google "view local ip address" or something.. Or try [this](https://www.whatismybrowser.com/detect/what-is-my-local-ip-address). Put `:8088` after putting your local stuff on IE's URL bar.

You can even use it as a cheapskate way of moving files to your VM without setting up shared folders. The other way around though, you'd have to run a simple FTP server instead of a HTTP one. And set up a FTP client on your VM. \*shrug\*
