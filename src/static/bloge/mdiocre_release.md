<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title       = "MDiocre Release" -->

<!--:longdate    = "2019-03-07T00:00:00+00:00" -->
<!--:lastedited  = "2020-04-22T17:47:13+07:00" -->
<!--:category    = "tools" -->

> You can tell it's so bad, it has nothing to hide.
>
> -- AVGN

[MDiocre](https://github.com/zoomten/MDiocre) released. It’s still quite messy and it hasn’t got a README for now, but eh

### Quick setup

This is the entire doc for now, I had to put the other one on hold because damn, I don’t even have time to work on this right now what the f

### Prerequisites

1. Make a folder to put all your stuff in, let’s say it’s named `site-stuff`.
2. Click `Clone or download`, then click `Download ZIP`.
3. Move the folder `MDiocre-master` into `site-stuff` (or whatever you called it), and rename it to `MDiocre`.

### Make a site

See [here](../stuff/mdiocre/), assuming the current directory is `site-stuff` or whatever you’ve named your folder.

Excluding code literals is now on my to-do list, because at the moment, when I pasted all the doc code here, it picks up the variables from my actual setup. That ain’t good. So I copied the html from the compiled documentation, so sorry if it looks a bit basic.

Also, if you’re on Windows you may need to make a batch file with the command (prefixed with `python`, maybe) and run that instead. I’ll work on that sometime - it works alright on Linux for now.
