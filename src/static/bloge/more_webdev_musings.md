<!--:mdiocre-template="../../built/template/blog_post.html"-->

<!--:title="The abyss that is web dev..."-->
<!--:description="If you stare into web devving, it stares back at you. So why even try?"-->
<!--:longdate="2021-09-28T20:15:00+07:00"-->
<!--:lastedited="2021-09-28T20:15:00+07:00"-->
<!--:category = "rants"-->

So recently I've had the pleasure to rework a friend's site, if not for making a case study for [MDiocre](../stuff/mdiocre). And this gave me time to re-read random articles about web design, practice considerations, and basically ponder about webdev, again. Even if this is for designing a static website, which is more involved than letting frameworks do the work as people usually do.

Now I could've just split this in multiple articles but idk.

### Zzz...
Making websites are srs bsns, yet at the same time it's not. HTML is very permissive, and it seems like a result of many, many compromises. When the WWW was growing, and people made their own sites, the web became the new hip thing to latch on, growing at an exponential rate. Eventually, you have the first browser wars. So many vendor extensions and incompatibilities. It was a much bigger thing than what TBL ever thought it would amount to (that is, papers with actual semantics).

By the late 90s, it was realized that HTML was beginning to look a bit too tied to visuals and presentation, but then there's terminals that can't really display them all that properly. Not to mention, maybe you wanted to print a page out, but the blue background would make you waste a lot of ink.

So then you start to have **separation of concerns**. HTML 4.0 dumped a lot of presentational elements (`font`) along with the presentational attributes (`color`, `valign`, `bgcolor`, etc.), and moved it to a new Thing known as CSS. The whole point of that was that you can set a style for screens, one for print, and maybe one for speech synthesizers (yes that's a thing lmao)

So now you have HTML for writing content, and CSS for writing displays. But that didn't stop the Geocitizens though, and it seemed to end during the dawn of Web 2.0 (and the hyped up Thing that is "HTML5"). Ironically enough, once *that* was in full swing, people are going back to that old Geocities method in a slightly different way - to the point that no one really realized that was going on.

### "CSS" classes

So `class` and `id` are supposed to be things you give to elements to *class*ify them as being of a certain Kind, or at least what is supposed to be if you can write new "elements". Which is why you can give multiple elements the same `class`, like `class="infobox"`. Unfortunately people don't see them as general classes but rather **CSS classes**.

As a result of this, people started to use classes as a way of saying what things should look like. Consider this "modern-looking" markup:
```
<span class="tc-red bg-light ff-sans fs-2">Hello World</span>
```
Dunno about you but it might as well be written:
```
<font color="red" bgcolor="white" face="sans-serif" size="+2">Hello World</font>
```
Or maybe just:
```
<span style="color: red; background: white; font-face: sans-serif; font-size: 2em">Hello World</span>
```
Sure, the "modern-looking" way would at least follow your brand rules, but maybe if the site structure actually *cascades* you probably wouldn't need to do that. It's quick to just do that instead of figuring out relations between elements and reflect that in your separate stylesheet, but if I could speculate on a reason why everyone's using this kinda markup and "utility-first" classes - it's useful when "componentizing" elements, especially when using a framework. When you build a component, you would probably want to describe what it looks like too. And then you can just plop it in your HTML. Things like this are easy feedback, but it's not a good look production-wise.

And by "not a good look" I mean that while it may benefit me as a developer, it's at the cost of whoever's trying to look at my site. The kitchen-sink CSS would likely have not much coverage in the HTML, so it'll take a toll on the viewer's data cap. Tailor-made systems where it automatically optimizes for what is actually used (removing unneeded classes) can help a bit, but you kinda need to look at the HTML too.

### "They don't look seperated to me..."

Separation of concerns means you either have one of the following: HTML that depends on CSS, or CSS that depends on HTML. The question then becomes which would be better?

CSS is more likely cached longer than HTML pages, so having less stuff in HTML reduces data usage more. [This Google Web Fundamentals article](https://developers.google.com/web/fundamentals/performance/get-started/httpcaching-6) recommends a month for CSS cache expiry, while everything else gets 2 days.

So if I have HTML that depends on CSS, it's more likely I'd start thinking about doing things like the previous section, outmoded practices and all. If I dump everything in the HTML, people will get to load more stuff each time. Hooray for wasted data.

On the contrary, if I have CSS that depends on HTML, it's more likely that I look at my HTML, see how spaghetti it looks, and I could start thinking about optimizations. Everything else should follow and I get accessibility for free. It might be neater... but it still depends on how much I understand and plan about it.

But does all of this matter, even with [such a diverse and tantalizing garden in the distance](http://www.csszengarden.com/)? Because in practice, if you were to completely redesign your website, you'd most likely want to change the HTML, too. Unless of course, you like a good (and possibly educational) challenge.

### You Mean Graphic Design...
Some people wouldn't exactly call what's commonly understood as "web design" as "web design". As I understand the term, it would involve graphic design for the web and then actually laying it out in HTML (the front-end).

But practically, it might as well *just be graphic design*, because looking around me, I see that we don't actually care about markup as long as it doesn't get in the way of implementing graphics (See [the following section](#headings)).

Implementing the actual markup should be considered part of design, or at least, engineering. Speaking of, if they wanna call HTML a programming language, you'd think they'd act as if it was one and start optimizing lmao. However some people really do the work when they say that, props to them. To the people who call it that just to spite some "establishment"? Lol.

Some people propose this: writing content in Notepad, then slowly putting tags around it until you have a finished page. I suppose if you're a front-end dev working from website art done by your graphics department, you could dump whatever text it has and try to structure them as if it were a paper. Slowly working your way up until you can fit a sensible structure.

<h3 id="headings">What does the H in H1, H2, ... stand for?</h3>

Not "font sizes", obviously lol. I guess people picked these tags just for the sizes, even though that's not what they mean.

In any book, you would see the chapter title as a first heading, then any sections and subsections following it. If books had huge text as a decoration, I don't think you'd say that text is a heading in of itself - it doesn't describe in general anything that's below it. In the same way, "heading" tags don't really say anything about font size but rather describes the topic of whatever is below it.

Because people use it as a quick way to make huge text without adding CSS, you now have web pages with nonsensical structures. Skipping heading numbers isn't a good look in production either, and may present usability problems. But I suppose everyone knows where accessibility is in the priorities section. Maybe Twitter Theorists should get a hold of that. Oh wait, they have two rule books.

### Why would you use PX?

PX (pixel sizing) seems reasonable if all you're looking at is a precise web-shoop. Looks good on 16px defaults, but there's people who do need to adjust their font sizes. Using PX is kinda like saying "why would you change the text size just use zoom lol". Maybe people just don't wanna dive for the zoom and rather just see things clearly? So, EM and REM is considered more acceptable. So, margins, padding, line headings, widths, heights, etc. all in EM. Here's a [nice article](https://www.24a11y.com/2019/pixels-vs-relative-units-in-css-why-its-still-a-big-deal/) I found on this.

Even for responsive breakpoints I think people should use EM. Don't have to mind Hi-DPI or Retina stuff or zoom points, and works well using large fonts. But the most you see everywhere you go is PX breakpoints. So, you will see a lot of jank too.

I'd personally consider PX acceptable if it's something like `border-radius` or `border`. Or take it from this site's style where I used it for specifying a slight shine on the nav and footer bars. Since EM is tied to font size, I don't really think the effect will work well using high font sizes.

### Baseline grid

All the talk of PX reminds me of putting baseline grids on the web. That concept from print where every line is neatly spaced? Again, it kinda says that web design is mostly just graphic design now. You'd draw a grid of say, 8px, and then say all text baselines should be aligned. But we really should be talking 0.5em, considering the previous sections.

According to what I've read, the goal is to make the line heights all pad up to the grid. Easier said that done, because the self-described `line-height` isn't really what you think it is. CSS `line-height` doesn't really adjust spacing *between baselines*, but what it does is create some sort of box around the line of text of X height, and then centers the text around it.

Some solutions are really hacky, and of course relies wayyy too much on PX. Actually, even in the design phase some people [want to be PX-perfect](https://uxdesign.cc/baseline-grids-design-systems-ae23b5af8cec). They basically use either relative positioning or transform tools to try and get the text to line up. But even then things may still break on weird setups. I've made this [sample page](img/baselines.html) using nothing but EMs (and a few %), but for everything to line up, it also really depends on the font to use. It might not line up *perfectly* but it might be *enough*.

### Frameworks.

I'm not really fond of frameworks in web dev. Several reasons:
<ul>
<li>People quickly leave one once another hyped frameworks appears, so, it runs the risk of having short shelf lives. Getting used to X.io&trade;? Whoops, jobs all use Y.io&trade; so you have to learn everything all over again. What are learning curves?</li>
<li>The market is just *full* of them right now allll claiming they can change the world</li>
<li>Because of the job thing it's really tempting to associate them with the 9 to 5 meat factory corpo culture that says they're all for "efficiency" and a reliable "process"... or lack thereof</li>
<li>I don't understand half of the fancypants paradigms they like to throw in there</li>
<li>Sometimes people throw one in their project just to use one specific function</li>
<li>Sometimes people recommend doing the above (!!!)</li>
<li>I do the opposite when they tell me not to worry about the heaps and heaps of code behind whatever it is I'm doing with it</li>
<li><span aria-label="bloat"><b>b L o A t</b></span></li>
</ul>

You might say, "If you don't wanna use frameworks, why aren't you making programs in assembly rather than C or even Python?" - leaning on the whole "frameworks are just abstractions to make a job easier" thing. I'd say that when we're talking about **web dev**, the vanilla stuff *are* such abstractions. For the best performance, you'd want to use as less abstractions as possible such that you still understand what you want to do with it.

That being said, the big stuff might help if you are running a really big service or whatever, since there might be some optimizations specific to running those big rigs. But for most people that run medium-sized stuff, huge and sprawling frameworks might be overkill. Ultimately though, it's all tradeoffs, picking the best tool for the job, knowing your trade and generally having good design. Most drawbacks seem to be resulting in not using a framework properly - either due to time, inexperience or copypasta. But how often does that *not* happen? By the way here's an [extreme example](https://adamdrake.com/command-line-tools-can-be-235x-faster-than-your-hadoop-cluster.html) of a small thing running around a bigger, more impressive and more "professional" thing.

Qt is basically a framework for desktop apps. It's pretty big, but I feel alright using it. Maybe because desktop apps probably needed abstraction way more than web apps. Web stuff already has built-in GUI, and I feel it has so much overhead already that using frameworks would make it worse. Though it might just be that I have a problem with desktop paradigms getting shoved into the web, that aren't exactly a good fit.

It's like MVC - the model-view-controller pattern. That's nice for the desktop, but ultimately, MVC on the web just reduces to request - response. What's a "view"? Is it the server that's rendering the page, or are you just sticking it in the client side? You might even end up with a Controller that depends entirely on the View, or vice versa.

Oh no, don't get me started on anything related to node.js.

### TL;DR

If you stare into web devving, it stares back at you. So why even try?

