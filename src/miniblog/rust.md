<!--:date="2024-03-10T22:37:36+07:00"-->

## non-technical gripes with rust specifically

the language is pretty great from a technical standpoint. i see its benefits. i know. other languages have taken ideas from it. i use packages that emulate it. but i have not brought myself to use it, if i can help it. the borrow checker seems to be the least of my problems.

### use of latest features

every new release of rust tries to maintain compatibility with most rust code that's been written (that is—[every rust code in github](https://blog.m-ou.se/rust-standard/)).

but it's immediately nullified by everyone (including the rust-in-linux project) using nightly instead of stable, or use a *very recent* stable.

a fortnightly release schedule is also a nightmare for "stable" (read: *glacial*) projects wanting to ship rust, so the compiler shipped with those versions couldn't compile a lot of rust code because those code use the newer versions.

while i trust that backwards-compatibility is not an issue here, it's the issue of providers being expected to keep up with rust releases. which means you basically need someone to guard that post, along with the headache of trying to coordinate that with the provider's manager. supporters brush it off by saying, "get with the times, old man".

rust's [release train mechanism](https://doc.rust-lang.org/book/appendix-07-nightly-rust.html) is looking more like a hype train: the fact that "minimum supported rust version" (MSRV) is a common phrase, and that [this website](https://www.whatrustisit.com/) is a thing should tell you something about how frequently the language is updated.

some people like chasing trains like this, others don't. 

### obtuse method chaining

i'm fine with "method chaining". i use it quite often. under the condition that it remains clear-at a high level-what it's doing. what do rust libraries often do?

`something.into().to_iter()`, `foo.unwrap()`, `std::iter::IntoIterator::into_iter(baz)`…

just get to the point already! no, i don't buy that "it's not boilerplate because it makes it clear what you are doing exactly to your object," i just want to decipher the damn thing!

"this is just what you signed up for when you do systems programming, everything is laid bare unlike C where you need to keep it all in your head" except rust is not just used for "systems programming", but then where's the line? it's just a `social construct` is it? can i trust what i see with my eyes? no, of course not.

### packaging

cargo's package culture seems to mirror that of npm. when people think of `left-pad` they think of:

1. the inadequacy of the standard library (javascript added a native `left-pad` after the incident, [around 2016/17 to be exact](https://caniuse.com/pad-start-end))
2. many, many trivial packages often consisting of a single line, upon which many packages either depend upon, or is the root of a deep, deep dependency tree.

rust's stdlib is capable enough, so point 1 doesn't quite apply. point 2 however… it's not that trivial in rust to do things that are trivial in other languages, sometimes including even C.

i understand, rust requires you to really think about memory in a way that other languages don't (because memory management is quite important), so there's lots of utility packages that iterate on each other. really driving home the meaning of "not reinventing the wheel in an insecure way"

however. one big problem.

compile a rust project, look at the cargo dir.

how big is it?

the simple formula is this: take the large amount of small packages, and consider that each package is statically-compiled. what do you get? BOOM! megabytes of package cruft. at least `node_modules`, even with the ungodly amount of files, don't actually weigh that much.

### culture

aka my biggest non-technical problem with rust.

one or two arrogant bad apples in a community—that's understandable. how about *an entire culture*? don't get me wrong—it's "welcoming", it's "pleasant", and i am sure it's a lot of fun to be around. *however*, consider that:

1. rust is positioned to be the cutting-edge in safe yet performant programming languages.
2. borrow-checking answers the "speed vs safety" question with "why not both?"
3. the same mechanism helps prevent data races, so 🔥fearless concurrency🔥 helping it be 🚀blazing fast🚀
4. result/options instead of exceptions allow for handling errors as soon as possible
5. the community is ✨welcoming✨, obviously

so all of those reasons easily position rust as the best choice for projects technically, objectively, even *morally*. 

Key word: **morally**. what does that mean?

it means people who are obsessed with being **morally correct** are gonna flock to the language, and become its foot soldiers. normally it results in a latent culture where it gives them the "right" to be smug, obnoxious propaganda machines because, what technical recourse do you have?

**after all, why be so wrong, when you can be so goddamn right?**

yes, it doesn't appear all the time, but there is always a chance of this happening when the community interacts with others. what other language has both this kind of reach *and* can afford to be this smug?

this behavior underpins the attitude people have towards things that rust cannot support yet. ["it's obsolete or not ready for prime time yet, you should trash it." "it cannot be secured, you should trash it."](https://github.com/pyca/cryptography/issues/5771)

are programming languages a fucking competition? use what works for the job, right? then [why put your energy into demeaning others who improve themselves](https://twitter.com/workingjubilee/status/1710342254953644244)?

"you're just mad because it's true", yeah and also you are too fucking smug. i couldn't believe someone like that can get ahead in the world, but they often do. i just lack the charisma to make my smugness tolerable.

in fact, this entire culture is what made me rethink the way i talk about linux to non-users. because it had made me realize **the linux community is exactly the same way, especially towards windows users**. so at least i have rustaceans to thank for making me introspect.

i'm not sure where to put this, but i've heard someone responding to this [white house statement](https://www.whitehouse.gov/oncd/briefing-room/2024/02/26/press-release-technical-report/) along the lines of:

> no one forgot about java, swift, ada, etc. as memory safe languages, it's just they suck

…which is *exactly* why the media [forgot about them](https://thenewstack.io/white-house-warns-against-using-memory-unsafe-languages/). again, most languages are memory-safe, but people think of only rust—it's like thinking of only C when talking about "real programs". incidentally steve klabnik [wrote something](https://steveklabnik.com/writing/memory-safety-is-a-red-herring) about putting too much focus on "memory safety".


rust has a "brand" like it or not. not to be confused with how the rust foundation [wants to control it](https://www.theregister.com/2023/04/17/rust_foundation_apologizes_trademark_policy/), however.

it's what makes the hypeseeking crowd want to proudly advertise that it's ["rust-based"](https://s.zkcdn.net/Advertisers/8b48a511be6b442b904549c85e2bfaa2.png), "written in rust", etc. etc.
what are people saying about it? "it's so i know it's something i can work with" "it shows the capability of the language, especially if it's niche"

yeah, same. i can say my projects are "written in nim" because i want to show that:
1. nim can do this kinda thing
2. that programmers who know the language can contribute to or use parts of it in their own projects

but to the wider audience it means nothing. nim means nothing to them, at worst it's derogatory. as in, "lmao, written in discount python."

how about rust?

consider the 70% figure that's repeated to the point that it's a bit like a slur for Cniles. that's the implication that makes "written in rust" different than "written in python" or "written in nim". and some software advertises itself as written in rust in a way that imply that [this alone automatically makes it safer than every other app](https://stalw.art/).

it isn't a panacaea, but it's marketed as such. it's a cure to the illness that is programmers themselves.

it's heavier than even "written in go". what does that imply? that it's easy to contribute to? that it's built on proven technology? i'm not sure.

----

i am sure in due time, i will have to use it, and i will overcome its challenges. but this is something that has come up time and time again. and i kept getting fucking gaslit about this. i'm sick of it.

…but then again, sike, the "non-technical" gripes *are* actually technical gripes, so.
