<!--:date="2021-11-06T01:06:56+07:00"-->

## if you really need to use a framework

might i recommend svelte (compile-time builds, major performance perks!)

or even better, alpine.js ~~(4kb goodness)~~ **it's 36kb**.
not bad, but i dunno where i got that ridiculously low number from.

elementals.js is also pretty good (25kb)
