<!--:date="2021-11-10T20:06:54+07:00"-->

## on building a single page app

when I make one of these, I want one that works well on html as much as it would on a single page. **easier said than done**.

the backend & HTML stuff is piss-easy. it's the SPA-izing that gets me.

the single script file I end up making has hundreds of lines for sure (even more if you count comments).

i'm trying to create and fade in components dynamically from a json response, y'know, like a regular SPA thing. the extra work here, then, is matching what the JS generates to what I laid out in the HTML.

i'm using [bliss.js](https://blissfuljs.com/) for this since it has some shortcuts for common JS stuff.

> come on... do it... you WILL  use react...

maybe i will, but i won't like it much. at least, not until i forget what react is doing to users.

in fact, to me, all of this is worth doing as I'm learning a bunch of stuff.

like `Prototype`, even if i'm doing it to global js objects like `String` and `Date`. we do a little trolling

maybe a bit about `Promise` and `addEventListener` too, even if it's just the basic stuff really

> hundreds of lines? yep, sounds like you're making your own shitty framework. it's not optimized and probably insecure

and?

my single script file so far is 30kb (9kb minified) + 12kb minified for bliss.js
what it's for does so much more than an extremely basic hello world in react (generated code: 130kb) lmao
