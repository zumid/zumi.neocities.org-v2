<!--: date = "2022-02-14T20:35:45+07:00" -->

## the absolute state of web dev

This is from a real website of a modern "web developer".

I couldn't have painted a better picture of 21st century web dev even if I tried.

<img src="state.png" alt="A view of Chrome developer tools, showing HTML code where 80% of it are filled with random emojis">
