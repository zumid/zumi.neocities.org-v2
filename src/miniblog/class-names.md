<!--:date="2021-11-06T06:56:46+07:00"-->

## css class methodologies

### bem (broke)
```
<section class="card">
  <div class="card__image">
    <img src="example.jpg" alt="Example image">
  </div>
  <div class="card__description card__description--flashy">
    <p>Hey lookit me!!! I'm fancy</p>
  </div>
</section>
```
```
.card {}
.card__image {}
.card__description {}
.card__description--flashy {}
```

### smacss (just got out of bed)
```
<section class="card">
  <div class="card-image">
    <img src="example.jpg" alt="Example image">
  </div>
  <div class="card-description is-flashy">
    <p>Hey lookit me!!! I'm fancy</p>
  </div>
</section>
```
```
.card {}
.card-image {}
.card-description {}
.card-description.is-flashy {}
```

### idunno what this is called, it's not like it's better or anything (woke)
```
<section class="card">
  <div class="_image">
    <img src="example.jpg" alt="Example image">
  </div>
  <div class="_description -flashy">
    <p>Hey lookit me!!! I'm fancy</p>
  </div>
</section>
```
```
.card {}
.card ._image {}
.card ._description {}
.card ._description.-flashy {}
```

### directly tying it to markup order (awokened)
```
<section>
  <div>
    <img src="example.jpg" alt="Example image">
  </div>
  <div>
    <p>Hey lookit me!!! I'm fancy</p>
  </div>
</section>
```
```
section {}
section div:nth-of-type(1) {}
section div:nth-of-type(2) {}
```

### utility-only (BESPOKE)
```
<section class="flex-horiz:lg flex-vert:md">
  <div class="h-3">
    <img src="example.jpg" alt="Example image" class="w-100p h-100p">
  </div>
  <div class="px-4 py-4 grey-100 flex-grow">
    <p>Hey lookit me!!! I'm fancy</p>
  </div>
</section>
```
