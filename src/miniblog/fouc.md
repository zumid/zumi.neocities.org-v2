<!--: date = "2021-11-06T00:52:30+07:00" -->

## fouc doesn't actually matter anymore

...because the either the page loads without the styling or the entire render is blocked until the style is loaded

anti-fouc measures can be decidedly anti-user by making the site completely unusable without javascript, as an anti-fouc approach includes covering the page with a loading screen and applying classes which makes elements hide.

if javascript is disabled, the anti-fouc hooks won't run, so nothing will be visible to the user

a surefire way to prevent fouc is ensuring the user is operating on `text/html` at any time
