<!--:date="2021-11-06T16:02:37+07:00"-->

## the most minimal opengraph setup

just the sitename, image, basic html description. ez:

```
<title>Delectable Dinkums</title>
<meta property="og:site_name" content="My Site">
<meta property="og:image" content="https://http.cat/200">
<meta name="description" content="This is where I get to dump all my shitposts :^)">
<meta name="author" content="anon">
```

(note, `og:image` must be an **absolute** path)

here's how it'll show up in discord etc. :

```
.---------------------------------------.
| My Site                               |
|                                       |
| Delectable Dinkums            [Image] |
|                                       |
| This is where I get to                |
| dump all my shitposts                 |
| :^)                                   |
._______________________________________.
```
