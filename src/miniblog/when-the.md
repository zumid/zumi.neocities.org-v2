<!--:date="2022-08-31T18:17:21+07:00"-->
<h2 style="font-size: 0; overflow: hidden;">when you webdev</h2>
<style>
	@keyframes lol {
		0% {
			transform: rotate(-3deg);
		}
		50% {
			transform: rotate(3deg);
		}
		100% {
			transform: rotate(-3deg);
		}
	}
	.sneed {
		position: relative;
		text-align: center;
		animation: lol 1s infinite;
	}
	.sneed img {
		flex-grow: 1;
		max-height: 8em;
		box-shadow: none;
	}
	.sneed p {
		font-family: impact,"arial black",sans-serif;
		font-size: 2em;
		line-height: 1.2;
		letter-spacing: .01em;
		text-shadow: 0 -1px #000, 0 1px #000, 1px 0 #000, -1px 0 #000,
		0 -2px #000, 0 2px #000, 2px 0 #000, -2px 0 #000;
	}
</style>
<div class="sneed">
	<p>WHEN YOU TRADE USER EXPERIENCE</p>
	<img src="ok.png" alt="3d smiley face mischevious thumbs-up">
	<p>FOR DEVELOPER EXPERIENCE</p>
</div>
