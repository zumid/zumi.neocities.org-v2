<!--:date="2021-11-06T01:08:51+07:00"-->

## why single page apps?

> oh its only like .3% who disable javascript and they're autists anyway

that number will rise alright, as more people have slowdowns thanks to the absolute metric ton of js being used on every website worth a damn

you know, this idealism probably won't work and that I should go with what the industry is doing anyway

I hear another point of SPA is shifting rendering logic from the server to the client

the problem is that clients will have **absolute shit** machines

> getting rid of timezone disparities

pretty good idea but even that can be adjusted for separately

> not having to store sessions since variables are global

looks like your app is pretty stateful
