<!--:date="2021-12-12T21:10:25+07:00"-->

## who cares?

> haha, you must be insane for only listing html, css and js on your skills list.
> that's the bare basics lmao. they'll look at people who have much more - react, vue, tailwind... the whole nine yards.
> 
> no one will GIVE A SINGLE SHIT if your website works without scripting or if it doesn't eat your user's ram.
> or if your website (really a web app) is "accessible", like, at all.
> 
> they only care if you can make it in like 5 hours, and looks really good regardless of contrast issues.
> they won't mind the 10 megabytes of javascript loading in the background.
> 
> the world moves fast, bucko. it's time for you to adapt.

who needs stability?

who needs performance?

who needs bandwidth optimization?

the world is burning, may as well roast a marshmallow on it.
