## how scalable are you?

does it matter? i.e. you are a big shot of a company needing to accomodate hundreds of customers concurrently

does throwing more code than needed on your service make it more scalable?

can you explain what is being added and how does it impact your customers?

(even function calls take time!)

<!--:date="2021-11-07T21:32:59+07:00"-->
