<!--: date = "2021-11-05T23:06:26+07:00" -->

## utility-first

they told me that it's utility-*first* CSS, not utility-*only*, and that `@apply` is the solution.

problem is, does anybody even **use** `@apply`????

i think more people should do this (tailwind as an example):

```
.button {
  @apply
    px-4
    py-2
    bg-blue-600
    text-white
    rounded
}
```

granted the output won't be very optimal, but it is a good step
