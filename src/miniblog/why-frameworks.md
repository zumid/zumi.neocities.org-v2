<!--:date="2021-11-06T01:09:36+07:00"-->

## framework 👏 review 👏

I looked at a [review](https://yewtu.be/cuHDQhDhvPE) of vanilla vs js frameworks

points taken:

* hooking to random elements can be hard, have to use `getElements?by*` or `querySelector(All)?`
* can't immediately tell which elements have bound events
    * could probably use something like `data-bound` or somethin
* you'll just make your own shitty library/framework anyway
    * don't reinvent the wheel, even to learn!!!! `</strawman>`

looking at independent demos made from those frameworks and you might've guessed that they require JS just to display the page

svelte I feel fares a bit better because you can make it so that it renders HTML from the get go. the [realworld demo](https://codebase.show/projects/realworld) is a project that aims to put all of these frameworks and libraries to the test, and [even the vanilla app!!!](https://conduit-vanilla.herokuapp.com/#/) *requires* javascript.

[svelte team's](https://realworld.svelte.dev/) on the other hand, has enough conscience to load the basic elements without javascript. [sawir's](https://realworld.sawirstudio.com/) implementation is even better, because you have everything rendered out already using php and I bet alpine is a sweetener.

but then most of these frameworks have syntax that just looks uncanny:

* react has jsx
* you have colons as attribute names in vue
