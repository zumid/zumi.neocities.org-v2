<!--: date = "2021-11-07T09:56:30+07:00" -->

## anti-user

cOnSidErEd hArMfUl:

* scripts obfuscated thru webpack and babel
* bloating the webpage with subversive patterns like utility-only css
* making the client do *all* the work (looking at you, react)
* fixed-unit sizing everywhere
* very low contrast designs where all the content is located
* failing to balance developer convenience with client soundness
* completely unusable site without javascript
