## you know the web sucks

* html
	* barely a standard
	* constantly getting toyed around by browser vendors (hello, browser wars)
	* everyone is picking elements for their visual appearance (see `<h1>` to `<h6>`)
	* one engine is dominant, there are no good browsers.

* js
	* most of js code is machine-generated or compiled, that's why everything's so bloated now
	* the ecosystem gets so big and unnecessarily complex that [javascript fatigue](https://html.duckduckgo.com/html/?q=javascript+fatigue) is actually a thing. i don't see this shit in other programming (or scripting) languages.
	* places with mostly static content that block you if you have js disabled: fuck you.
		* "imagine disabling js": imagine prioritizing <abbr title="developer experience">DX</abbr> over <abbr title="user experience">UX</abbr>, my comfort is your suffering.
		* everywhere should be treated as a privacy AND security nightmare with js enabled. can never be too safe.
		* if your site is ORDERS OF MAGNITUDE faster to navigate with js off, you've got serious issues.
		* how about the [recent](https://www.forbes.com/sites/gordonkelly/2021/09/25/google-chrome-warning-zero-day-hack-new-attack-upgrade-chrome-now/) [zero days](https://www.forbes.com/sites/gordonkelly/2021/10/02/google-chrome-warning-zero-day-new-hack-attack-upgrade-chrome-now/) huh? that's another reason to have noscript.

* css
	* the syntax doesn't make sense for a *cascading* language, oh, but it *is* designed to be "quick".
	* `!important;` `!important;` `!important;` `!important;` `!important;` are you coding son?
	* it's so hacky to get anything right in it
	* noooooooooo you have to use `.message.-error`! haha utility classes go `tx-red.p-4.mx-3.bg-blue.h-4.bs-black:4.zoom.tf-serif`

<!--:date="2021-11-09T07:45:12+07:00"-->
