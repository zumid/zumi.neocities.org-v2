<!--:date="2021-11-06T01:13:01+07:00"-->

## always remember

every html element is a `<div>` or a `<span>` if you wish for it hard enough

so there's no excuse for picking elements solely based on how they look
