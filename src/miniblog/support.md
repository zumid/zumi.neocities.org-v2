## support cutoff

the best way to start thinking about absolute browser "support cutoff" now is from your own server

does it use https? good

now check what kinds of cipher it's using for connections and check the earliest version of a browser that supports it

like i said, it's kinda pointless to design specifically for a 90s browser but then you can't open it as-is on it

<!--:date="2021-11-07T21:37:08+07:00"-->
