## feature detection > user agent sniffing

user agent sniffing is how we got "Mozilla/5.0" on every visual browser UA

not sure if you need extra boilerplate to do that (*cough* Modernizr *cough*), feature detection should be as simple as `if ("customElements" in window)`

<!--:date="2021-11-07T21:28:56+07:00"-->
