# zumi.neocities.org v2

This is the source code for [Zumi's website](https://zumi.neocities.org) starting from Apr. 10, 2021. ~~It has been continuously deployed using GitLab Runner since Oct. 18, 2022.~~ suspended for now lol

## Dependencies

* Python 3
  * `mdiocre`
  * `markdown`
  * `docutils`
  * `pygments` (optional)
* GNU Make
* [MIDICSV](https://www.fourmilab.ch/webtools/midicsv/)
* `tsc` (implies node.js) or `esbuild`.

## Build Overview

1. Create the built CSS directory. (`built/site/css`)
2. Build up the CSS themes inside the CSS directory. (`src/css/<theme>/*.css` -> `built/site/css/<theme>.css`)
3. Generate appropriate templates based on relative directories. (`src/templates/main.html` -> `built/template/*.html`)
4. Run MDiocre on `src/static` to `built/site`.
5. Mark main site as generated.
6. Generate blog indices to `built/site/bloge/index*.html`.
7. Generate blog RSS feed to `built/site/bloge/feed.rss`.
8. Generate miniblog.
9. Generate tagged MIDI files.
10. If there are uncompiled Typescript files (due to missing `tsc`), try to compile them using ESBuild.

## Structure

Where a `//` is specified, it's relative to the built site directory (`built/site/`) or of the website's root. Otherwise, it's relative to the source code root.

### root

* `Makefile` - generate the site. Targets include:
  * `check` - evaluate entire site's HTML under Jason Knight's [ideal HTML size](https://cutcodedown.com/article/minimalist_semantic_markup) algorithm.
  * `clean` - clean generated site and the `.site-generated` mark.
  * `clean-all` - clean site as well as templates.
  * `all` - generate everything below:
  * `css` - generate all CSS themes defined in `$(THEMES)`.
  * `blog-index` - generate both the categorical and chronological blog index.
  * `miniblog` - generates the single-page blog under `//stuff/miniblog/index.html`.
  * `midi` - generates tagged MIDI files under `//stuff/midi/mine/*.mid`.
  * `rss` - generates the site blog's RSS feed.

* `feed.json` - feed metadata template.
  * `baseurl` - where are all the urls relative to?
  * `dirs` - maps source directory to URL directory
  * `pub_var` - which MDiocre variable to use as the publish date

* `Pipfile`; `Pipfile.lock` - recommended Python environment setup.

### src/css

The site's CSS themes. Each theme is in its own folder, and will be built into their respective single files through what is basically `cat *.css > theme.css`.

The files are built to `//css/*.css`.

### src/templates

Directory for the site's templates, which are processed further by `tools/mktemplate.py` in an attempt to create a "portable" site (that is, it doesn't depend on a single base URL). These variables are supplied at compile time (see `Makefile`). MDiocre at this time could not be used to override page variables in this way.

The files are built to `built/template/*.html`.

### src/static

Source code to most of the site, generated directly using MDiocre.

### src/miniblog

The single-page miniblog located in `//stuff/miniblog/index.html`. `_.html` serves as the page template, while `tools/mkminiblog.py` is used to generate the page itself.

The names of each file are used as IDs for the index page, and a single MDiocre variable is used: `date`.

### src/my_midi

Raw MIDI files to be then processed by the tool to produce tagged MIDIs. Tags are in `meta.ini`, where `t` is the song's title text, `c` is the copyright information, and `d` is the date the MIDI has finished sequencing.

### tools/

* `check.py` - modules to calculate a markup's ideal size based on Jason's algorithm, linked above. When executed directly, it checks a single file.
* `check_all.py` - calculates the ideal size of every HTML file in a directory.
* `mkfeed.py` - generate an RSS feed using MDiocre and Python built-ins.
* `mkindex.py` - generates a chronological (by date, descending) blog index to stdout, configured to build under `//bloge/index-chrono.html` in the Makefile. Includes the corresponding blog index template.
* `mkindex_categorical.py` - generates a categorical blog index to stdout, configured to build under `//bloge/index.html` in the Makefile. Includes the corresponding blog index template.
* `mkminiblog.py` - generates a single-page miniblog from the contents of a source folder and template file to `stdout`, configured to build under `//stuff/miniblog/index.html`.
* `mktemplate.py` - substitutes variables in a file provided in the command line invocation. Variables are formatted as `{{ variable }} {{ variable2 }}`, and are supplied in the CLI as `"variable=My Variable Here,variable2=This Other Variable" `.
* `safe_cat.py` - a version of `cat` to account for NTFS Interix symbolic links.
* `produce_midi.py` - (attempts to) optimize and tags my MIDIs. Should a tag be missing, it won't insert it into the MIDI at all.

### utils/

Python functions used by MDiocre via the `using:` directive, primarily to transform dates and texts.
