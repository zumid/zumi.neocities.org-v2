BUILD_DIR := built
SITE_DIR  := $(BUILD_DIR)/site
UNCOMPILED_TS := $(shell find $(SITE_DIR) -name '*.ts')
COMPILED_JS := $(patsubst %.ts, %.js, $(UNCOMPILED_TS)) 

all: $(COMPILED_JS)

%.js: %.ts
	sed 's/\(let\|const\)\s\+/var /g' < $< | esbuild --loader=ts --banner="\"use strict\";" --target=firefox27 > $@
	rm $<
