def ISO8601_to_BlogDate(arg):
	from datetime import datetime
	return datetime.strftime(
		datetime.strptime(arg, "%Y-%m-%dT%H:%M:%S%z"),
		"%B %-d, %Y"
	)

def ISO8601_to_BlogDateTime(arg):
	from datetime import datetime
	return datetime.strftime(
		datetime.strptime(arg, "%Y-%m-%dT%H:%M:%S%z"),
		"%b %-d '%y at %H:%M"
	)
